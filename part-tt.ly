\version "2.23.4"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Psyché" }
  \markup\null
}

%% Musique
\act "Acte Cinquième"
\markup { \bold 5-1 à \bold 5-13 : tacet }
\markup\column {
  \line { \hspace#2 \smallCaps Mars }
  \line { \hspace#6 Mes plus fiers ennemis vaincus ou pleins d’effroi, }
  \line { \hspace#8 Ont vu toujours ma valeur triomphante : }
  \line { \hspace#10 L’Amour est le seul qui se vante }
  \line { \hspace#10 D’avoir pu triompher de moi. }
}
%% 5-14
\pieceTocNb "5-14" \markup\wordwrap {
  Chœur des Dieux : \italic { Chantons les plaisirs charmants }
}
\includeScore "FDKchoeur"

\markup\column {
  \line { \bold 5-15 à \bold 5-26 : tacet }
  \line { \hspace#2 \smallCaps Momus }
  \fill-line {
    \column {
      \line { \hspace#10 Folâtrons, divertissons-nous, }
      \line { \hspace#10 Raillons, nous ne saurions mieux faire, }
      \line { \hspace#10 La raillerie est nécessaire, }
      \line { \hspace#12 Dans les jeux les plus doux : }
      \line { \hspace#8 Sans la douceur que l’on goûte à médire, }
      \line { \hspace#8 On trouve peu de plaisirs sans ennui, }
      \line { \hspace#10 Rien n’est si plaisant que de rire, }
      \line { \hspace#10 Quand on rit aux dépens d’autrui. }
    }
    \column {
      \line { \hspace#10 Plaisantons, ne pardonnons rien, }
      \line { \hspace#10 Rions, rien n’est plus à la mode : }
      \line { \hspace#10 On court péril d’être incommode, }
      \line { \hspace#12 En disant trop de bien : }
      \line { \hspace#8 Sans la douceur que l’on goûte à médire, }
      \line { \hspace#8 On trouve peu de plaisirs sans ennui, }
      \line { \hspace#10 Rien n’est si plaisant que de rire, }
      \line { \hspace#10 Quand on rit aux dépens d’autrui. }
    }
  }
}
%\partPageBreak#'(trompette-timbales)
%% 5-27
\pieceTocNb "5-27" \markup\wordwrap {
  Prélude de trompettes, et de violons, en écho, pour Mars.
}
\includeScore "FDXprelude"

\markup\column {
  \line { \bold 5-28 : tacet }
  \line { \hspace#2 \smallCaps Mars }
  \line { \hspace#10 Laissons en paix toute la terre, }
  \line { \hspace#10 Cherchons de doux amusements : }
  \line { \hspace#10 Parmi les jeux les plus charmants, }
  \line { \hspace#10 Mêlons l'image de la guerre. }
}
%% 5-29
\pieceTocNb "5-29" "Premier air pour les suivants de Mars"
\includeScore "FDYrondeau" \noPageTurn
%% 5-30
\pieceTocNb "5-30" "Deuxième Air, pour les suivants de Mars"
\includeScore "FDZair"
%% 5-31
\pieceTocNb "5-31" \markup\wordwrap {
  Chœur des Dieux : \italic { Chantons les plaisirs charmants }
}
\reIncludeScore "FDKchoeur" "FDZZchoeur"
\actEnd\markup { FIN DU CINQUIÈME ET DERNIER ACTE. }
