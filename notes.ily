\notesSection "Notes"
\markuplist\with-line-width-ratio #0.7 \column-lines {
  \livretAct NOTES
  \null
  \livretParagraph {
    Cette édition de la tragédie lyrique \italic { Psyché }
    de Jean-Baptiste Lully,
    est basée sur les sources suivantes :
  }
  \null
  %% manuscrit
  \indented-lines#5 {
    \line\bold { [Manuscrit Versailles] }
    \line { Jean-Baptiste Lully (1632-1687), }
    \wordwrap\italic {
      PSICHÉ, Tragedie Representée par l'accademie Royalle de Musique.
    }
    \wordwrap {
      Bibliothèque Municipale de Versailles, Manuscrit musical 101,
      ca. 1690-1702.
    }
  }
  \null
  %% gravure
  \indented-lines#5 {
    \line\bold { [Ballard] }
    \line { Jean-Baptiste Lully (1632-1687), }
    \wordwrap\italic {
      Psyché, tragédie, mise en musique par Mr de Lully, représenté par l'académie Royale de musique en l'année 1678, partition générale imprimée pour la première fois 
    }
    \wordwrap {  J. B. Christophe Ballard, Paris, 1720 }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Musique, VM2-37
    }
  }
  \null
  \paragraph {
    C’est le manuscrit de Versailles qui est la source principale
    pour la musique de cette édition. Les titres, indications scéniques,
    et la syntaxe des textes sont ceux de l’édition Ballard.
    Quand l’édition Ballard présente des ajouts ou des différences
    notables par rapport au manuscrit, ceux-ci ont été reportés dans
    cette édition avec la précision : \italic { [Ballard]. }
  }
  \null
  \paragraph {
    Le matériel d'orchestre est constitué des parties
    séparées suivantes :
    dessus, hautes-contre, tailles, quintes, basses, basse continue.
  }
  \null
  \paragraph {
    Cette édition est distribuée selon les termes de la licence
    Creative Commons Attribution-ShareAlike 4.0.  Il est donc permis,
    et encouragé, de jouer cette partition, la distribuer,
    l’imprimer.
  }
}
