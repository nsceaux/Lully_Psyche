\version "2.19.37"

\header {
  copyrightYear = "2006"
  composer = "Jean-Baptiste Lully"
  poet = \markup\fontsize#-2 \column {
    \fill-line { "Quinault, Corneille," }
    \fill-line { "Bernard le Bovier de Fontenelle" }
  }
  date = "1678"
  opus = "LWV 56"
}

#(ly:set-option 'original-layout #f)
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size
  (cond ((not (symbol? (ly:get-option 'part))) 16)
        ((memq (ly:get-option 'part) '(basse-continue)) 18)
        (else 20)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"

\setPath "ly"
\opusPartSpecs
#`((dessus       "Dessus"        () (#:notes "dessus"))
   (haute-contre "Hautes-contre" () (#:notes "haute-contre" #:clef "alto"))
   (taille       "Tailles"       () (#:notes "taille" #:clef "alto"))
   (quinte       "Quintes"       () (#:notes "quinte" #:clef "alto"))
   (basse        "Basses"        () (#:notes "basse" #:tag-notes basse #:clef "basse"))
   (timbales     "Timbales"      () (#:notes "timbales" #:clef "basse"))
   (trompette    "Trompette"     () (#:notes "trompette" #:clef "treble"))
   (trompette-timbales "Tompettes et timbales" () (#:score "score-tt"))
   (basse-continue
    "Basse continue" ()
    (#:notes "basse" #:tag-notes basse-continue #:clef "basse")))
\opusTitle "Psyché"
