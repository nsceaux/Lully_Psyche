\tag #'(couplet1 basse) {
  Ren -- dez "- vous," beau -- tés cru -- el -- les, 
  sou -- pi -- rez, sou -- pi -- rez à vo -- tre tour.

  Voi -- ci la Rei -- ne des Bel -- les, 
  qui vient ins -- pi -- rer l'a -- mour.
  - mour.
}
\tag #'couplet2 {
  Souf -- frons tous qu'A -- mour nous bles -- se,
  lan -- guis -- sons, lan -- guis -- sons, puis -- qu'il le faut.

  Que sert un cœur sans ten -- dres -- se ? 
  Est - il un plus grand dé -- faut ?
  - faut ?
}
\tag #'(couplet1 basse) {
  Un bel ob -- jet tou -- jours sé -- vè -- re, 
  ne se fait ja -- mais bien ai -- mer. 

  C'est la beau -- té qui com -- men -- ce de plai -- re,
  mais la dou -- ceur a -- chè -- ve de char -- mer.
}
\tag #'(couplet1 voix2 basse) {
  C'est la beau -- té qui com -- men -- ce de plai -- re,
  mais la dou -- ceur a -- chè -- ve de char -- mer.
  - mer.
}
