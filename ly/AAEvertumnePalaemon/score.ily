\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'voix2 \includeNotes "voix" 
    >> \keepWithTag#'voix2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >>
    \keepWithTag#'couplet1 \includeLyrics "paroles"
    \keepWithTag#'couplet2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
