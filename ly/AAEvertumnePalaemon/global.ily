\key do \major \midiTempo#120
\time 4/4
s1*8 \alternatives s1 s2 \bar ".|:" s2 s1*12
\alternatives {
  \set Score.measureLength = #(ly:make-moment 2 4)
  s2
} {
  \set Score.measureLength = #(ly:make-moment 4 4)
  s1
}
\bar "|."
\endMark\markup\smaller\override#'(line-width . 60) \justify {
  Après ce Duo, Vertumne commence le second couplet :
  \normal-text { Souffrons, &c. } Palemon continue,
  & ils repetent de suite :
  \normal-text { Un bel objet, &c. } jusqu’à la fin du Duo.
}
