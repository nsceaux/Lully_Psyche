<<
  \tag #'(voix1 basse) {
    \clef "vhaute-contre" <>^\markup\character Vertumne
    r4 do'8. re'16 mi'4. mi'16 mi'16 (|
    fa'4. ) mi'8 re'4\trill re'4 |
    r4 sol'8 sol'8 sol'8[ fa'8] r8 fa'16 fa'16 |
    fa'4 mi'4 re'4.\trill do'8 |
    do'2.
    \clef "vtaille" <>^\markup\character Palemon
    r8 mi'8 |
    si4. si8 do'4 do'8 si8 |
    la4\trill la4 r4 re'4 |
    do'4\trill do'8 si8 si4 ( la8.)\trill sol16 |
    sol1 |
    sol2
    \ffclef "vhaute-contre" <>^\markup\character Vertumne
    r8 mi'8 re'8. mi'16 |
    fa'4. mi'8 re'8 [ dod'8 ] re'4 |
    dod'4\trill dod'4 r4 la'8. la'16 |
    mi'8. mi'16 fa'4 mi'4.\trill re'8 |
    re'2
    \ffclef "vtaille" <>^\markup\character Palemon
    r8 la8 la8 si8 |
    do'4 do'8 re'8 mi'8 fa'16 [ mi'16 re'8 ] mi'16 [ do'16 ] |
    si4\trill si4 r8 mi'8 mi'8 do'8 |
    la4. la8 si4 do'4 |
    do'4( si8\trill) do'8 do'2 |
  }
  \tag #'voix2 { \clef "vhaute-contre" R1*9 | r2 r2 | R1*8 }
>>
<<
  \tag #'(voix2 basse) {
    <>^\markup\character Vertumne
    r8 mi'8 mi'8. fa'16 sol'4 mi'8. mi'16 |
    fa'4 fa'8. mi'16 re'4\trill re'4 |
    r8 sol'8 sol'8 sol'8 fa'4. fa'8 |
    fa'4 mi'4 re'4.\trill do'8 |
    do'2 |
    do'1 |
  }
  \tag #'voix1 {
    r8 do'8 do'8. re'16 mi'4 dod'8. dod'16 |
    re'4 re'8. do'16 si4\trill si4 |
    r8 mi'8 mi'8 do'8 la4. la8 |
    si4 do'4 si4.\trill do'8 |
    do'2 |
    do'1 |
  }
>>