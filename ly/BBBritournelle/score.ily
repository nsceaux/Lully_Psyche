\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
      \new Staff << \global \includeNotes "flute3" >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
  >>
  \layout { }
  \midi { }
}
