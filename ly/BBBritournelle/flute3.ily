\clef "basse" R1.*9 |
r2 do'1 ~ |
do'2 si2 sib2 |
la2 lab1 |
sol2 do1 |
fa1 fa2 |
mib4. fa8 sol2 sol,2 |
do1 r2 |
R1.*2 |
r2 sol2. sol4 |
lab2 la2. la4 |
sib2 sol2. sol4 |
lab1 fa2 |
sib2 sib,1 |
mib1 r2 |
r2 sib2. sib4 |
fa1 r2 |
r2 do'2. do'4 |
sol1 sol2 |
fa1. |
mib1 fa2 |
sol2 sol,1 |
do1 r2 |
R1.*5
