\score {
 <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
      \new Staff << \global \includeNotes "flute3" >>
    >>
    \new Staff << \global \includeNotes "dessus" >>
  >>
  \layout { }
}
