\piecePartSpecs
#`((basse-continue)
   (basse)
   (quinte)
   (taille)
   (haute-contre)
   (dessus #:score "score-dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#37 #}))
