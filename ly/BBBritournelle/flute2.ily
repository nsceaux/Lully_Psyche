\clef "dessus" R1.*9
r2 mib''2 fa''2 |
re''2. re''4 mib''4. re''8 |
do''2. re''4 re''4. do''8 |
si'4. la'16 si'16 do''2. si'4 |
la'4. si'16 do''16 si'2. si'4 |
do''2. do''4 si'4.\trill do''8 |
do''1 r2 |
R1.*2 |
r2 si'2. si'4 |
do''2 do''2. do''4 |
re''2 re''2 sol''2 |
do''2 re''2 mib''2 |
mib''2 re''2.\trill do''8 re''8 |
mib''1 r2 |
r2 re''2. mib''4 |
fa''1 r2 |
r2 mib''2. mib''4 |
re''2 mib''2. mib''4 |
mib''2 re''2. re''4 |
re''2 do''2. do''4 |
do''2 si'2.\trill la'8 si'8 |
do''1 r2 |
R1.*5 |
