\clef "dessus" <>^"Violons" r2 sol'2. sol'4 |
lab'2 sol'2 do''2 ~ |
do''2 si'2. si'4 |
do''2 re''2. re''4 |
mib''2 mi''2. mi''4 |
fa''2 sol''2. sol''4 |
lab''2 re''2. mib''4 |
si'2 do''2. re''4 |
mib''2 re''2.\trill do''4 |
do''2 r r |
R1.*5 |
r2 mib''2. mib''4 |
re''2 re''2. mib''4 |
do''2 do''2. re''4 |
si'2\trill r r |
R1.*4 |
r2 sib'2. do''4 |
re''2 r r |
r2 do''2. re''4 |
mib''2 r r |
R1.*4 |
r2 mib''2. re''4 |
do''2 sib'2. do''4 |
la'2 re''2. do''4 |
si'2 sol'2 mib''2 ~ |
mib''4. re''8 re''2.\trill do''4 |
do''1. |
