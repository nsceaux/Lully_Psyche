\clef "dessus" R1.*9
r2 <>^"Flûtes" sol''2 lab''2 |
fa''2 sol''2. sol''4 |
sol''2 fa''2. fa''4 |
fa''2 mib''2. mib''4 |
mib''2 re''2. re''4 |
mib''2 re''2.\trill do''4 |
do''1 r2 |
R1.*2 |
r2 <>^"Flûtes" sol''2. sol''4 |
sol''2 fa''2. fa''4 |
fa''4. sol''16 la''16 sib''2. sib''4 |
sib''2 lab''2 sol''2 |
fa''2. fa''4 sol''4 lab''4 |
sol''1\trill r2 |
r2 fa''2. sol''4 |
lab''1 r2 |
r2 sol''2. la''4 |
sib''1 do'''4 sol''4 |
lab''1 sib''4 fa''4 |
sol''2. sol''4 fa''4. mib''8 |
re''4. mib''8 re''2.\trill do''4 |
do''1 r2 |
R1.*5
