\clef "haute-contre" si' si'8 do'' re''4 |
la'2 la'8 si' |
sol'4 sol'4. la'8 |
fad'4 fad'4. sol'8 |
la'4 mi'4. fad'8 |
sol'2 sol'8 la' |
si'4 si'4. si'8 |
do''4 dod''4. dod''8 |
re''2 re''4~ |
re'' re''4. do''8 |
si' la' sol'4 re'8 mi' |
fad'2 fad'4 |
sol'2 sol'4 |
la'8 sol' fad'4.\trill sol'8 |
sol'4 sol'8 la' si'4 |
r la'8 sol' fad'4 |
r sol' sol' |
la'8 sol' fad'4.\trill mi'16 fad' |
sol'4 sol'8 la' si' sol' |
la'4 fad'8 sol' la' fad' |
sol'2 sol'8. la'16 |
la'8. sol'16 fad'4.\trill sol'8 |
sol'4 si'4. si'8 |
do''4 la'4. la'8 |
la'2 sol'4~ |
sol'8 la' si'2 |
si'2. |
la'2 la'4 |
si'2 si'4 |
la'8 si' dod''4 re'' |
mi''8 re'' dod''4.( si'16 dod'') |
re''4 fad'4. fad'8 |
sol'2 sol'4 |
la'4 sol'4. fad'8 |
mi'4. mi'8 la' sol' |
fad'2. |
r4 sol' sol' |
sol' la' la' |
la' re''2 |
re''4. si'8 do'' re'' |
mi''4 do''4. do''8 |
re''2 re''8 do'' |
si' do'' la'4.\trill sol'8 |
sol'4 sol' re'8 mi' |
fad'2 fad'4 |
sol'2 la'4 |
la'8 sol' fad'4.\trill sol'8 |
sol'4 sol'8 la' si'4 |
r la'8 sol' fad'4 |
r sol' sol' |
la'8 sol' fad'4.\trill mi'16 fad' |
sol'4 sol'8 la' si' sol' |
la'4 fad'8 sol' la' fad' |
sol'2 sol'8. la'16 |
la'8. sol'16 fad'4.\trill sol'8 |
sol'2. |
