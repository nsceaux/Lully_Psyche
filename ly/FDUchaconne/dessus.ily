\clef "dessus" sol''4 sol''8 la'' si'' sol'' |
la''4 re''4. re''8 |
re''4 dod''4. si'16 dod'' |
re''4 la'4. si'8 |
do''4 do''4. re''8 |
si'4.\trill la'8 si' do'' |
re''4. re''8 mi'' fad'' |
mi''4. mi''8 fad'' sol'' |
fad''4.\trill fad''8 sol''4~ |
sol''8 la'' la''4.\trill sol''8 |
sol''4 sol'8 la' si' do'' |
re''4. mi''8 re'' mi'' |
re'' mi'' re'' mi'' re'' mi'' |
re'' mi'' re'' mi'' re'' do'' |
si'4 si'8 do'' re''4 |
r la'8 si' do''4 |
r sol'8 la' si'4 |
do''8 si' la'4 si'8 do'' |
si'4 si'8 do'' re'' mi'' |
la'4 la'8 si' do'' re'' |
sol'4 sol'8 la' si'8. do''16 |
do''8.\trill si'16 la'4.\trill sol'8 |
sol'4 sol''4. sol''8 |
sol''4 fad''4. la''8 |
red''4. si'8 mi''4~ |
mi''8 fad'' fad''4.\trill mi''8 |
mi''4 sol''4. sol''8 |
sol''4 fad''4. fad''8 |
fad''4 sol''8 fad'' mi'' re'' |
dod''4 la'' si''8 la'' |
sol'' fad'' mi''4 fad''8 sol'' |
fad''4\trill re''4. re''8 |
re''4 do''4.\trill do''8 |
do''4 si'4. si'8 |
si'4 la'4. la'8 |
la'2. |
r4 re'' sol'' |
mi'' mi'' la'' |
fad''4.\trill fad''8 sol'' la'' |
si''2.~ |
si''4 la''4. la''8 |
la''2 sol''4~ |
sol''8 la'' fad''4.\trill mi''16 fad'' |
sol''4 sol'8 la' si' do'' |
re''4. mi''8 re'' mi'' |
re'' mi'' re'' mi'' re'' mi'' |
re'' mi'' re'' mi'' re'' do'' |
si'4 si'8 do'' re''4 |
r4 la'8 si' do''4 |
r sol'8 la' si'4 |
do''8 si' la'4 si'8 do'' |
si'4 si'8 do'' re'' mi'' |
la'4 la'8 si' do'' re'' |
sol'4 sol'8 la' si'8. do''16 do''8. si'16 la'4.\trill sol'8 |
sol'2.\fermata |
