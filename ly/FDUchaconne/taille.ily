\clef "taille" re'4 re' re'8 mi' |
fad'2 fad'4 |
sol' mi'4. mi'8 |
la'2 re'4 |
mi'8 re' do' si la4 |
si sol'2 |
sol' sol'4 |
sol' la'2 |
la' sol'4~ |
sol' fad'4.\trill( mi'16 fad') |
sol'4 si8 do' re'4 |
re'2 re'4 |
re'4 do'4. do'8 |
do'8 si la4 re' |
re' re' re' |
r re' re' |
r si mi'~ |
mi' re'4. re'8 |
re'4 re' re' |
re'2 re'8 la |
si2 mi'4~ |
mi' re'4. do'8 |
si2 sol'8 fad' |
mi'4 fad'4. fad'8 |
fad'2 mi'4~ |
mi' red'4.\trill mi'8 |
mi'2. |
mi'4 re'2 |
re' mi'4 |
mi'2 fad'4 |
mi' mi'4.\trill re'8 |
re'2 re'4 |
si mi'4. mi'8 |
re'4 re'2 |
do'8 re' mi'2 |
re'2. |
r4 re' re' |
mi' mi' mi' |
re'8 mi' fad'4. fad'8 |
sol'2 sol'4 |
sol' la'2 |
re'2. |
re'4 re'4. do'8 |
si2. |
re'2 re'4 |
re'4 do'4. do'8 |
do' si la4 re' |
re' re' re' |
r4 re' re' |
r si mi'~ |
mi' re'4. re'8 |
re'4 re' re' |
re'2 re'8 do' |
si2 mi'4~ |
mi' re'4. do'8 |
si2. |
