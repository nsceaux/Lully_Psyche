\clef "basse" sol4 sol sol |
fad2. |
mi |
re2 re4 |
la,2 la,4 |
mi4. mi8 re do |
si,4 sol,2 |
do4 la,2 |
re si,4 |
sol, re re, |
sol, sol2 |
fad2. |
mi2 do4 |
re re,2 |
sol,4 sol2 |
fad re4 |
mi2. |
do4 re re, |
sol, sol2 |
fad4 fad2 |
mi2. |
do4 re2 |
sol2. |
la |
si2 mi4~ |
mi si,2 |
mi4. fa8 mi re |
dod4 re2 |
sol,2. |
la,2 fad,4 |
sol, la,2 |
re, re4 |
mi2 mi4 |
fad4 sol2 |
do4 dod2 |
re4. mi8 re do |
si,2 si,4 |
do dod2 |
re re4 |
sol4. la8 si sol |
do'2. |
si |
sol4 re' re |
sol2. |
fad |
mi2 do4 |
re re,2 |
sol,4 sol2 |
fad re4 |
mi2. |
do4 re re, |
sol, sol2 |
fad4 re2 |
mi2. |
do4 re re, |
sol,2. |
