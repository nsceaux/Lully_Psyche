\clef "quinte" sol4 re'8 do' si sol |
re'2 re'4 |
mi'2 mi'4 |
la2 la4 |
la2 la4 |
sol2 sol4 |
sol re'2 |
do'8 re' mi'2 |
re'4 la si~ |
si la re' |
re' re'8 do' si4 |
la2 la4 |
sol mi4. mi8 |
fad sol la4. la8 |
sol4 sol sol |
r la la |
r sol mi |
la la4. la8 |
sol4 sol sol |
re'2 la4 |
sol8 la si2 |
la4 la re' |
re'2 re'4 |
do'2 do'4 |
si2 si4~ |
si si4. si8 |
sol2. |
la4. la8 re' re' |
si4 sol sol'8 fad' |
mi'2 re'4 |
si4 la4. la8 |
la2 la4 |
sol2 do'4 |
la4 si sol |
sol la4. la8 |
la2. |
r4 si8 do' re'4 |
do' la la |
la2 la4 |
sol re'4. re'8 |
do'8 re' mi'4. mi'8 |
si4. do'8 re'4 |
re' re'4. re'8 |
re'4 re'8 do' si4 |
la2 la4 |
sol mi4. mi8 |
fad sol la4. la8 |
sol4 sol sol |
r4 la la |
r sol mi |
la la4. la8 |
sol4 sol sol |
re'2 la4 |
sol8 la si2 |
la4 la re' |
re'2. |
