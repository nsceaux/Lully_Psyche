\tag #'couplet1 {
  Bac -- chus veut qu’on boive à longs traits,
  on ne se plaint ja -- mais
  sous son heu -- reux em -- pi -- re : - re.
  
  Tout le jour on n’y fait que ri -- re,
  la nuit on y dort en paix.
  Tout le jour on n’y fait que ri -- re,
  la nuit on y dort __ on y dort en paix.
  La nuit on y dort __ on y dort en paix.
  Tout le
  paix.
}
\tag #'couplet2 {
  Ce Dieu rend nos vœux sa -- tis -- fait ;
  Que sa cour a d’at -- traits !
  Chan -- tons- y bien sa gloi -- re : - re.
}
