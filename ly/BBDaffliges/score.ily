\score {
  \new ChoirStaff \with { \smallStaff } <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { system-count = 6 }
  \midi { }
}
