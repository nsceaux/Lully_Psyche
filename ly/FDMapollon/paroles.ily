\tag #'(couplet1 basse) {
  Le Dieu qui nous en -- ga -- ge
  à lui fai -- re la cour,
  dé -- fend qu’on soit trop sa -- ge.
  Les plai -- sirs ont leur tour :
  C’est leur plus doux u -- sa -- ge
  que de fi -- nir les soins du jour ;
  La nuit est le par -- ta -- ge des jeux et de l’a -- mour.
}
\tag #'couplet2 {
  Ce se -- rait grand dom -- ma -- ge
  qu’en ce char -- mant sé -- jour,
  on eut un cœur sau -- va -- ge.
}
