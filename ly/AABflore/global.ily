\key do \major \midiTempo#120
\time 2/2 s1*8 \alternatives s1 { \digitTime\time 3/4 s2. }
\bar ".!:" s2.*16 \alternatives s2.*2 { \digitTime\time 2/2 s2 }
\bar "|."
