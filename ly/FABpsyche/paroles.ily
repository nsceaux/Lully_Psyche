Si je fais va -- ni -- té de ma ten -- dresse ex -- trê -- me,
en puis- je trop a -- voir quand c’est de l’A -- mour mê -- me
que mon cœur s’est lais -- sé char -- mer ?
Je sens que rien ne peut é -- bran -- ler ma cons -- stan -- ce.
Ah ! pour -- quoi m’o -- bli -- ger d’ai -- mer,
s’il faut ai -- mer sans es -- pé -- ran -- ce !
Sans es -- pé -- ran -- ce ? non, c’est of -- fen -- ser l’A -- mour,
ce Dieu qui plaint les maux dont je suis pour -- sui -- vi -- e,
jus -- que dans les En -- fers a pris soin de ma vi -- e,
et c’est par lui que je re -- viens au jour.
Ce sont i -- ci les jar -- dins de sa mè -- re,
peut- être en ce mo -- ment il lui par -- le de moi.
Je puis l’y re -- con -- trer : pour mé -- ri -- ter sa foi,
cher -- chons jus -- qu’au bout à lui plai -- re.
Si mes en -- nuis ont pu ter -- nir
ces at -- traits dont l’é -- clat m’a su ren -- dre cou -- pa -- ble,
cet -- te boë -- te me va four -- nir
de quoi pa -- raître en -- core ai -- ma -- ble…
Ou -- vrons… Quel -- les promp -- tes va -- peurs
me font des sens per -- dre l’u -- sa -- ge !
Si la mort fi -- nit mes mal -- heurs,
ô toi qui de mes vœux re -- çois le tendre hom -- ma -- ge,
son -- ge qu’en ex -- pi -- rant, c’est pour toi que je meurs.
