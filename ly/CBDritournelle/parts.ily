\piecePartSpecs
#`((basse-continue)
   (basse)
   (quinte)
   (taille)
   (haute-contre)
   (dessus)
   (silence #:on-the-fly-markup , #{ \markup\tacet#20 #}))
