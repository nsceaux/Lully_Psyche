\score {
  \new StaffGroupNoBar <<
    \new Staff \withLyrics << 
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics << 
      \global \keepWithTag #'recit0 \includeNotes "voix"
    >> \keepWithTag #'recit0 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      { s1 s2.*2 s1*4 s1 s2. s1*3 s1*7 s1*4 s2. s1 
        s1*3 s2.*4 s1 s1*4 s2.*4 s1*2 s2. \break }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
