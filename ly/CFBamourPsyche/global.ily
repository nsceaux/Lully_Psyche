\set Score.currentBarNumber = 8 \bar ""
\key la \minor
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*4
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*3
\time 4/4 s1*7
\digitTime\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 \midiTempo#80 s2.*4
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 s2.*4
\time 4/4 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*19
\time 4/4 \midiTempo#80 s1*7
\digitTime\time 3/4 s2.
\time 4/4 s1*7
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*2
\time 4/4 s1*4
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 s2.*2
\bar ".|:" s2.*8 \alternatives s2. s2. s2.*14 \bar "|."
