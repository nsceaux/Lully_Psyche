<<
  \tag #'(recit1 basse) {
    \clef "vhaute-contre" <>^\markup\character L'Amour
    r2 r8 mi'8 do'8 mi'8 |
    si4 si8 si16 si16 mi'8 si8 |
    do'8 do'16 do'16 do'8. fa'16 re'8 re'16 sol'16 |
    mi'8\trill mi'8 r8 sol'8 do'8 do'8 do'8 mi'8 |
    la8 la8 re'4. re'8 re'8 dod'8 |
    re'4 r16 la16 la16 si16 do'8 do'16 mi'16 do'8 do'16 si16 |
    si4\trill si4 
    \clef "vbas-dessus" <>^\markup\character Psyché
    r4 mi''4 |
    r4 si'4 sold'4 sold'8 si'8 |
    mi'8 mi'8 r8 mi'16 mi'16 la'8 la'16 si'16 |
    do''8. la'16 la'8. la'16 re''8 re''8 re''8 mi''8 |
    fa''4 fa''8 re''8 la'8 la'8 la'8 si'8 |
    do''8 do''16 do''16 sol'8 la'16 sib'16 la'8\trill la'8 r16 la'16 la'16 si'16 |
    do''4. do''8 re''8 re''8 mi''8 fa''8 |
    mi''4\trill r8 mi'16 mi'16 la'4 la'8 la'8 |
    fad'4 fad'8 fad'8 si'4 si'8 si'8 |
    sold'4 sold'8 si'8 si'8 si'8 si'8 do''8 |
    la'4 la'8 la'8 re''4 re''8 re''8 |
    si'4\trill si'4 r8 si'8 si'8 mi''8 |
    dod''4 dod''8 red''8 mi''4 mi''8 red''8 |
    mi''1 |
    r4 mi'8 fa'8 sol'4 la'8 si'8 |
    do''4 do''8 do''8 do''4 re''8 mi''8 |
    la'4\trill la'8 r8 do''8 do''16 do''16 do''8 do''8 |
    fa''8 mi''8 re''8 do''8 si'8 la'8 |
    sold'8\trill sold'8 si'16 si'16 si'16 do''16 la'4 la'8 sold'8 |
    la'4 
    \clef "vhaute-contre" <>^\markup\character L'Amour
    r8 do'8 dod'8 dod'8 dod'8 dod'8  |
    re'8 re'8 mi'8 fa'8 dod'4. re'8  |
    re'4 re'8 fa'8 la8 la8 la8 si8  |
    do'8 do'8 do'8 re'8 re'8.\trill mi'16  |
    mi'4 r8 si16 si16 mi'8 mi'16 mi'16  |
    dod'8\trill dod'8 r16 la16 la16 la16 re'8 mi'8  |
    fa'8 la'16 sol'16 fa'8 fa'16 mi'16 re'8 do'8  |
    si4\trill si4 r4 si8 do'8  |
    la8 la8 
    \clef "vbas-dessus" <>^\markup\character Psyché
    r16 do''16 do''16 mi''16 do''8 do''8 do''8 si'8 |
    si'8\trill 
    \clef "vhaute-contre" <>^\markup\character L'Amour
    si8 mi'8 mi'8 dod'8 dod'8 red'8 mi'8 |
    red'4\trill red'4 
    \clef "vbas-dessus" <>^\markup\character Psyché
    r8 si'8 si'8 si'8 |
    si'4 la'8 la'16 sold'16 la'4 r4 |
    \clef "vhaute-contre" <>^\markup\character L'Amour
    r2 r16 mi'16 mi'16 mi'16 |
    si8 si8 si8 si8 do'8 do'8 |
    la8\trill la8 r8 do'16 do'16 fa'8 fa'16 la16 |
    si8 si8 do'8. re'16 re'8.\trill do'16 |
    do'8 
    \clef "vbas-dessus" <>^\markup\character Psyché
    r16 mi'16 mi'16 mi'16 mi'16 mi'16 la'8 la'8 la'8 mi'8 |
    fa'4 fa''4 r16 re''16 re''16 re''16 si'8 do''16 re''16 |
    sold'4\trill sold'4 r4 |
    <>^\markup\character Psyché
    mi''2 si'8 si'8 |
    do''4 dod''4. dod''8 |
    re''4 la'4. si'8 |
    do''4 do''4. re''8 |
    si'4.\trill si'8 si'8 si'8 |
    mi''4 mi''4 do''4 |
    la'4 la'4 r4 |
    la'4 si'4 do''4 |
    do''4. ( si'8 ) si'16 [ la'16 ] si'8 |
    do''2 mi''4 ~ |
    mi''4 r4 mi''8 mi''8 |
    la'4 la'4. re''8 |
    si'4 do''4. do''8 |
    la'4 la'4 la'8 si'8 |
    sold'4 la'4 ( sold'8 ) la'8 |
    sold'4\trill sold'4 r4 |
    la'2 la'8 la'8 |
    la'4. ( sold'8 ) sold'8 la'8 |
    la'2 <>^\markup\character Psyché
    r4 do''4 |
    r8 do''8 do''8 do''16 do''16 la'8 la'16 la'16 fad'8\trill fad'16 sol'16 |
    sol'8 sol'16 si'16 si'16 si'16 si'16 re''16 sol'8 sol'16 la'16 fa'8 fa'16 mi'16 |
    mi'4\trill 
    \clef "vhaute-contre" <>^\markup\character L'Amour
    r16 do'16 do'16 do'16 sol4 sol8 sol16 do'16 |
    la4\trill la8 la16 la16 re'8 do'8 si8 la8 |
    sold4\trill sold16 r16 mi'8 si4 r16 si16 si16 do'16 |
    re'4 re'8 re'8 re'4 do'8 si8 |
    do'4 do'16 do'16 do'16 mi'16 do'8 do'16 do'16 |
    la4\trill r16 do'16 do'16 do'16 fa'8 fa'16 fa'16 re'8\trill re'16 re'16 |
    si4\trill si16 r16 re'8 re'8 re'8 mi'8 fa'8 |
    sol'4 mi'8 sol'8 do'4 do'8 si8 |
    do'4 r8 mi'8 do'8 do'8 do'8 do'8 |
    sol4 sol8 la8 sib4 sib8 la8 |
    la4\trill r16 do'16 do'16 do'16 dod'8.\trill dod'16 dod'16 dod'16 dod'16 re'16 |
    re'8 re'8 r8 fa'16 fa'16 re'8 re'16 re'16 sib8 sib8 |
    sold2\trill sold4 sold8 la8 |
    la4 r8 do'8 do'8 do'8 |
    si8 si8 si8 si8 si8 mi'8 |
    la4 la8 fa'8 re'8 re'8 re'8 re'8 |
    re'4. mi'8 do'4 do'8 do'16 si16 |
    si4.\trill mi'8 mi'8 re'8 re'8 mi'8 |
    dod'4. dod'8 fad'8 fad'8 fad'8 sol'8 |
    red'2\trill red'4 sol'4 |
    mi'8. mi'16 mi'8 fad'8 red'8.\trill mi'16 |
    mi'2. |
    <>^\markup Air r4 r la8 la8 |
    mi'4 mi'4 fa'4 |
    sol'4. sol'8 sol'8 la'8 |
    fa'8 mi'8 re'2\trill |
    do'2 r8 sol'8 |
    sol'4. sol'8 fa'8. mi'16 |
    fa'4. mi'8 re'4 |
    do'4 si4 la4 |
    sold2.\trill |
    sold2 mi'8 re'8 |
    dod'2\trill dod'4 |
    re'4 ( do'4 ) si8 do'8 |
    si4.\trill la8( sol4) |
    sol'4 mi'4. fa'8 |
    dod'2.\trill |
    re'4 re'4. dod'8 |
    re'2 fa'8 fa'8 |
    fa'2 mi'4 |
    re'4.\trill mi'8 do'4 |
    si4.\trill la8( sold4) |
    mi'4 si4 do'4 |
    re'2. |
    do'4 si4.\trill la8 |
    la2. |
  }
  \tag #'recit0 {
    \clef "vhaute-contre"
    R1 R2.*2 R1*4 R1 R2. R1*3 R1*7 R1*4 R2. R1 R1*3 R2.*4 R1 R1*4
    R2.*4 R1*2 R2.*2 <>^\markup\character L'Amour
    la'2 mi'8 mi'8 |
    fa'4 fa'4. sol'8 |
    mi'4 mi'4 fa'4 |
    re'4. re'8 re'8 re'8 |
    sol'4 sol'4 mi'4 |
    do'4 do'4 r4 |
    fa'4 fa'4 mi'4 |
    re'2 mi'8 fa'8 |
    mi'2\trill sol'4~ |
    sol'4 r4 sol'8 la'8 |
    fa'4 fa'4. fa'8 |
    fa'4 mi'4. mi'8 |
    mi'4 re'4 re'8 re'8 |
    re'8.[ do'16] do'4 ( si8 ) do'8 |
    si4\trill si4 r4 |
    do'2 re'8 mi'8 |
    si2 si8 do'8 |
    la2 r2 
    R1*6 R2. R1*7 R1 R2.*2 R1*4 R1 R2.*2 R2.*8 R2. R2. R2.*14
  }
>>