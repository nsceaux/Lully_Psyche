\key fa \major
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 s2.
\bar ".|:" s2.*7 \alternatives s2. s2. s2.*13
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.*3
\time 4/4 s1*4
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*11
\digitTime\time 2/2 \midiTempo#160 s1*2 \bar "|."
