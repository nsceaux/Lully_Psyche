Quoi, vous vous em -- ploy -- ez pour la fiè -- re Psy -- ché ;
Pour une in -- so -- len -- te mor -- tel -- le ?
Cet in -- di -- gne tra -- vail vous tient donc at -- ta -- ché,
et l’é -- poux de Ve -- nus se dé -- cla -- re contre el -- le ?

Et de -- puis quand, s’il vous plait, vi -- vons- nous
dans une a -- mi -- tié si par -- fai -- te, 
qu’il fail -- le que je m’in -- qui -- è -- te 
de tous vos ca -- pri -- ces ja -- loux ?
Il vous sied bien de vous mettre en co -- lè -- re.
Lors -- que j’é -- tais ja -- loux a -- vec plus de rai -- son,
vous en fai -- siez "-vous" une af -- fai -- re ?
Vous l’ê -- tes main -- te -- nant, et vous trou -- ve -- rez bon
qu’on ne s’en em -- ba -- ras -- se guè -- re.

Ah ! que l’a -- mour est promp -- te -- ment gué -- ri,
quand l’hy -- men a ré -- duit deux cœurs sous sa puis -- san -- ce !
- ce !
Que les du -- re -- tés de ma -- ri, 
aux ten -- dres -- ses d’a -- mant, ont peu de res -- sem -- blan -- ce !
Que les du -- re -- tés de ma -- ri, 
aux ten -- dres -- ses d’a -- mant, ont peu de res -- sem -- blan -- ce !

Vous con -- nais -- sez tou -- te la dif -- fé -- ren -- ce,
et de l’a -- mant et de l’é -- poux,
et nous sa -- vons le -- quel des deux, chez vous, 
a mé -- ri -- té la pré -- fé -- ren -- ce.
Je ne fais pour Psy -- ché que bâ -- tir un pa -- lais,
vous êtes en -- co -- re trop heu -- reu -- se :
Si j’é -- tais de na -- ture un peu plus a -- mou -- reu -- se,
vous me ver -- riez a -- do -- rer ses at -- traits ;
La ven -- gean -- ce se -- rait plus bel -- le ;
Mais je suis à ma forge oc -- cu -- pé nuit et jour ;
Je n’ai pas le loi -- sir de lui par -- ler d’a -- mour,
et je me borne à tra -- vail -- ler pour el -- le.

Je sais que par ces grands ap -- prêts, 
c’est à mon fils que vous cher -- chez à plai -- re ;
C’est lui qui le pre -- mier tra -- hit mes in -- té -- rêts,
il sau -- ra que je suis sa mè -- re.

L’A -- mour i -- ci nous a man -- dé ex -- près :
A -- che -- vons, a -- che -- vons ce qui nous reste à fai -- re.
A -- che -- vons, a -- che -- vons ce qui nous reste à fai -- re.