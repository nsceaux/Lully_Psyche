\clef "vbas-dessus" <>^\markup\character Venus
do''4 la'16 la'16 la'16 do''16 fa'8 fa'16 fa'16 sib'8 sib'16 do''16 |
re''4 re''16 re''16 re''16 fa''16 sib'8 sib'16 sib'16 |
la'8\trill la'8 r8 do''16 do''16 la'8 la'16 la'16 |
re''4 re''8 re''8 la'4 si'8. do''16 |
si'8\trill sol'16 sol'16 sol'8 sol'16 sol'16 do''8 do''16 do''16 re''8 mi''16 fa''16 |
mi''8\trill mi''8 
\clef "vhaute-contre" <>^\markup\character Vulcain
r16 do'16 do'16 do'16 sol8 sol16 la16 sib8 sib16 la16 |
la4\trill la16 la16 sib16 do'16 re'8 re'16 mi'16 |
dod'4\trill dod'8 mi'8 fa'8. fa'16 fa'16 mi'16 re'16 dod'16 |
si8\trill si8 sol'16 sol'16 fa'16 mi'16 re'8\trill re'16 mi'16 |
do'4 r16 mi'16 mi'16 sol'16 do'8 do'16 do'16 do'8 re'16 mi'16 |
fa'8 fa'16 do'16 do'16 do'16 do'16 re'16 mib'8 mib'16 mib'16 mib'8 mib'16 re'16 |
re'4\trill sib16 sib16 sib16 re'16 sib8 sib16 sib16 |
sol8\trill sol8 r8 sol'8 mi'8 mi'8 mi'8 mi'8 |
do'4. do'8 do'8 do'8 do'8 do'8 |
re'8. re'16 re'16 re'16 re'16 sol'16 mi'8 fa'8 fa'8 [ mi'8 ]|
fa'2. |
\clef "vbas-dessus" <>^\markup\character Venus
la'2 la'8 sib'8 |
do''4. do''8 sol'8 la'8 |
sol'4( fa'4 mi'8)\trill fa'8 |
mi'2\trill do''8 sib'8 |
la'4 la'4. re''8 |
si'4.\trill si'8 do''8 do''8 |
re''8 mi''8 mi''4 ( re''4\trill) |
do''2. |
do''4 mi''8 mi''8 mi''8 fa''8 |
re''4 re''4. mi''8 |
dod''2\trill dod''8 dod''8 |
re''2 re''8 la'8 |
sib'4. re''8 mib''8 re''8 |
do''8 sib'8 sib'4 ( la'4)\trill |
sol'4 sib'8 la'8 sib'8 do''8 |
re''4 re''4. mi''8 |
fa''4 la'4.\trill sib'8 |
do''2 re''8 mib''8 |
re''4.\trill sib'8 sib'8 la'16[ sol'16] |
la'8 sib'8 la'4( sol'4)\trill |
fa'4 
\clef "vhaute-contre" <>^\markup\character Vulcain
r8 la8 la8 sib8 |
do'4 do'8 do'16 do'16 re'8 mi'8 |
fa'8 fa'8 r16 fa'16 fa'16 fa'16 re'8 re'8 re'8 mi'8 |
dod'4.\trill la8 la8 la8 |
re'8 re'8 re'8 do'8 do'8 si8 |
si4\trill re'8 re'16 mi'16 do'4 do'8 do'16 si16 |
do'8 do'8 r8 sol'16 sol'16 mi'8 mi'16 mi'16 |
do'4 do'8 do'8 sol4 la8 sib8 |
la8.\trill do'16 la8 la16 la16 sib8 sib16 do'16 |
re'8 re'8 r8 fa'16 fa'16 re'8 re'16 re'16 |
mib'8 re'8 do'8 sib8 la8 sol8 |
fad4\trill fad8 r16 re'16 re'8 fad8 |
sol4 sol8 la8 sib4 la8\trill sib8 |
sol4 r8 sol'16 sol'16 re'8 re'16 re'16 sib8 re'8 |
sol8 sol8 r8 sib16 sib16 sib8 sib16 do'16 |
la4 la8 sib8 do'4 re'16 [ do'16 ] re'8 |
mi'4 r8 do'16 do'16 fa'8 fa'16 fa'16 |
re'8 re'8 re'8 do'8 sib8 la8 |
sol4 r8 do'8 re'8 mi'8 |
fa'8 mi'8 re'8 do'8 sib8 la8 sol4\trill |
fa4 
\clef "vbas-dessus" <>^\markup\character Venus
r8 fa'8 do''16 do''16 do''16 do''16 fa''8 fa''8 |
re''4\trill re''8 re''16 fa''16 sib'16 sib'16 sib'16 re''16 sol'8 sol'8 |
mi'4\trill mi'8 r16 do''16 do''8 do''8 sib'8 la'8 |
sib'8 re''8 sib'4 sib'8 sib'16 la'16 |
<>^\markup\italic { Venus rentre dans son char, & s’envole. }
la'4.\trill fa''16 fa''16 re''8 re''16 re''16 re''8 mi''8 |
do''8 do''8 
\clef "vhaute-contre" <>^\markup\character-text Vulcain aux Cyclopes
r8 sol8 sol8 la8 |
sib4. sib8 sib4 |
la4 sol8([ fa8]) sol4 |
la2 la8 la8 |
re'2 re'8 re'8 |
mi'4. mi'8 mi'8 mi'8 |
fa'8 sol'8 mi'2\trill |
re'2 la8 sib8 |
do'2 do'8 do'8 |
re'4. re'8 re'8 mi'8 |
fa'8 sol'8 la'4 ( sol'4)\trill |
fa'2 r2 |
R1 |
