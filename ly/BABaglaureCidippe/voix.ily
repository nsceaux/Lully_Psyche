\clef "vbas-dessus" <>^\markup\character Aglaure
r4 r8 sol'8 si'4 r8 si'8 |
do''4 r8 la'8 re''8 re''8 re''8 re''8 |
si'4\trill si'8 si'16 re''16 sol'8. sol'16 sol'16 fa'16 fa'16 mi'16 |
mi'4\trill mi'8 mi''8 do''8 do''8 do''8 do''8 |
la'4\trill la'4 re''8 re''16 re''16 la'8 si'8 |
do''4 do''8 mi''8 do''4 do''8 si'8 |
si'8\trill r16 si'16 mi''8 mi''16 mi''16 dod''8 dod''16 re''16 |
re''4 re''8 
\clef "vbas-dessus" <>^\markup\character Cidippe
la'8 fad'8 fad'16 fad'16 sol'8 sol'16 la'16 |
si'4 si'8 si'8 do''8 do''8 do''8 do''16 si'16 |
do''8 r16 mi''16 mi''16 mi''16 mi''16 sol''16 do''8 do''16 do''16 do''8\trill do''16 si'16 |
si'4\trill si'4
\clef "vbas-dessus" <>^\markup\character Aglaure
r8 si'8 si'8 si'8 |
do''8 si'8 la'8 sol'8 fad'8 fad'16 fad'16 si'8 si'16 si'16 |
sold'8.\trill mi''16 mi''8 mi''8 si'16 si'16 si'16 dod''16 re''8. mi''16 |
dod''8 dod''8
\clef "vbas-dessus" <>^\markup\character Cidippe
r16 mi''16 mi''16 mi''16 la'16 mi'16 mi'16 fad'16 sol'8. la'16 |
fad'8\trill la'16 la'16 la'8 si'16 do''16 re''8 si'16 si'16 si'8 do''16 re''16 |
mi''8 mi''8 r8 mi''8 do''8\trill do''16 si'16 la'8\trill la'16 si'16 |
sol'2. |
\clef "vbas-dessus" <>^\markup\character Aglaure
re''4 re''4 si'4 |
do''4 la'4. re''8 |
si'2\trill sol'4 |
r4 si'4 do''4 |
la'2 la'4 |
la'4 sol'8\trill[ fad'8] sol'4 |
fad'2.\trill |
fad'2.\trill |
la'4 la'4 si'4 |
do''4 re''8[ do''8] si'8[ la'8] |
mi''2 si'4 |
sold'2 sold'4 |
si'4 do''4 re''4 |
do''4.\trill si'8( la'4) |
do''4 do''4 re''4 |
si'4 do''8[ si'8 ] la'8[ sol'8] |
re''2 re''4 |
mi''2 do''4 |
la'4 si'4 do''4 |
si'4( la'2)\trill |
sol'4
\clef "vbas-dessus" <>^\markup\character Cidippe
si'8 re''8 sol'4 sol'8 sol'16 fad'16 |
fad'2.\trill |
sol'4 la'4 si'4 |
la'4. si'8 sol'4 |
re''2 re''4 |
si'4 do''4 re''4 |
mi''4 do''4.\trill si'8 |
la'2.\trill |
la'2.\trill |
la'4 la'4 si'4 |
do''4 do''4 ( si'8 ) do''8 |
si'2.\trill |
do''4 re''4 mi''4 |
re''4. mi''8 fa''4 |
mi''4.\trill re''8( do''4) |
si'4 do''4 re''4 |
la'4. si'8 sol'4 |
fad'2.\trill |
la'4 si'4 do''4 |
do''4. re''8 si'4 |
si'4( la'2)\trill |
sol'2. |
\clef "vbas-dessus" <>^\markup\character Aglaure
si'4 r8 si'8 fad'8 fad'16 fad'16 si'8 la'8 |
sold'4\trill r8 mi''8 si'8 si'8 dod''8 re''8 |
dod''4\trill r8 la'8 la'8 la'8 la'8 la'8 |
re''8. fad''16 re''8. re''16 re''8 dod''8 |
re''4 re''4
\clef "vbas-dessus" <>^\markup\character Cidippe
r8 re''8 |
si'4 si'4. do''8 |
la'2 sol'4 |
fad'2\trill re''8 do''8 |
si'4. si'8 do''8 re''8 |
mi''2 mi''4 |
si'4 si'4. dod''8 |
re''4 dod''4. re''8 |
re''2 r8 re''8 |
re''2. |
re''4 re''4. si'8 |
mi''4 mi''4. fad''8 |
red''4. dod''8( si'4) |
r4 si'4. si'8 |
do''8[( si'8] la'4.)\trill sol'8 |
sol'4( fad'4.)\trill mi'8 |
mi'2. |
mi''4 si'4. si'8 |
do''4. re''8 si'4 |
la'2\trill la'4 |
r4 re''4 si'4 |
do''2 do''4 |
r4 do''4 la'4 |
si'8 [ do''8 si'8 do''8 ]( re''4 )|
do''8 [ si'8 ] la'4.\trill sol'8 |
sol'2. |
\clef "vbas-dessus" <>^\markup\character Aglaure
r4 r8 re''8 sib'4 sib'8 sib'8 |
sol'4 
\clef "vbas-dessus" <>^\markup\character Cidippe 
sib'8 re''8 sib'4 sib'8 sib'8 |
sol'4 sol'8 sol'8 do''4 do''8 la'8 |
    