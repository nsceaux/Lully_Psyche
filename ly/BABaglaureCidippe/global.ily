\set Score.currentBarNumber = 18 \bar ""
\key sol \major
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*9
\digitTime\time 3/4 \midiTempo#160 s2. \bar ".|:" s2.*6
\alternatives s2. s2. s2.*12
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 s2. \bar ".|:" s2.*5
\alternatives s2. s2. s2.*13 \bar "||"
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 \midiTempo#160 s2.*2 \bar ".|:" s2.*7
\alternatives s2. s2. s2.*16 \bar "||"
\key re \minor \time 4/4 s1*3
