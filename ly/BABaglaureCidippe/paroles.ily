%% Aglaure
En -- fin, ma sœur, le ciel est a -- pai -- sé,
et le ser -- pent qui nous ren -- dait à plain -- dre,
va n’ê -- tre plus à crain -- dre.
Tout pour le sa -- cri -- fice est i -- ci dis -- po -- sé ;
Psy -- ché, pour l’of -- frir, va s’y ren -- dre.
%% Cidippe
Les peu -- ples, d’er -- reurs pré -- ve -- nus,
la nom -- maient une au -- tre Ve -- nus ;
Sur la Di -- vi -- ni -- té c’é -- tait trop en -- tre -- pren -- dre.
%% Aglaure
Ils en sont tous as -- sez pu -- nis
par les maux in -- fi -- nis 
que du ser -- pent nous a cau -- sé la ra -- ge.
%% Cidippe
Ne son -- geons plus a nos mal -- heurs pas -- sés,
le ser -- pent, en ces lieux, ne fait plus de ra -- va -- ge ;
ce sont des mal -- heurs ef -- fa -- cés.
%% Aglaure
A -- près un temps plein d’o -- ra -- ges,
quand le calme est de re - tour, - tour,
qu’a -- vec plai -- sir, d’un beau jour
on goû -- te les a -- van -- ta -- ges !
qu’a -- vec plai -- sir, d’un beau jour
on goû -- te les a -- van -- ta -- ges !
%% Cidippe
Tout suc -- cède à nos dé -- sirs ;
si des ri -- gueurs in -- hu -- mai -- nes
nous ont cau -- sé des sou -- pirs : - pirs :
on ne con -- naît les plai -- sirs 
qu’a -- près l’é -- preu -- ve des pei -- nes.
On ne con -- naît les plai -- sirs, 
qu’a -- près l’é -- preu -- ve des pei -- nes.
%% Aglaure
Mais d’où vient qu’a -- vec tant d’at -- traits,
Psy -- ché n’ai -- ma ja -- mais ?
Qui bra -- ve trop l’A -- mour doit crain -- dre sa co -- lè -- re.
%% Cidippe
Il est un fa -- tal mo -- ment, 
où l’o -- sier le plus sé -- vè -- re 
se rend aux vœux d’un a -- mant : Il - mant :
Et plus la bel -- le dif -- fè -- re,
plus elle ai -- me ten -- dre -- ment.
Et plus la bel -- le dif -- fè -- re,
plus elle ai -- me,
plus elle ai -- me ten -- dre -- ment.
%% Aglaure
Ly -- cas vient à nous.
%% Cidippe
Son vi -- sa -- ge nous mar -- "que u" -- ne vi -- ve dou -
