Cé -- lé -- brons, cé -- lé -- brons ce grand jour,
cé -- lé -- brons ce grand jour ;
Cé -- lé -- brons tous u -- ne fê -- te si bel -- le,
cé -- lé -- brons tous u -- ne fê -- te si bel -- le :
Que nos chants en tous lieux en por -- tent la mé -- moi -- re ;
\tag #'(vdessus vhaute-contre vtaille) {
  Que nos chants en tous lieux en por -- tent la mé -- moi -- re ;
  Qu’ils fas -- sent re -- ten -- tir,
  qu’ils fas -- sent re -- ten -- tir, re -- ten -- tir,
}
\tag #'vbasse {
  Qu’ils fas -- sent re -- ten -- tir le cé -- les -- te sé -- jour,
  Qu’ils fas -- sent re -- ten -- tir,
}
re -- ten -- tir,
qu’ils fas -- sent re -- ten -- tir,
re -- ten -- tir le cé -- les -- te sé -- jour,
le cé -- les -- te sé -- jour.
Qu’ils fas -- sent re -- ten -- tir le cé -- les -- te sé -- jour.
\tag #'(vhaute-contre vtaille vbasse) {
  Chan -- tons, ré -- pé -- tons tour à tour,
  qu’il n’est point d’â -- me si cru -- el -- le,
  qui tôt ou tard ne se rende à l’A -- mour.
  
}
Chan -- tons, ré -- pé -- tons, ré -- pé -- tons tour à tour,
qu’il n’est point d’â -- me si cru -- el -- le,
qui tôt ou tard ne se rende à l’A -- mour.
\tag #'(vhaute-contre vtaille vbasse) {
  Chan -- tons, ré -- pé -- tons tour à tour,
}
\tag #'vtaille {
  qu’il n’est point d’â -- me si cru -- el -- le,
  qui tôt ou tard ne se rende à l’a -- mour.
}
Chan -- tons, ré -- pé -- tons tour à tour,
ré -- pé -- tons tour à tour,
\tag #'(vhaute-contre vtaille vbasse) {
  qu’il n’est point d’â -- me si cru -- el -- le, si cru -- el -- le,
}
\tag #'vtaille {
  qui tôt ou tard ne se rende à l’A -- mour.
}
Qu’il n’est point d’â -- me si cru -- el -- le, si cru -- el -- le,
qui tôt ou tard, qui tôt ou tard ne se rende à l’A -- mour.
Qui tôt ou tard ne se rende à l’A -- mour.
