\clef "vdessus" r4 r8 re''16 re''16 dod''2 |
r4 r8 fa''16 fa''16 re''4 re''8 sol''8 |
mi''2\trill r4 r8 mi''16 mi''16 |
fa''4 do''8. do''16 re''2 |
r8 re''8 re''8 re''8 do''4 do''8 do''8 |
do''4 do''8 do''8 do''4 ( si'4) |
do''8. do''16 do''8 do''8 re''4 re''8 mi''8 |
fa''4 fa''8 fa''8 re''4. ( mi''8) |
dod''2\trill r4 dod''8. dod''16 |
re''4 la'8. la'16 si'4. si'8 |
mi''8. fa''16 sol''8 fa''16[ mi''16] mi''4( re''4)\trill |
do''4 <<
  { \voiceOne mi''8 mi''8 fa''4 re''8 do''8 |
    si'4. si'8 do''8*3/2 do''8*1/2 do''8 do''8 |
    do''4( si'4\trill) la'4. \oneVoice }
  \new Voice \with { autoBeaming = ##f } \sugNotes {
    \voiceTwo do''8 do'' do''4 si'8 la' |
    sold'4. sold'8 la'8*3/2 la'8*1/2 la'8 la' |
    la'4( sold') la'4.
  }
>> la'8 |
la'8. la'16 la'8 la'8 si'4. re''8 |
mi''8. mi''16 mi''8 mi''8 fa''4 fa''8 mi''8 |
fa''4 fa''8 mi''8 fa''4. fa''8 |
re''8. re''16 re''8 do''8 re''4 re''8 do''8 |
re''4 fa''8. mib''16 re''4 re''8. do''16 |
do''4 do''8. sib'16 la'4 la'8. sol'16 |
sol'4. do''8 do''8. do''16 do''8 do''8 |
re''4 re''8 sol''8 mi''4\trill mi''8 fa''8 |
fa''2. |
R2.*16 |
r4 r4 re''4 |
do''2 do''8. do''16 |
do''2 fa''8. fa''16 |
fa''4 mi''4.\trill re''8 |
dod''2.\trill |
re''4 re''4 re''4 |
mi''2 mi''4 |
fa''2 re''4 |
mi''2 mi''4 |
mi''4 mi''4. fa''8 |
re''4 re''4. re''8 |
mi''8 [ re''8 ] dod''4.\trill re''8 |
re''2 r4 |
R2.*14 |
r4 r4 dod''4 |
re''2 re''8. re''16 |
re''4 re''4. re''8 |
mi''2 mi''8. mi''16 |
fa''4 fa''4. fa''8 |
mi''2. |
R2.*10 |
dod''4 dod''4 dod''4 |
re''2 re''4 |
mi''2 mi''4 |
fa''2 re''4 |
mi''2 fa''4 |
re''2( mi''4) |
dod''2.\trill |
re''4 re''4. do''8 |
si'2. |
do''4 do''4. do''8 |
la'4 la'4. re''8 |
do''4 si'4. la'8 |
la'2 r4 |
fa''4 fa''4. fa''8 |
fa''4 mi''4.\trill re''8 |
re''4 dod''4. re''8 |
re''2. |
