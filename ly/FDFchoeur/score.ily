\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics << 
        \global \includeNotes "voix-dessus" 
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-haute-contre" 
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-taille" 
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-basse" 
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff <<
        \global \includeNotes "basse-continue"
        { s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4 s2.*2\pageBreak
          s2.*9\pageBreak
          s2.*7\pageBreak
          s2.*9\pageBreak
          s2.*9\pageBreak
          s2.*8\pageBreak
          s2.*8\pageBreak
          s2.*9\pageBreak
          s2.*8\pageBreak }
      >>
    >>
  >>
  \layout {
    \context {
      \Score
      \override NonMusicalPaperColumn #'line-break-permission = ##f
      \override NonMusicalPaperColumn #'page-break-permission = ##f
    }
  } 
  \midi { }
}
