\piecePartSpecs
#`((basse-continue #:system-count 3)
   (basse #:system-count 3)
   (quinte)
   (taille #:system-count 3)
   (haute-contre #:system-count 3)
   (dessus)
   (silence #:on-the-fly-markup , #{ \markup\tacet#35 #}))
