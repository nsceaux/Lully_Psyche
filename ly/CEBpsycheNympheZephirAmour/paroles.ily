\tag #'(recit1 basse) {
  Quels a -- gré -- a -- bles sons ont frap -- pé mes o -- reil -- les !

  At -- tends en -- cor, Psy -- ché, de plus gran -- des mer -- veil -- les.
  Tout est dans ces beaux lieux, sou -- mis à tes ap -- pas.
  Pour ren -- dre ton bon -- heur du -- ra -- ble,
  sou -- viens- toi seu -- le -- ment que lors -- qu'on est ai -- ma -- ble,
  c'est un cri -- me de n'ai -- mer pas.
  Sou -- viens- toi seu -- le -- ment que lors -- qu'on est ai -- ma -- ble,
  c'est un cri -- me, c'est un cri -- me de n'ai -- mer pas.

  Est- ce qu'ai -- mer est né -- ces -- sai -- re ?

  D'un jeu -- ne cœur, c'est la plus douce af -- fai -- re.
}
\tag #'(recit1 recit0 basse) {
  Ai -- mez, ai -- mez, il n'est de beaux ans 
  que dans l'a -- mou -- reux em -- pi -- re.
}
\tag #'(recit1 basse) {
  Qui laisse é -- chap -- per le temps,
  quel -- que fois trop tard sou -- pi -- re.
}
\tag #'(recit0 basse) {
  Qui laisse é -- chap -- per le temps,
  quel -- que fois trop tard sou -- pi -- re.
}
\tag #'(recit1 recit0 basse) {
  Ai -- mez, ai -- mez, il n'est de beaux ans 
  que dans l'a -- mou -- reux em -- pi -- re.

  Et qui veut- on me faire ai -- mer ?

  Un Dieu qui se pré -- pare à t'as -- su -- rer lui- mê -- me
  de son a -- mour ex -- trê -- me.

  Qui se -- rait donc ce Dieu que j'au -- rais su char -- mer ?

  C'est moi, Psy -- ché, c'est moi qui me rends à vos char -- mes.

  S'il est ain -- si, pa -- rais -- sez en ce lieu.

  Le des -- tin vous dé -- fend de me voir com -- me Dieu,
  ou ma perte aus -- si -- tôt vous coû -- te -- ra des lar -- mes.

  Et le moy -- en d'ai -- mer ce qu'on ne voit ja -- mais ?

  Pour me mon -- trer à vous je vais dans ce pa -- lais, 
  pren -- dre d'un mor -- tel la fi -- gu -- re.

  Ah ! ve -- nez donc, n'im -- por -- te sous quels traits,
  pour -- vu qu'en vous voy -- ant mon es -- prit se ras -- su -- re.
}
