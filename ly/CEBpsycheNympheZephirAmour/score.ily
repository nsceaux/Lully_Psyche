\score {
  \new ChoirStaff <<
    \new Staff \withLyrics << 
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics << 
      \global \keepWithTag #'recit0 \includeNotes "voix"
    >> \keepWithTag #'recit0 \includeLyrics "paroles"
    \new Staff << 
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
