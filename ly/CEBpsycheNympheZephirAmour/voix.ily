<<
  \tag #'(recit1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    r2 re''8 re''16 re''16 re''8 la'8 |
    sib'4 sib'8 re''8 sib'4 sib'8 la'8 |
    la'8\trill la'8 
    \ffclef "vdessus" <>^\markup\character-text Nymphe cachée
    r8 fa''8 re''8 re''8 re''8 re''8 |
    si'4\trill si'8 si'8 mi''4 re''16[ dod''16] re''8 |
    dod''2\trill dod''2 |
    r2 r4 re''4 |
    la'4 la'4 la'4 sib'4 |
    do''2. do''4 |
    sib'4 la'4 sol'4.\trill fa'8 |
    fa'2 r4 do''4 |
    dod''4 dod''4 dod''4. re''8 mi''4 mi''4 |
    fa''4 re''4 fa''4 mi''4 |
    re''2 si'4. si'8 |
    sold'2. la'4 |
    si'4 do''4 re''4 si'4 |
    do''4 la'4 mi''4 fa''4 |
    re''2 re''4 do''4 |
    si'2.\trill la'4 |
    la'2 do''4 do''4 |
    do''2 sib'4. la'8 |
    sib'2. sib'4 |
    sib'4 la'4 la'4.*5/6 sol'16[ fa'16 sol'16] |
    la'4 mi'4 mi''4 fa''4 |
    re''4 re''4 sol''4 mi''4 |
    dod''2 la'4 fa''4 |
    fa''2 ( mi''4.\trill) re''8 |
    re''1 |
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    fa''4 fa''8 re''8 si'8.\trill si'16 si'8 do''8 |
    do''8 do''8 
    \ffclef "vhaute-contre" <>^\markup\character-text Zephir caché
    r8 mi'8 mi'8 fa'8 |
    re'8 re'8 re'8 re'8 mi'8 fa'8 |
    dod'4 dod'4 <>^\markup\character-text Deux Zephir cachés
    r8 mi'8 | \noBreak
    fa'2 fa'4 |
    re'2 re'4 |
    sol'4. la'8 fa'4 |
    mi'2. |
    la'4. la'8 sol'8 fa'8 |
    mi'8 fa'8 fa'4 ( mi'4\trill) |
    re'2 r8 re'8 |
    la4 la8 la8 la8 si8 |
    do'2 do'8 do'8 |
    re'4. re'8 mi'4 ~|
    mi'8 fa'8 fa'4( mi'4) |
    fa'2
  }
  \tag #'recit0 {
    \ffclef "vtaille" R1*10 R1. R1*17 R2.*2
    r4 r \ffclef "vtaille" r8 dod'8 |
    re'2 la4 |
    sib2 si4 |
    mi'4. fa'8 re'4 |
    dod'2. |
    fa'4. fa'8 mi'8 re'8 |
    dod'8 re'8 re'4 ( dod'4) |
    re'2 r4 |
    R2.*4 |
    r4 r
  }
>>
<<
  \tag #'recit1 { r4 R2.*4 r4 r }
  \tag #'(recit0 basse) {
    \tag #'basse \ffclef "vtaille"
    r8 la8 |
    si4 si8 mi'8 dod'8 re'8 |
    mi'2 mi'8 mi'8 |
    fa'4. mi'8 re'4 |
    do'4 si2\trill |
    la2
    \tag #'basse \ffclef "vhaute-contre"
  }
>>
<<
  \tag #'(recit1 basse) {
    r8  mi'8 |
    fa'2 fa'4 |
    re'2 re'4 |
    sol'4. la'8 fa'4 |
    mi'2. |
    la'4. la'8 sol'8 fa'8 |
    mi'8 fa'8 fa'4 ( mi'4\trill) |
    re'2
  }
  \tag #'recit0 {
    r8 dod'8 |
    re'2 la4 |
    sib2 si4 |
    mi'4. fa'8 re'4 |
    dod'2.\trill |
    fa'4. fa'8 mi'8 re'8 |
    dod'8 re'8 re'4 ( dod'4 )|
    re'2 
  }
>>
<<
  \tag #'(recit1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    re''8 re''16 re''16 si'16 si'16 si'16 si'16 |
    sold'4\trill 
    \ffclef "vhaute-contre" <>^\markup\character-text Zephir caché
    r8 si8 mi'8 mi'8 mi'8 mi'8 |
    dod'8\trill dod'8 dod'8 dod'8 re'8 mi'8 |
    fa'4 fa'16 mi'16 re'16 do'16 si8 do'8 |
    la8 la8 
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    r16 dod''16 dod''16 dod''16 dod''8 re''8 |
    mi''4 la'8 la'16 la'16 la'8 mi'8 |
    fa'8
    \ffclef "vbas-dessus" <>^\markup\character-text L'Amour caché
    fa'' re'' re'' la'4 r8 fa'' |
    mi''4 mi''8 fa'' re''4 re''8 mi'' |
    dod''4\trill dod''8
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    la'8 la'8 re''8 |
    si'4 sol'8 sol'8 do''4 do''8 re''8 |
    mi''4
    \ffclef "vbas-dessus" <>^\markup\character-text L'Amour seul
    do''8 do'' sol'4 sol'8 la' |
    sib'4 sib'8 do'' re''4 re''8 mi'' |
    fa''4 la'8 la' la'4 sib'8 do'' |
    re'' sib' sib' sib' sib'4( la'8) sib' |
    la'4 la'8
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    r8 do''8 do''16 do''16 do''8 la'8 |
    sib'4 sib'8 sib'16 la'16 sol'8. fa'16 |
    mi'4\trill
    \ffclef "vbas-dessus" <>^\markup\character-text L'Amour caché
    la'8 la'16 la' re''8 la' |
    sib' re'' sib' sib' sib' sib' |
    sol'4 do''8 do'' do'' re'' |
    sol'2 sol'4 la' |
    fa' fa'8 r
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    fa''4 r8 do''16 do''16 |
    la'4 r8 fa''8 re''8 re''8 re''8 re''8 |
    si'4. si'8 mi''8 si'8 do''8 re''8 |
    dod''4\trill la'8 la'8 re''4. re''16 dod''16 |
    re''2 re''4 r4 |
  }
  \tag #'recit0 {
    r2 R1 R2.*4 R1*2 R2. R1*6 R2.*4 R1*6
  }
>>
