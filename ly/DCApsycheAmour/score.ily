\score {
  \new StaffGroupNoBar <<
    \new Staff \withLyrics <<
      { s2.*4 s1*2
        <>^\markup\italic\override #'(line-width . 50) \wordwrap {
          Psyché lève le rideau qui ferme l’alcove, & l’on voit
          l’Amour endormi sous la figure d’un enfant.
        }
      }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
