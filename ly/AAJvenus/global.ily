\key do \major
\time 4/4 \midiTempo#80 s1*14
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2 \bar "|."
