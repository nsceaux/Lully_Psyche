\clef "dessus" do''4 fa''2 |
mi''2 fa''8\trill mi''16 fa''16 |
sol''2 la''4 |
re''2\trill re''4 |
do''4 fa''2 |
mi''2 fa''8\trill mi''16 fa''16 |
sol''2 la''4 |
re''2.\trill |
sol''4 mi''4.\trill re''16 mi''16 |
fa''2. |
mi''4 fa''8 mi''8 re''8 do''8 |
si'2 do''8\trill si'16 do''16 |
re''4. do''8 re''8 mi''8 |
mi''2\trill re''4 |
sol''4 mi''4.\trill re''16 mi''16 |
fa''2. |
mi''4 fa''8 mi''8 re''8 do''8 |
si'4.\trill la'16 si'16 do''4 ~ |
do''8 re''8 re''4.\trill do''8 |
do''2. |
