\tag #'(silene basse) \clef "vhaute-contre"
\tag #'satyre1 \clef "vtaille"
\tag #'satyre2 \clef "vbasse"
\setMusic #'trio <<
  \tag #'(silene basse) {
    \tag #'basse \clef "vhaute-contre"
    r4 r8 <>^\markup\character Silene fa'8 do'4 |
    re'2 re'8 do'8 |
    sib4. ( la8 ) sib4 |
    la2 la4 |
    r8 do'8 do'4 fa'4 |
    re'2 sol'4 |
    mi'2 fa'4 |
    sol'2. |
    r8 do'8 re'4 mi'4 |
    fa'2. |
    r8 re'8 mi'4 fa'4 |
    sol'2 fa'8 [ mi'8] |
    mi'4( re'4.\trill) do'8 |
    do'4. do'8 re'4 |
    mi'4. mi'8 mi'4 |
    fa'4. mi'8 ( re'4) |
    mi'2 mi'4 |
    R2.*2 |
    r8 fa'8 fa'4 mib'4 |
    re'4. re'8 re'8 mi'8 |
    fa'2 sol'4 |
    fa'4( mi'4.\trill) fa'8 |
    fa'2
  }
  \tag #'satyre1 {
    R2.*7 |
    r8 mi8 fa4. sol8 |
    la8 la8 si4 dod'4 |
    re'2. |
    r8 si8 do'4 re'4 |
    mi'2 re'8 [ do'8 ]|
    do'4 ( si4. ) do'8 |
    do'4. mi8 fa4 |
    sol4. sol8 sol4 |
    la4. sol8 ( fa4 )|
    sol2 sol4 ~|
    sol8 do'8 do'4 sib4 |
    la2 la4 |
    sib2 do'4 |
    fa4. fa8 sib8 sib8 |
    la2 sib4 |
    la4( sol4.\trill) fa8 |
    fa2
  }
  \tag #'satyre2 {
    R2.*7 |
    r8 do8 re4. mi8 |
    fa2 mi4 |
    re2 re4 |
    sol8 sol8 sol4 fa4 |
    mi2 fa4 |
    sol4 ( sol,4. ) do8 |
    do2 r4 |
    R2.*3 |
    r8 do8 re4 mi4 |
    fa8 fa8 fa4 mib4 |
    re2 la,4 |
    sib,2 sol,4 |
    re2 sib,4 |
    do2 ~ do8 fa,8 |
    fa,2
  }
>>
\keepWithTag #'(silene basse satyre1 satyre2) \trio
<<
  \tag #'silene {
    r4 R2.*15
  }
  \tag #'(satyre1 basse) {
    \tag #'basse \ffclef "vtaille"
    <>^\markup\character Permier Satyre la8 sib8 | \noBreak
    do'4 dod'4. dod'8 |
    re'2 re'4 |
    r4 sol'4 mi'4 |
    dod'4 la4. re'8 |
    re'4 ( dod'4. si16 [ dod'16]) |
    re'4 fa'4. la8 |
    sib4 si4. si8 |
    do'2 ( si4) |
    <<
      \tag #'basse { do'8 s s2 s2.*6 }
      \tag #'satyre1 {
        do'2 r4 |
        R2.*6 |
      }
    >>
  }
  \tag #'(satyre2 basse) {
    <<
      \tag #'basse { s4 s2.*8 s8 \ffclef "vbasse" }
      \tag #'satyre2 { r4 | R2.*8 | r8 }
    >> <>^\markup\character Deuxième Satyre do'8 do'4 sib4 |
    la2 sib8 [ la8 ]|
    la4( sol4.\trill) fa8 |
    fa8 fa8 fa4. sol8 |
    mi4. fa8 [ mi8 fa8 ]|
    sol4 ( sol,4. ) do8 |
    do2. |
  }
>>
\trio
<<
  \tag #'silene {
    r4 | R2.*17
  }
  \tag #'(satyre1 basse) {
    \tag #'basse \ffclef "vtaille"
    <>^\markup\character Permier Satyre fa'4 |
    re'4. re'8 sol'8 fa'8 |
    mi'2 r8 la'8 |
    fad'4.\trill re'8 mib'8 [ re'8] |
    do'8 [ sib8 ] sib4 ( la4\trill) |
    sol2 sib4 |
    la4. la8 sib8 do'8 |
    re'2 re'4 |
    mi'4. sol'8 la'8 [ sol'8] |
    fa'8[ mi'8 ] mi'4 ( re'4\trill) |
    <<
      \tag #'basse { do'8 s s2 }
      \tag #'satyre1 {
        do'2 r4 |
        R2.*7 |
      }
    >>
  }
  \tag #'(satyre2 basse) {
    <<
      \tag #'basse { s4 s2.*9 s8 \ffclef "vbasse" }
      \tag #'satyre2 { r4 | R2.*9 | r8 }
    >> <>^\markup\character Deuxième Satyre do'8 sol4. la8 |
    sib4. sib8 re4 |
    mib8 [ fa16 sol16 ] fa4. fa8 |
    sib,4. re'8 re'8 do'8 |
    sib4. sib8 sib8 la8 |
    sol4. fa8 mi4 |
    fa8 [ mi16 re16 ] do4. fa8 |
    fa2. |
  }
>>
\trio

