\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'silene \includeNotes "voix" 
    >> \keepWithTag #'silene \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'satyre1 \includeNotes "voix" 
    >> \keepWithTag #'satyre1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'satyre2 \includeNotes "voix" 
    >> \keepWithTag #'satyre2 \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}
