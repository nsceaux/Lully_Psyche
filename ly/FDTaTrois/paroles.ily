\setMusic #'trio {
  \tag #'(silene basse) {
    Vou -- lez- vous des dou -- ceurs par -- fai -- tes ?
    Ne les cher -- chez qu’au fond des pots.
    Ne les cher -- chez, ne les cher -- chez qu’au fond des pots.
    Vou -- lez- vous des dou -- ceurs par -- fai -- tes,
    Ne les cher -- chez, ne les cher -- chez qu’au fond des pots.
  }
  \tag #'satyre1 {
    Ne les cher -- chez, ne les cher -- chez,
    ne les cher -- chez qu’au fond des pots.
    Vou -- lez- vous des dou -- ceurs par -- fai -- tes,
    Ne les cher -- chez qu’au fond des pots.
    Ne les cher -- chez qu’au fond des pots.
  }
  \tag #'satyre2 {
    Ne les cher -- chez qu’au fond des pots.
    Ne les cher -- chez qu’au fond des pots.
    Ne les cher -- chez, ne les cher -- chez qu’au fond des pots,
    qu’au fond des pots.
  }
}
\keepWithTag #'(silene basse satyre1 satyre2) \trio
\tag #'(satyre1 basse) {
  Les gran -- deurs sont su -- jet -- tes à cent pei -- nes se -- crè -- tes,
  à cent pei -- nes se -- crè -- tes.
}
\tag #'(satyre2 basse) {
  L’a -- mour fait per -- dre le re -- pos.
  L’a -- mour fait per -- dre le re -- pos.
}
\trio
\tag #'(satyre1 basse) {
  C’est là que sont les ris, les jeux, les chan -- son -- net -- tes.
  c’est là que sont les ris, les jeux, les chan -- son -- net -- tes.  
}
\tag #'(basse satyre2) {
  C’est dans le vin qu’on trou -- ve les bons mots.
  c’est dans le vin, c’est dans le vin qu’on trou -- ve les bons mots.
}
\trio
