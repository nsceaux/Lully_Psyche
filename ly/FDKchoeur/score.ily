\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
    \new StaffGroup <<
      \new Staff << <>^"Tompettes" \global \includeNotes "trompette" >>
      \new Staff << <>_"Timbales" \global \includeNotes "timbales" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \includeNotes "voix-dessus" 
      >> \includeLyrics "paroles"
      \new Staff \withLyrics << 
        \global \includeNotes "voix-haute-contre" 
      >> \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-taille"
      >> \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "voix-basse"
      >> \includeLyrics "paroles"
    >>
    \new StaffGroup <<
      \new Staff <<
        \global \includeNotes "basse-continue"
        \includeFigures "chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { }
}
