\score {
  <<
    \new Staff \with { \tinyStaff } \withLyrics <<
      \global \includeNotes "voix-dessus" 
    >> { \set fontSize = -2 \includeLyrics "paroles" }
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff \with {
        instrumentName = \markup\center-column { Trompettes en Ré }
      } <<
        \transpose re do \global
        \transpose re do { \includeNotes "trompette" }
        { s4 s2.*14 s1*2 <>^"Trompette en ré" }
      >>
      \new Staff \with { instrumentName = "Timbales" } <<
        \global \includeNotes "timbales"
        { s4 s2.*14 s1*2 <>^"Timbales" s1*11 s2.\break }
      >>
    >>
  >>
  \layout { }
}
