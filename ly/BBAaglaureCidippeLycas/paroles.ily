\tag #'(recit1 basse) {
  - leur…
  
  Ah, prin -- ces -- ses !

  De quel mal -- heur ce sou -- pir est- il le pré -- sa -- ge ?

  I -- gno -- rez- vous en -- cor le des -- tin de Psy -- ché ?

  Qu’a -- vons- nous à crain -- dre pour el -- le ?

  La dis -- grâ -- ce la plus cru -- el -- le,
  dont vous puis -- siez ja -- mais a -- voir le cœur tou -- ché.
  Tan -- dis que cha -- cun en sou -- pi -- re,
  el -- le seule i -- gno -- re son sort ;
  et c’est i -- ci qu’on lui va di -- re,
  que le ciel ir -- ri -- té
  la con -- damne à la mort.
}
\tag #'(recit3 recit2 basse) {
  À la mort !
  et le roi n’y met -- trait point d’ob -- sta -- cle ?
}
\tag #'(recit1 basse) {
  Le roi d’a -- bord nous a ca -- ché l’o -- ra -- cle ;
  mais mal -- gré- lui le grand prêtre a par -- lé.
  Ah ! Pour -- quoi n’a- t’il pu se tai -- re ?
  Voi -- ci ce qu’il a ré -- vé -- lé,
  et l’ar -- rêt qui nous dé -- ses -- pè -- re.
  Vous al -- lez voir au -- gmen -- ter les mal -- heurs 
  qui vous ont coû -- té tant de pleurs,
  si Psy -- ché sur le Mont, pour ex -- pi -- er son cri -- me,
  n’at -- tend que le ser -- pent la pren -- ne pour vic -- ti -- me.

  Et Psy -- ché ne sait rien de ce fu -- neste ar -- rêt ?

  Pour se ren -- dre Ve -- nus pro -- pi -- ce,
  el -- le croit n’a -- voir in -- té -- rêt 
  qu’à ve -- nir en ces lieux,
  of -- frir un sa -- cri -- fi -- ce.

  Voi -- là l’ef -- fet de ce nom de Ve -- nus, 
  on trai -- tait Psy -- ché d’im -- mor -- tel -- le.

  C’est de là que nos maux et les siens sont ve -- nus :
  qui croi -- rait que ce fut un cri -- me d’ê -- tre bel -- le ?
}
\tag #'(recit2 recit3 basse) {
  Ah ! qu’il est dan -- geu -- reux 
  de trou -- ver un sort heu -- reux
  dans une in -- jus -- te lou -- an -- ge ! 
  "- an" -- ge !
  En vain on veut se fla -- ter, 
  tôt ou tard le ciel se ven -- ge,
  quand on o -- se l’i -- ri -- ter.
  Tôt ou tard le ciel se ven -- ge,
  quand on o -- se,
  quand on o -- se l’i -- ri -- ter.
}
\tag #'(recit1 basse) {
  Vo -- yez com -- me cha -- cun, re -- gret -- tant la prin -- ces -- se,
  a -- ban -- don -- ne son cœur à l’en -- nui qui le pres -- se.
}
\tag #'recit1 {
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  On ne peut trop ver -- ser de pleurs.
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  On ne peut trop ver -- ser de pleurs.
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop,
  on ne peut trop ver -- ser de pleurs.
}
\tag #'(recit2 recit3 basse) {  
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  On ne peut trop ver -- ser de pleurs.
  Pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  On ne peut trop ver -- ser de pleurs.
  Pleu -- rons, pleu -- rons ; en de si grands mal -- heurs,
  on ne peut trop ver -- ser de pleurs.
  On ne peut trop ver -- ser de pleurs.
}
