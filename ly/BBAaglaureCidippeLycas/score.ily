\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics << 
      \global \keepWithTag #'recit3 \includeNotes "voix"
    >> \keepWithTag #'recit3 \includeLyrics "paroles"
    \new Staff \withLyrics << 
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff \withLyrics << 
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff << 
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*3 s2. s1*2 s2. s1 s2. s1*2 s1 s2. s1*3 s2. s1
        \break
        s1 s2.*3 s1*5 s1*2 s1*3 s2. 
        s1 s2.*3 s1*3 s2. s1*2 s2. s1 s2.*2 s1
        \break
        s1*6 s1 s1 s1*11
        \break
        s1*5
        \break
        s1*31
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
