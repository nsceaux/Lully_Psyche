<<
  \tag #'(recit1 basse) {
    \clef "vbas-dessus"
    fad'4\trill
    \clef "vbasse-taille" <>^\markup\character Lycas
    re'4 r8 fad8 |
    sol4 sol4 
    \clef "vbas-dessus" <>^\markup\character Aglaure
    r8 re''8 re''8 re''8 |
    mib''4 do''8 do''8 do''8. sib'16 sib'8 sib'16 la'16 |
    la'8\trill la'8 
    \clef "vbasse-taille" <>^\markup\character Lycas
    r16 do'16 do'16 do'16 la8 do'8 |
    fa4 fa8 mib8 mib4 mib8 re8 |
    re4\trill 
    \clef "vbas-dessus" <>^\markup\character Cidippe
    re''16 re''16 re''16 fa''16 sib'8 sib'16 fa'16 |
    sol'4 sol'4 
    \clef "vbasse-taille" <>^\markup\character Lycas
    r4 mib'8 mib'8 |
    mib'4 mib'8 re'8 re'4. mib'8 |
    si4\trill si8 sol8 sol8 sol8 sol8 sol8 |
    do'8 mib'8 si8\trill si8 si8. do'16 |
    do'4 r8 do8 sol8 sol16 sol16 sol8 sol16 la16 |
    sib8 sib8 r8 sib16 sib16 sib8. sib16 sib8 sib16 la16 |
    la4\trill fa8 fa16 fa16 sib4 sib8 sib16 re'16 |
    sol4 sol8 sib16 sib16 mib'8 mib'16 sol16 |
    la4 fa8 fa8 sib4 sib8 la8 |
    <<
      \tag #'recit1 { sib2 r2 | R2. | r4 r8 }
      \tag #'basse {
        sib4. s8 s2 | s2. | s4. \clef "vbasse-taille"
      }
    >> <>^\markup\character Lycas do'8 do'8 do'8 |
    la8 la8 la8 la8 mi8 la8 |
    fad4\trill fad4 re'4 re'8 fad8 |
    sol4 sol8 re8 fa4 fa8 mi8 |
    mi4\trill do'8 r16 do'16 sol8 sol16 sol16 sol8 do'8 |
    la4\trill la8 re'8 si8 si16 si16 si8 dod'16 re'16 |
    dod'4 r8 la16 la16 re'4 re'16 re'16 re'16 dod'16 |
    re'2 re'2 |
    r4 sib4 sib4 sib4 |
    sib4 sib8 sib8 si4 si8 do'8 |
    do'4 sol16 sol16 sol16 la16 sib4 sib8 do'8 |
    la4\trill re8 re8 la4 la8 la8 |
    re'8. la16 la8 sib8 do'8 la8 |
    sib4 sib8 re'8 re'8. re'16 re'8 re'8 |
    mib'8. mib'16 la8\trill la8 la8 sib8 |
    sol4 sol8 
    \clef "vbas-dessus" <>^\markup\character Cidippe
    re''16 re''16 sib'8 sib'16 re''16 |
    sol'4 sol'8 sol'16 sol'16 do''8 sol'8 |
    la'4 
    \clef "vbasse-taille" <>^\markup\character Lycas
    fad8 fad8 sol8 sol8 sol8 sol16 sol16 |
    mi8\trill mi8 r8 mi16 mi16 fa8. fa16 sol8 la16 sib16 |
    la4\trill la8 sib8 do'4 do'8 do'8 |
    re'8 sib8 sol4 sol8 sol16 la16 |
    fa8 fa8
    \clef "vbas-dessus" <>^\markup\character Aglaure
    r16 do''16 do''16 do''16 la'8 la'16 la'16 sib'8 sib'16 sib'16 |
    sol'4 mib''16 mib''16 mib''16 mib''16 si'4 si'8 do''8 |
    do''4 do''8 
    \clef "vbas-dessus" <>^\markup\character Cidippe
    mib''16 sol''16 mib''8 mib''16 mib''16 |
    do''4 do''8 do''8 la'4 la'8 la'8 |
    fad'4 r8 re''16 re''16 fad'8 fad'16 fad'16 |
    sol'8 la'8 sib'8 sib'8 sib'8 do''8 |
    re''2 re''4 r4 |
    <<
      \tag #'recit1 { R1*19 | \clef "vbasse-taille" r4 }
      \tag #'basse { s1*19 s4 \clef "vbasse-taille" }
    >> <>^\markup\character Lycas
    r8 re'8 sib4 sib8 sib16 re'16 |
    sol4 sol8 re8 mib4 mib8 fa8 |
    re8\trill re8 r8 sib16 sib16 sol4 sol8 sol8 |
    mi4\trill mi8 mi8 la4 la8 la8 |
    fad2\trill fad4 r4 |
    \tag #'recit1 {
      <>^\markup\character Lycas
      r2 r4 sol4 |
      sol2. sol4 |
      fad2. fad4 |
      sol4 sol4 sol4. sol8 |
      re4 re4 re4 mib4 |
      do4. do8 do4. sib,8 |
      sib,2. sib4 |
      si2. si4 |
      do'2. do'4 |
      la4 la4 la4 ( sol8 ) la8 |
      sib4 sib4 sib4 la4 |
      sol4 fa4 mi4. mi8 |
      fa4 fa4 fa4 fa4 |
      mib4 mib4 mib4 re4 |
      do2. do'4 |
      do'2. do'4 |
      sib2. sib4 |
      fad4 fad4 sol8[ fad8] sol4 |
      re4 re'4 re'4 do'4 |
      sib4 la4 sol4 fa4 |
      mib4 mib4 mib4 re4 |
      do4 do4 do4. do8 |
      re2. re'4 |
      re'2. re'4 |
      sib2. sib4 |
      mib'4 mib'4 mib'4 re'4 |
      do'4 do'4 do'4 do'4 |
      re'4 re'4 re'4 sol4 |
      fad2. sol4 |
      re2. re4 |
      sol,1 |
    }
  }
  \tag #'recit2 {
    \clef "vbas-dessus"
    R2. R1*2 R2. R1 R2. R1*2 R1 R2. R1*3 R2. R1
    r4 r8 <>^\markup\character Cidippe
    re''16 re''16 sib'4 r8 sib'16 do''16 |
    re''8 re''8 re''8 re''8 re''8 re''8 |
    sol'4 sol'4 r4 |
    R2. R1*5 R1*2 R1*3 R2. R1 R2.*3 R1*3 R2. R1*2 R2. R1 R2.*2 R1
    <>^\markup\character Cidippe
    sib'2. sib'4 |
    sol'2 do''4. sib'8 |
    la'2 la'4 si'4 |
    do''4 do''4 sol'4 la'4 |
    sib'4 sib'4 sib'4 la'4 |
    sol'2 sol'4 la'4 |
    fad'2\trill fad'2 |
    fad'2\trill fad'4 la'4 |
    si'2. si'4 |
    do''2 si'4. do''8 |
    do''2 do''4 do''4 |
    la'4 la'4 re''4 sib'4 |
    sol'4 sol'4 sol'4. sol'8 |
    la'4 sib'4 la'4 ( sol'8 ) la'8 |
    sib'2 sib'4 sib'4 |
    sol'4 sol'4 do''4 la'4 |
    fad'4\trill fad'4 sib'4. sib'8 |
    sol'4 sol'4 do''4 la'4 |
    fad'4 sol'4 fad'4.\trill sol'8 |
    sol'4 r4 r2 |
    R1*4
    <>^\markup\character Cidippe
    r2 r4 sib'4 |
    do''2. do''4 |
    do''2. la'4 |
    sib'4 sib'4 sib'4 ( la'8 ) sib'8 |
    la'4 sib'4 sib'4 sib'4 |
    sib'4. la'8 la'4.\trill sib'8 |
    sib'2. re''4 |
    re''2. re''4 |
    sol'2. sol'4 |
    do''4 do''4 do''4. do''8 |
    fa'4 fa'4 fa'4 fa'4 |
    sib'4 sib'4 sib'4 ( la'8 ) sib'8 |
    la'1 |
    r4 la'4 la'4 si'4 |
    do''4 sib'4 la'4.\trill sol'8 |
    fad'2. fad'4 |
    sol'2. sol'4 |
    la'4 la'4 re'4. sol'8 |
    fad'4 fad'4 fad'4 fad'4 |
    sol'4 la'4 sib'4 sib'4 |
    sib'4 sol'4 la'4 si'4 |
    do''4 sib'4 la'4. sol'8 |
    fad'4. sib'8 fad'4. sol'8 |
    la'2. fad'4 |
    sib'4 sib'4 sib'4 sib'4 |
    sol'4. sol'8 sol'4. sol'8 |
    do''4 sib'4 la'4. sol'8 |
    fad'4 fad'4 fad'4 sol'4 |
    la'2. sol'4 |
    sol'2( fad'4.)\trill sol'8 |
    sol'1 |
  }
  \tag #'(recit3 basse) {
    <<
      \tag #'recit3 {
        \clef "vbas-dessus"
        R2. R1*2 R2. R1 R2. R1*2 R1 R2. R1*3 R2. R1 r4 r8
      }
      \tag #'basse {
        s2. s1*2 s2. s1 s2. s1*2 s1 s2. s1*3 s2. s1 s4.
        \clef "vbas-dessus"
      }
    >> <>^\markup\character Aglaure
    fa''16 fa''16 re''4 r8 re''16 mib''16 |
    fa''8 fa''8 fa''8 fa''8 fa''8 mib''16[ re''16] |
    mib''4
    <<
      \tag #'recit3 {
        mib''4 r4 |
        R2. R1*5 R1*2 R1*3 R2. R1 R2.*3 R1*3 R2. R1*2 R2. R1 R2.*2 R1
      }
      \tag #'basse {
        mib''8 s4. |
        s2. s1*5 s1*2 s1*3 s2. s1 s2.*3 s1*3 s2. s1*2 s2. s1 s2.*2 s1
        \clef "vbas-dessus"
      }
    >> <>^\markup\character Aglaure
    re''2. re''4 |
    mib''2 mib''4. re''8 |
    do''2\trill do''4 re''4 |
    mib''4 mib''4 mib''4 ( re''8 ) mib''8 |
    re''4 re''4 re''4 do''4 |
    sib'2 sib'4 do''4 |
    la'2\trill la'2 |
    la'2\trill la'4 re''4 |
    re''2. re''4 |
    mib''2 re''4.\trill do''8 |
    do''2 mib''4 mib''4 |
    do''4 do''4 fa''4 re''4 |
    sib'4 sib'4 mib''4. mib''8 |
    mib''4 re''4 do''4\trill( sib'8) do''8 |
    re''2 re''4 re''4 |
    sib'4 sib'4 mib''4 do''4 |
    la'4\trill la'4 re''4. re''8 |
    sib'4 sib'4 mib''4 do''4 |
    la'4 sib'4 la'4.\trill sol'8 |
    sol'4 <<
      \tag #'recit3 { r4 r2 | R1*4 }
      \tag #'basse { s2. s1*4 \clef "vbas-dessus" }
    >> <>^\markup\character Aglaure
    r2 r4 re''4 |
    mib''2. mib''4 |
    re''2. re''4 |
    re''4 re''4 re''4. mi''8 |
    fa''4 fa''4 fa''4 sol''4 |
    mib''4. mib''8 mib''4 ( re''8 ) mib''8 |
    re''2.\trill fa''4 |
    fa''2. sol''4 |
    mib''2. mib''4 |
    mib''4 mib''4 mib''4. mib''8 |
    re''4 re''4 re''4 re''4 |
    re''4 re''4 do''4. do''8 |
    do''1 |
    r4 do''4 do''4 re''4 |
    mib''4 re''4 do''4.\trill sib'8 |
    la'2.\trill la'4 |
    re''2. re''4 |
    do''4 do''4 sib'4 ( la'8 ) sib'8 |
    la'4\trill la'4 la'4 la'4 |
    sib'4 do''4 re''4 re''4 |
    sol'4 sib'4 do''4 re''4 |
    mib''4 re''4 do''4. sib'8 |
    la'4.\trill re''8 la'4. sib'8 |
    fad'2. la'4 |
    re''4 re''4 re''4 re''4 |
    sib'4. sib'8 sib'4 sib'4 |
    mib''4 re''4 do''4. sib'8 |
    la'4 la'4 la'4 sib'4 |
    do''2. sib'4 |
    sib'2( la'4.\trill) sol'8 |
    sol'1 |
  }
>>