\key do \major
\time 4/4 \midiTempo#80 s1*6
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1*5
\digitTime\time 3/4 \midiTempo#80 s2. \bar "|."
