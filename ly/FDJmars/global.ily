\key re \major
\time 4/4 \midiTempo#80 s1*4
\alternatives { \digitTime\time 2/2 \midiTempo#160 s1 } { \digitTime\time 3/4 \midiTempo#120 s2. }
s2. \bar ".!:" s2.*13 \alternatives s2. s2. \bar "|."
