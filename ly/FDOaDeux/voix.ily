<<
  \tag #'(voix1 basse) {
    \clef "vdessus" <>^\markup\character Les Muses
    r4 la'4. sib'8 |
    sib'4 ( la'4. ) la'8 |
    re''2 mi''4 |
    dod''2\trill dod''4 |
    r4 mi''4. fa''8 |
    re''2 mi''4 |
    fa''4. mi''8 ( re''4 )|
    mi''2 mi''4 |
    re''4. mi''8 do''4 |
    si'4.\trill si'8 do''4 ~|
    do''8 re''8 si'4.\trill la'8 |
    la'2. |
    la'2. |
    r4 do''4. do''8 |
    do''4. ( re''8 ) sib'4 |
    la'4. ( sol'8 ) la'4 |
    sib'2 sib'4 |
    r4 re''8 [ do''8 ] sib'8 [ la'8 ]|
    sol'4. sol'8 la'4 ~|
    la'8 sib'8 sol'4.\trill fa'8 |
    fa'2. |
    r4 fa''4. mi''8 |
    re''2 re''4 |
    r4 sol''4. fa''8 |
    mi''2 mi''4 |
    r4 fa''4. fa''8 |
    dod''2 re''4 ~|
    re''8 mi''8 dod''4.\trill re''8 |
    re''2. |
  }
  \tag #'voix2 {
    \clef "vbas-dessus" r4 fa'4. sol'8 |
    mi'2 fa'4 |
    fa'2 sol'4 |
    la'2 la'4 |
    r4 do''4. re''8 |
    si'2 do''4 |
    do''2 si'4 |
    do''2 do''4 |
    si'4. do''8 la'4 |
    sold'4. sold'8 la'4 ~|
    la'8 si'8 sold'4.\trill la'8 |
    la'2. |
    la'2. |
    r4 la'4. mi'8 |
    fad'2 sol'4 |
    sol'2 fad'4 |
    sol'2 sol'4 |
    r4 sib'8 [ la'8 ] sol'8 [ fa'8 ]|
    mi'4. mi'8 fa'4 ~|
    fa'8 sol'8 mi'4.\trill fa'8 |
    fa'2. |
    r4 la'4. la'8 |
    sib'2 sib'4 |
    r4 si'4. si'8 |
    dod''2 dod''4 |
    r4 re''4. la'8 |
    sol'2 fa'4 ~|
    fa'8 mi'8 mi'4.\trill re'8 |
    re'2. |
  }
>>
