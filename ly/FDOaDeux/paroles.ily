\tag #'(couplet1 basse) {
  Gar -- dez- vous, beau -- tés sé -- vè -- res,
  les A -- mours sont trop d’af -- fai -- res,
  crai -- gnez tou -- jours de vous lais -- ser char -- mer.
  - mer.
  
  Quand il faut que l’on sou -- pi -- re,
  tout le mal n’est pas de s’en -- flam -- mer ;
  Le mar -- ti -- re de le di -- re,
  coû -- te plus cent fois que d’ai -- mer.
}
\tag #'couplet2 {
  On ne peut ai -- mer sans pei -- nes,
  il est peu de dou -- ces chaî -- nes,
  à tout mo -- ment on se sent a -- lar -- mer.
  - mer.
}
