Ve -- nus veut- el -- le ré -- sis -- ter ?
N’a- t’el -- le point as -- sez é -- cou -- té sa co -- lè -- re,
et l’A -- mour qui lan -- guit ne peut- il se flat -- ter
que ses maux tou -- che -- ront sa mè -- re ?

Quoi ! je souf -- fri -- rais qu’à mon fils
u -- ne sim -- ple mor -- telle a -- spi -- re ?

Si tu ne m’en veux point dé -- di -- re,
il n’est rien pour Psy -- ché qui ne me soit per -- mis,
seule aux yeux de l’A -- mour, elle est ai -- mable et bel -- le ;
pour l’é -- ga -- ler à lui, je la fais im -- mor -- tel -- le.

Puis -- que d’une im -- mor -- telle il doit ê -- tre l’é -- poux,
Ju -- pi -- ter a par -- lé, je n’ai plus de cou -- roux.

Viens, A -- mour, viens, A -- mour,
tes sou -- pirs em -- por -- tent la vic -- toi -- re ;
Viens, A -- mour, tes sou -- pirs em -- por -- tent la vic -- toi -- re.
Viens, A -- mour, tes sou -- pirs em -- por -- tent la vic -- toi -- re.

Psy -- ché, re -- vois le jour,
on te per -- met en -- fin de vi -- vre pour l’A -- mour.

Vous y con -- sen -- tez ? quel -- le gloi -- re !

Viens pren -- dre place au -- près de ton a -- mant.

On me rend donc à vous ? Ô des -- tin plein de char -- mes !

Ô fa -- vo -- ra -- ble chan -- ge -- ment !

Ô des -- tin plein de char -- mes !

Ô fa -- vo -- ra -- ble chan -- ge -- ment !

Ai -- mez sans trouble et sans a -- lar -- mes,
ai -- mez, ai -- mez sans trouble et sans a -- lar -- mes.
Vous, Dieux, ac -- cou -- rez tous, et dans cet heu -- reux jour,
cé -- lé -- brez à l’en -- vie la gloi -- re de l’A -- mour,
et dans cet heu -- reux jour,
cé -- lé -- brez à l’en -- vie la gloi -- _ re de l’A -- mour.
