\ffclef "vbasse" <>^\markup\character Jupiter
r8 re'8 fad8. re16 la8 la8 si8 dod'8 |
re'4. si8 si8 si8 si8 fad8 |
sol4 sol8 si8 sol4 sol8 la8 |
fad8\trill fad8 r8 re'16 re'16 si4 si8 si8 |
sold4 sold8 sold8 sold4 sold8 si8 |
mi4 la8 la8 si8 si16 si16 dod'8 re'8 |
dod'4\trill dod'4 
\ffclef "vbas-dessus" <>^\markup\character Venus
r4 mi''4 |
r8 la'8 la'8 la'8 mi'4 mi'8 fad'8 |
sol'4 sol'8 sol'8 sol'8 sol'16 la'16 si'8 dod''8 |
re''4 re''4 
\ffclef "vbasse" <>^\markup\character Jupiter
r4 si4 |
si8 si8 si8 si8 fad4.\trill sol8 |
la8 la8 r8 la16 si16 do'8 do'16 re'16 |
si8.\trill sol16 sol8 sol8 sol8 si8 |
mi8 r8 la8. la16 mi8 mi16 mi16 |
dod8 r16 mi16 mi8 mi8 la8 la8 |
fad8\trill fad8 r8 la8 la16 la16 si16 dod'16 |
re'4 re'8 re'8 sold4 sold8 la8 |
la4 la4 
\ffclef "vbas-dessus" <>^\markup\character Venus
dod''8 dod''16 dod''16 dod''8 la'8 |
re''4 re''8 re''8 la'4 la'8 re''8 |
si'4 r8 si'16 si'16 mi''4 mi''8 mi''8 |
dod''4 la'8 la'8 re''4 re''8 dod''8 |
re''1 
\ffclef "vbasse" <>^\markup\character Jupiter
fad2. re4 |
la1 |
si2. re'4 |
dod'2 la4. la8 |
re'2. re'4 |
si8[ dod'8 re'8 dod'8 si8 la8 sol8 fad8] (|
mi4. ) mi8 mi4 la4 |
fad4. mi8 ( re2 )|
re'2. re'4 |
si2 sold4. si8 |
mi2. la4 |
re8[ \melisma dod8 re8 mi8 fad8 sol8 fad8 sol8] |
la8[ sol8 la8 si8 dod'8 si8 dod'8 la8] |
re'4 \melismaEnd re'8[ dod'8] si8 [ la8 ] sol8 [ fad8 ]|
fad2( mi2\trill) |
re1 |
re'2. re'4 |
si2 sold4. si8 |
mi2. la4 |
re8[ \melisma dod8 re8 mi8 fad8 sol8 fad8 sol8] |
la8[ sol8 la8 si8 dod'8 si8 dod'8 la8] |
re'4. \melismaEnd re'8 re'4. re'8 |
re'2 ( dod'2 )|
re'4 
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 re'8 la'8 la'8 la'8 la'8 |
re''4 si'8 si'16 si'16 dod''8. re''16 |
mi''8. mi''16 fad''8. si'16 si'8.\trill la'16 |
la'4 
\ffclef "vbas-dessus" <>^\markup\character Psyche
r4 dod''8 dod''8 dod''8 mi''8 |
la'2 re''4 fad''4 |
si'8 si'8 
\ffclef "vbasse" <>^\markup\character Jupiter
re'4 si8 si8 |
do'8 si8 la8 sol8 fad8 mi8 |
red4 
\ffclef "vbas-dessus" <>^\markup\character-text Psyche à l’Amour
fad'8 fad'16 fad'16 si'8 si'8 |
sold'4 mi''8 si'8 do''4 do''8 do''8 |
la'4\trill la'4 
\ffclef "vbas-dessus" <>^\markup\character L'Amour
fa'' re''8 re'' |
si'4 si' dod'' re'' |
dod''2
\ffclef "vbas-dessus" <>^\markup\character Psyche
mi''4. la'8 |
do''4 do''8 re''8 si'4\trill si'4 
\ffclef "vbas-dessus" <>^\markup\character L'Amour
re'' re''8 sol'' mi''8. mi''16 mi''8. fad''16 |
re''1 |
\ffclef "vbasse" <>^\markup\character Jupiter
r4 re'4 la4 la4 |
si4 si4 la4.\trill sol8 |
fad2\trill fad4 si4 |
sold4 sold4 la4 la4 |
si4. si8 si4 mi'4 |
dod'2 dod'2 |
r2 la2 |
re'2 r8 si8 si8 re'8 |
sol2 r4 si4 |
do'4. si8 la8[ sol8] fad8[ mi8] |
red2 r4 fad8 fad8 |
sol2 sol4 la4 |
si4. si8 si8[\melisma do'8 re'8 si8] |
do'8[ re'8 do'8 si8 la8 sol8 fad8 mi8] |
si4\melismaEnd la8[ sol8] fad4.\trill mi8 |
mi2. la4 |
fad4. fad8 fad4 la4 |
re2. re8 mi8 |
fad2 fad4 sol4 |
la4. la8 la8[\melisma si8 dod'8 la8] |
re'8[ mi'8 re'8 dod'8 si8 la8 sol8 fad8]\melismaEnd |
si8 la8[ sol8 fad8] mi4.\trill re8 |
re1 |
