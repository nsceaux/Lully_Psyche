\newBookPart#'()
\act "Acte Cinquième"
\sceneDescription\markup\justify {
  Le théâtre représente les jardins de Venus.
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center\smallCaps { Psyché. }
%% 5-1
\pieceToc "Ritournelle"
\includeScore "FAAritournelle"
%% 5-2
\pieceToc \markup {
  Psyché : \italic { Si je fais vanité de ma tendresse extrême }
}
\includeScore "FABpsyche"
\sceneDescription\markup\justify {
  Psyché tombe sans force sur un gazon, où elle demeure couchée.
}
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center\smallCaps { Venus, Psyché. }
%% 5-3
\pieceToc\markup\wordwrap {
  Venus, Psyché : \italic { Enfin, insolente rivale }
}
\includeScore "FBAvenusPsyche"
\sceneDescription\markup\justify {
  Mercure descend ici en volant.
}
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center\smallCaps { Mercure, Vénus. }
%% 5-4
\pieceToc\markup\wordwrap {
  Mercure, Vénus : \italic { Vous croyez trop la jalouse colère }
}
\includeScore "FCAmercureVenus"
\sceneDescription\markup\justify {
  Jupiter descend sur son Trône, au milieu de son Palais.
}
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène Dernière" "Scène IV"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Jupiter, Venus, L'Amour, Mercure, Psyché.
}
%% 5-5
\pieceToc "Prélude"
\includeScore "FDAprelude"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-6
\pieceToc\markup\wordwrap {
  Jupiter, Venus, Psyché, l’Amour :
  \italic { Venus veut-elle résister }
}
\includeScore "FDBjupiterVenusPsycheAmour"
\newBookPart#'(full-rehearsal full-urtext)
\sceneDescription\markup\center-column {
  \justify {
    L’Amour descend, et va s’assoir aux pieds de Jupiter.
    Venus et Psyché étant enlevées par un nuage, vont se placer
    aux deux côtés de l’Amour ; Apollon, Bacchus, Momus et Mars
    descendent dans leurs machines, auprès de leurs quadrilles.
    Le jardin disparaît, et tout le théâtre représente le Ciel.
  }
  \justify {
    Apollon conduit les Muses, et les Arts ; Bacchus est accompagné
    de Silene, de Satyres, et de Menades ; Momus, Dieu de la raillerie,
    mène après lui une troupe enjouée de Polichinelles et de Matassins ;
    et Mars paraît à la tête d’une troupe de Guerriers, suivis de timballes,
    de tambours, et de trompettes.
  }
}
%% 5-7
\pieceToc "Prélude"
\includeScore "FDDritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-8
\pieceToc\markup\wordwrap {
  Apollon : \italic { Unissons-nous, troupe immortelle }
}
\includeScore "FDEapollon"
\markupCond#(not (symbol? (ly:get-option 'part))) \markup\vspace#20
\newBookPart#'(full-rehearsal full-urtext)
%% 5-9
\pieceToc\markup\wordwrap {
  Chœur des divinités célestes : \italic { Célébrons ce grand jour }
}
\includeScore "FDFchoeur"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-10
\pieceToc\markup\wordwrap { Prélude pour Bacchus, Momus et Mars }
\includeScore "FDGritournelle"
%% 5-11
\pieceToc\markup\wordwrap {
  Bacchus : \italic { Si quelque fois, suivant nos douces lois }
}
\includeScore "FDHbacchus"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-12
\pieceToc\markup\wordwrap {
  Momus : \italic { Je cherche à médire }
}
\includeScore "FDImome"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-13
\pieceToc\markup\wordwrap {
  Mars : \italic { Mes plus fiers ennemis vaincus ou pleins d'effroi }
}
\includeScore "FDJmars"
\markupCond#(not (symbol? (ly:get-option 'part))) \markup\vspace#15
\newBookPart#'(full-rehearsal full-urtext)
%% 5-14
\pieceToc\markup\wordwrap {
  Chœur des Dieux : \italic { Chantons les plaisirs charmants }
}
\includeScore "FDKchoeur"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-15
\pieceToc\markup\wordwrap { Premier air, pour les bergers }
\includeScore "FDLritournelle"
%% 5-16
\pieceToc\markup\wordwrap {
  Apollon : \italic { Le Dieu qui nous engage }
}
\includeScore "FDMapollon"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-17
\pieceToc\markup\wordwrap { Deuxième air, pour les bergers }
\includeScore "FDNritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-18
\pieceToc\markup\wordwrap {
  Les Muses : \italic { Gardez-vous, beautés sévères }
}
\includeScore "FDOaDeux"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-19
\pieceToc\markup\wordwrap { Premier Air, pour les menades et les satyres }
\includeScore "FDPair"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-20
\pieceToc\markup\wordwrap {
  Bacchus : \italic { Admirons le jus de la treille }
}
\includeScore "FDQbacchus"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-21
\pieceToc\markup\wordwrap { Deuxième Air, pour les menades et les satyres }
\includeScore "FDRair"
\sceneDescription\markup\justify {
  Silene, nourricier de Bacchus, paraît monté sur son âne.
}
\newBookPart#'(full-rehearsal full-urtext)
%% 5-22
\pieceToc\markup\wordwrap {
  Silène : \italic { Bacchus veut qu'on boive à longs traits }
}
\includeScore "FDSsilene"
\markupCond#(not (symbol? (ly:get-option 'part))) \markup\vspace#15
\newBookPart#'(full-rehearsal full-urtext)
%% 5-23
\pieceToc\markup\wordwrap {
  Dialogue de Silene avec deux Satyres
}
\includeScore "FDTaTrois"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-24
\pieceToc "Air, pour les Polichinelles"
\includeScore "FDUentree"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-25
\pieceToc "[Ballard] Chaconne"
\includeScore "FDUchaconne"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-26
\pieceToc\markup\wordwrap {
  Momus : \italic { Folâtrons, divertissons-nous }
}
\includeScore "FDVmome"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-27
\pieceToc\markup\wordwrap {
  Prélude de trompettes, et de violons, en écho, pour Mars.
}
\includeScore "FDXprelude"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-28
\pieceToc\markup\wordwrap {
  Mars : \italic { Laissons en paix toute la terre }
}
\markup\wordwrap {
  [Manuscrit Versailles : cet air est situé avant le prélude
  de trompettes.]
}
\includeScore "FDWmars"
%% 5-29
\sceneDescription\markup\justify {
  Quatre hommes portant des enseignes, s’en servent à faire
  paraître leur adresse en dansant.
}
\pieceToc "Premier air pour les suivants de Mars"
\includeScore "FDYrondeau"
%% 5-30
\pieceToc "Deuxième Air, pour les suivants de Mars"
\includeScore "FDZair"
\newBookPart#'(full-rehearsal full-urtext)
%% 5-31
\sceneDescription\markup\justify {
  Les quatre troupes différentes de la suite d’Apollon, de Bacchus,
  de Momus, et de Mars, reprennent le Chœur,
  \italic { Chantons les plaisirs charmants &c, }
  pour former la dernière entrée de cette tragédie, et terminer
  la fête des nôces de l’Amour et de Psyché.
}
\pieceToc\markup\wordwrap {
  Chœur des Dieux : \italic { Chantons les plaisirs charmants }
}
\reIncludeScore "FDKchoeur" "FDZZchoeur"
\actEnd\markup { FIN DU CINQUIÈME ET DERNIER ACTE. }
