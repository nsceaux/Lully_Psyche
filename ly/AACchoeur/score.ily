\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        <>^\markup\smallCaps { Chœur des divinités de la terre et des eaux }
        \global \keepWithTag #'vdessus \includeNotes "voix" 
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics << 
        \global \keepWithTag #'vhaute-contre \includeNotes "voix" 
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics << 
        \global \keepWithTag #'vtaille \includeNotes "voix" 
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics << 
        \global \keepWithTag #'vbasse \includeNotes "voix" 
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse-continue"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
