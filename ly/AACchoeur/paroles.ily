\tag #'vdessus {
  Nous goû -- tons un -- e paix pro -- fon -- de ;
  les plus doux jeux sont i -- ci -- bas ;
  les plus doux jeux sont i -- ci -- bas ;
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de.
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de. 

  Des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner, 
  nous don -- ner de beaux jours.
  Ve -- nez nous don -- ner de beaux jours.
  Des -- cen -- Dez, Des -- Cen -- Dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.

  Ve -- nez, ve -- nez nous don -- ner de beaux jours
  ve -- nez, ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours, 
  des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours,
  mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner de beaux jours.

  Des -- cen -
  jours.
}
\tag #'vhaute-contre {
  Nous goû -- tons un -- e paix pro -- fon -- de ; 
  les plus doux jeux sont i -- ci -- bas ; 
  les plus doux jeux sont i -- ci -- bas ; 
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de.
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de. 

  Des -- cen -- dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner, 
  nous don -- ner de beaux jours.
  Des -- cen -- dez, des -- cen -- dez,
  des -- cen -- dez, des -- cen -- dez, mère des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez,
  mè -- re des A -- mours, mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez,
  des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez, ve -- nez nous don -- ner de beaux jours.
  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours,
  des -- cen -- dez, des -- cen -- dez, 
  des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner de beaux jours.
  jours.
}
\tag #'vtaille {
  Nous goû -- tons un -- e paix pro -- fon -- de ; 
  les plus doux jeux sont i -- ci -- bas ; 
  les plus doux jeux sont i -- ci -- bas ; 
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de.
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de. 

  Des -- cen -- dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner, 
  nous don -- ner de beaux jours.
  Ve -- nez nous don -- ner de beaux jours.
  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez,
  des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez ve -- nez nous don -- ner de beaux jours
  des -- cen -- dez des -- cen -- dez, mè -- re des A -- mours,
  des -- cen -- dez des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner de beaux jours.
  jours.
}
\tag #'vbasse {
  Nous goû -- tons un -- e paix pro -- fon -- de ; 
  les plus doux jeux sont i -- ci -- bas ; 
  les plus doux jeux sont i -- ci -- bas ; 
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de.
  On doit ce re -- pos plein d'ap -- pas 
  au plus grand Roi du mon -- de. 

  Des -- cen -- dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.
  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours, 
  Ve -- nez nous don -- ner de beaux jours.
  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours,
  ve -- nez nous don -- ner de beaux jours.

  Des -- cen -- dez, des -- cen -- dez,
  des -- cen -- dez, mère des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.
  Des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours, 
  des -- cen -- dez, des -- cen -- dez, mè -- re des A -- mours, 
  ve -- nez nous don -- ner de beaux jours.
  Ve -- nez, ve -- nez nous don -- ner de beaux jours.
  Des -- cen -
  
  jours.
}
