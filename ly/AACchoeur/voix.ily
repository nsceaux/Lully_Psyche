<<
  \tag #'vdessus {
    \clef "vdessus"
    mi''4 mi''4 |
    fa''2 do''4. re''8 |
    mi''4 do''4 re''2\trill |
    mi''1 |
    r4 si'4 si'4 dod''4 |
    re''4 la'4 la'4 si'4 |
    do''4 do''4 do''4 do''4 |
    la'4 la'4 la'4. si'8 |
    sold'2.\trill mi''4 |
    mi''2 dod''4. dod''8 |
    re''2 re''4. re''8 |
    re''2. re''4 |
    mi''4 mi''4 re''4 mi''4 |
    do''1\trill |
    si'2. re''4 |
    mi''2 mi''4. mi''8 |
    fa''2 fa''4. fa''8 |
    mi''2. fa''4 |
    fa''4 re''4 mi''4. fa''8 |
    mi''2 ( re''2)\trill |
    do''2. |
    r4 mi''4 mi''4 |
    re''2. |
    do''2 do''4 |
    do''2 re''4 |
    si'2 do''4 |
    do''4 do''4 re''4 |
    si'4. si'8 mi''4 |
    dod''2 dod''4 |
    re''2 re''4 |
    mi''4 mi''4 mi''4 |
    fa''4 fa''4 mi''4 |
    re''4. re''8 mi''4 |
    dod''2\trill mi''4 |
    fa''4 fa''4 fa''4 |
    mi''4. fa''8 mi''4\trill |
    re''4 la'4 la'4 |
    sib'4 do''4 re''4 |
    re''2. |
    re''2 re''4 |
    re''4 ( do''4.\trill) re''8 |
    re''2 re''4 |
    mib''4 mib''4 mib''4 |
    re''4. re''8 re''4 |
    si'2 r4 |
    R2.*6 |
    r2 mi''4 |
    fa''2 fa''4 |
    sol''4 sol''4 sol''4 |
    do''4. do''8 do''4 |
    fa''2 fa''4 |
    mi''2 sol''4 |
    do''4 do''4 fa''4 |
    re''4. re''8 sol''4 |
    mi''2\trill r4 |
    R2.*18 |
    r4 sol''4 fa''4 |
    mi''4 mi''4 mi''4 |
    re''2. |
    do''2 do''4 |
    si'2 do''4 |
    la'4 re''4 re''4 |
    re''4 do''4 do''4 |
    do''4 si'4. si'8 |
    do''2 do''4 |
    re''4 re''4 re''4 |
    do''4. ( si'8 ) do''4 |
    si'2\trill re''4 |
    mi''4 mi''4 mi''4 |
    fa''4. fa''8 fa''4 |
    mi''2 fa''4 |
    re''2 re''4 |
    mi''4 mi''4 fa''4 |
    re''4. re''8 sol''4 |
    mi''4 mi''4 mi''4 |
    mi''2. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre"
    sol'4 sol'4 |
    la'2 la'4. la'8 |
    sol'4 la'4 fa'2\trill |
    sol'1 |
    r4 re'4 re'4 mi'4 |
    fa'4 fa'4 fa'4. sol'8 |
    mi'4. mi'8 mi'4 mi'4 |
    fa'4 fa'4 re'4. re'8 |
    mi'2. sold'4 |
    la'2 mi'4. mi'8 |
    fad'2 fad'4. fad'8 |
    sol'2. sol'4 |
    sol'4 sol'4 sol'4. sol'8 |
    sol'2 ( fad'2 )|
    sol'2. sol'4 |
    sol'2 sol'4. sol'8 |
    la'2 la'4. la'8 |
    la'2. la'4 |
    sol'4 sol'4 sol'4. la'8 |
    sol'2. ( fa'4 ) |
    mi'2. |
    r4 sol'4 sol'4 |
    sol'2. |
    mi'2 mi'4 |
    mi'2 re'4 |
    mi'2 mi'4 |
    fa'4 fa'4 fa'4 |
    mi'4. mi'8 mi'4 |
    mi'2 mi'4 |
    fa'2 fa'4 |
    mi'4 la'4. sol'8 |
    fa'4 re'4 mi'4 |
    fa'4. fa'8 sol'4 |
    mi'4 la'4 la'4 |
    la'2. |
    r4 la'4 sol'4 |
    fad'4 fad'4 fad'4 |
    sol'2. |
    la'2 la'4 |
    sol'2 re'4 |
    sol'2 sol'4 |
    fad'2 sol'4 |
    sol'4 sol'4. la'8 |
    fad'4. sol'8 fad'4 |
    sol'2 r4 |
    R2.*5 |
    r4 sol'4 fa'4 |
    mi'4 sol'4 sol'4 |
    fa'2 do'4 |
    do'4 do'4. sol'8 |
    fa'4 la'4 la'4 |
    la'2 la'4 |
    la'2 sol'4 |
    la'4 la'4 la'4 |
    sol'4. sol'8 sol'4 |
    sol'2 r4 |
    R2.*6 |
    r4 la'4 sol'4 |
    fa'4 fa'4 mi'4 |
    re'2. |
    r4 sol'4 fa'4 |
    mi'2 r4 |
    r4 la'4 sol'4 |
    fa'2. |
    sol'2 sol'4 |
    mi'2 mi'4 |
    fa'2 fa'4 |
    re'2 sol'4 |
    mi'4 mi'4 fa'4 |
    re'4. mi'8 re'4\trill |
    do'2 r4 |
    r4 sol'4 sol'4 |
    sol'4 fa'4 fa'4 |
    fa'4 mi'4 mi'4 |
    fa'2 fa'4 |
    sol'4 sol'4 la'4 |
    fa'4 fa'4 sol'4 |
    mi'4 mi'4 fa'4 |
    re'4 sol'4. sol'8 |
    la'2 la'4 |
    re'2 sol'4 |
    sol'4 sol'4 sol'4 |
    la'4. la'8 la'4 |
    la'2 la'4 |
    sol'2 sol'4 |
    sol'4. sol'8 la'4 |
    sol'4. sol'8 sol'4 |
    sol'4 r2 |
    sol'2. |
  }
  \tag #'vtaille {
    \clef "vtaille"
    do'4 do'4 |
    do'2 do'4. do'8 |
    do'4 do'4 si2 |
    do'1 |
    r4 sol4 sol4 sol4 |
    la4 re'4 re'4 re'4 |
    do'4 la4 la4 do'4 |
    do'4 do'4 re'4. re'8 |
    si2.\trill si4 |
    dod'2 la4. la8 |
    la2 la4. la8 |
    si2. si4 |
    do'4 do'4 re'4 do'4 |
    do'1 |
    re'2. si4 |
    do'2 do'4. do'8 |
    do'2 do'4. re'8 |
    do'2. do'4 |
    re'4 re'4 do'4 do'4 |
    do'2 ( si2 )|
    do'2. |
    r4 do'4 do'4 |
    si2. |
    do'2 la4 |
    la2 si4 |
    sold2 la4 |
    la4 la4 si4 |
    sold4. fad8 sold4 |
    la2 la4 |
    la2 la4 |
    la4 la4 la4 |
    la4 la4 la4 |
    la4. la8 sol4 |
    la2 dod'4 |
    re'4 re'4 re'4 |
    re'4. re'8 dod'4 |
    re'4 re'4 re'4 |
    re'4 do'4 sib4 |
    la2. |
    sib2 la4 |
    sol2 sol4 |
    la2 sib4 |
    sib4 sib4 do'4 |
    la4. la8 re'4 |
    re'2 r4 |
    R2.*6 |
    r4 do'4 do'4 |
    do'4 sib4 la4 |
    sol2. |
    la2 la4 |
    re'2 re'4 |
    do'2 do'4 |
    do'4 do'4 re'4 |
    si4.\trill la8 si4 |
    do'2 r4 |
    R2.*6 |
    r4 fa'4 mi'4 |
    re'4 re'4 do'4 |
    si2. |
    r4 mi'4 re'4 |
    do'2 r4 |
    r4 fa'4 mi'4 |
    re'2. |
    mi'2 mi'4 |
    dod'2 dod'4 |
    re'2 re'4 |
    si2 mi'4 |
    do'4 do'4 re'4 |
    si4.\trill la8 si4 |
    do'2 r4 |
    R2. |
    r4 do'4 la4 |
    si4 sol4 sol4 |
    la4 la4 la4 |
    sol2 mi4 |
    la4 si4 si4 |
    si4 la4 la4 |
    la4 sol4 sol4 |
    sol4 ( fad4. ) sol8 |
    sol2 si4 |
    do'4 do'4 do'4 |
    do'4 do'4 re'4 |
    do'2 do'4 |
    re'2 re'4 |
    do'4 do'4 do'4 |
    do'4. re'8 si4 |
    do'4 r2 |
    do'2. |
  }
  \tag #'vbasse {
    \clef "vbasse"
    do'4 do'4 |
    fa2 fa4. fa8 |
    mi4 fa4 re2\trill |
    do1 |
    r4 sol4 sol4 sol4 |
    re4 re4 re4 re4 |
    la,4 la4 la4 la4 |
    fa4 fa4 fa4. mi8 |
    mi2. mi4 |
    la2 la4. la8 |
    re2 re4. re8 |
    sol2. sol4 |
    do'4 do'4 si4 do'4 |
    la1\trill |
    sol2. sol4 |
    mi2 mi4 do4 |
    fa2 fa4. re8 |
    la2. la4 |
    si4 si4 do'4. fa8 |
    sol2 ( sol,2 )|
    do2. |
    r4 do'4 do'4 |
    sol2. |
    la2 la4 |
    fa2 fa4 |
    mi2 do4 |
    fa4 fa4 re4 |
    mi4. re8 mi4 |
    la,4 la4 sol4 |
    fa4 mi4 re4 |
    dod2. |
    re2 do4 |
    sib,4. ( la,8 ) sib,4 |
    la,2 la4 |
    fa4 fa4 re4 |
    la4. sol8 la4 |
    re4 re'4 do'4 |
    sib4 la4 sol4 |
    fad2. |
    sol2 fa4 |
    mib4. ( re8 ) mib4 |
    re2 sib,4 |
    mib4 mib4 do4 |
    re4. do8 re4 |
    sol,2 r4 |
    R2.*6 |
    r4 do'4 sib4 |
    la4 sol4 fa4 |
    mi2. |
    fa2 fa4 |
    re2 re4 |
    la2 mi4 |
    fa4 fa4 re4 |
    sol4. fa8 sol4 |
    do2 r4 |
    R2.*8 |
    r4 sol4 fa4 |
    mi2 r4 |
    r4 la4 sol4 |
    fa2 r4 |
    r4 sib4 la4 |
    sol2. |
    la2 la4 |
    re2 re4 |
    sol2 mi4 |
    la4 la4 fa4 |
    sol4. ( fa8 ) sol4 |
    do4 do'4 do'4 |
    si4 sib4 sib4 |
    la2. |
    sol2 sol4 |
    fa2 fa4 |
    mi4 mi4 mi4 |
    re4 re4 re4 |
    do2. |
    si,2 si,4 |
    la,2 la,4 |
    sol,2 sol4 |
    mi4 mi4 do4 |
    fa4. fa8 re4 |
    la2 la4 |
    si2 si4 |
    do'4 do'4 fa4 |
    sol4. fa8 sol4 |
    do4 do'4 do'4 |
    do2. |
  }
>>