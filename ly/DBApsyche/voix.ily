\clef "vbas-dessus" R2.*6 |
<>^\markup\character-text Psyché sans voir Venus r4 re''4 fa''4 | \noBreak
sib'2 mib''8 sol''8 |
do''2\trill do''8 re''8 |
sib'2 la'8 sib'8 |
la'4\trill la'8 fa''8 do''8 re''8 |
mib''8 re''8 re''4 ( do''4)\trill |
sib'4 r4 r4 |
R2.*4 |
r4 re''4 mib''4 |
do''4.\trill do''8 fa''4 |
sib'4. sib'8 do''8 re''8 |
mib''8 re''8 do''2\trill |
sib'4 sib'8 lab'8 lab'16 [ sol'16 ] lab'8 |
sol'4\trill mib''8. mib''16 mib''8. fa''16 |
re''4 mi''4 fa''4 |
sib'4. ( la'8 )[ sib'8 do''8 ]|
la'2.\trill |
r4 re'' mib'' |
la'4\trill do''4 do''8 re''8 |
mib''4. mib''8 re''8 do''8 |
si'4.\trill si'8 si'8 si'8 |
do''4 re''8 [ do''8 ] re''4 |
mib''4 do''4 mib''8 mib''8 |
fa''8 sol''8 re''4.\trill do''8 |
do''4 mib''4 mib''8 fa''8 |
re''4.\trill re''8 re''8 mib''8 |
fa''4. re''8 re''8 sib'8 |
sol'4 sol'4. do''8 |
la'4.\trill sol'8 ( fa'4 )|
fa''4. fa''8 fa''8 sol''8 |
re''4( do''4.\trill) sib'8 |
sib'4 r4 r4 |
R2.*6 |