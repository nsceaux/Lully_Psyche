\piecePartSpecs
#`((basse-continue #:score-template "score-basse-voix")
   (basse #:score-template "score-basse-voix")
   (dessus #:score "score-dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#54 #}))
