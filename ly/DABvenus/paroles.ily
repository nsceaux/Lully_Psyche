Pom -- pe que ce pa -- lais de tous cô -- tés é -- ta -- le,
bril -- lant sé -- jour, que vous bles -- sez mes yeux !
Je ne vois rien qui ne parle en ces lieux, 
de la gloi -- re de ma ri -- va -- le.
Tant de di -- vi -- ni -- tés dont elle a tous les soins,
et la plus for -- te com -- plai -- san -- ce,
sont au -- tant de hon -- teux té -- moins 
de son pou -- voir, et de mon im -- puis -- san -- ce.
Que le mé -- pris est ri -- gou -- reux,
à qui se croit di -- gne de plai -- re !
Un seul ob -- jet qu'on nous pré -- fè -- re,
nous fait un des -- tin mal -- heu -- reux.
Un seul ob -- jet qu'on nous pré -- fè -- re,
nous fait un des -- tin mal -- heu -- reux.
Que le mé -- pris est ri -- gou -- reux,
à qui se croit di -- gne de plai -- re !

Dé -- jà la nuit chas -- se le jour !
Qu'il ne re -- vien -- ne point, a -- vant que je me ven -- ge.
Je sais l'or -- dre du sort ; si Psy -- ché voit l'A -- mour,
aus -- si -- tôt sa for -- tu -- ne chan -- ge.
Ces -- sons de per -- dre des sou -- pirs ; 
Per -- dons, per -- dons Psy -- ché,
sans que Psy -- ché le sa -- che,
el -- le brû -- le de voir cet a -- mant qui se ca -- che ;
il faut con -- ten -- ter ses dé -- sirs.
