\clef "dessus"
\setMusic #'rondeau {
  la''4 la''8 sol''8 fad''4 la''4 |
  re''2 mi''4.\trill re''16 mi''16 |
  fad''4 sol''8 fad''8 sol''8 fad''8 mi''8 re''8 |
  mi''2 la'2 |
  la''4 la''8 sol''8 fad''4 la''4 |
  re''2 mi''4.\trill re''16 mi''16 |
  fad''8 mi''8 fad''8 sol''8 mi''4.\trill re''8 |
  re''1 |
}
\keepWithTag #'() \rondeau
R1*8 |
\rondeau
R1*8 |
\rondeau
