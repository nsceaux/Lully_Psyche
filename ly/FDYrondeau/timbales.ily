\clef "basse"
\setMusic #'rondeau {
  re4 re8 re8 re4 re4 |
  re4 re8 re8 la,4 la,8 la,8 |
  re4 re8 re8 re4 re4 |
  la,4 la,8 la,8 la,4 la,4 |
  re4 re8 re8 re4 re4 |
  re4 re8 re8 la,4 la,8 la,8 |
  re4 re8 re8 la,4. re8 |
  re16 re16 re16 re16 re16 re16 re16 re16 re2 |
}
\keepWithTag #'() \rondeau
R1*8 |
\rondeau
R1*8 |
\rondeau
