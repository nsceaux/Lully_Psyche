\score {
  \new StaffGroup <<
    \new Staff \with {
      instrumentName = \markup\center-column { Trompette en Ré }
    } <<
      \keepWithTag #'() \transpose re do \global
      \transpose re do { \includeNotes "trompette" }
    >>
    \new Staff \with { instrumentName = "Timbales" } <<
      \keepWithTag #'() \global \includeNotes "timbales"
    >>
  >>
  \layout { indent = \largeindent }
}