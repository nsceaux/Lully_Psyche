\score {
  <<
    \new StaffGroup <<
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "dessus"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "taille"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "quinte"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "basse"
      >>
    >>
    \new StaffGroup \with { \haraKiri } <<
      \new Staff <<
        \keepWithTag #'() \global \includeNotes "trompette"
        <>^"Trompettes"
      >>
      \new Staff <<
        \keepWithTag #'() \global \includeNotes "timbales"
        <>_"Timbales"
      >>
    >>
    \new Staff << 
      \keepWithTag #'() \global \includeNotes "basse"
      \modVersion {
        s1*8\break
        s1*8\break
        s1*8\break
        s1*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
