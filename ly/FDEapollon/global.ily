\key re \minor
\time 4/4 \midiTempo#80 s1*2
\time 3/2 \midiTempo#160 s1.
\time 4/4 \midiTempo#80 s1*3 \alternatives s1 s1
\bar "|!:" s1*5 \alternatives s1 s \bar "|."
