\clef "vhaute-contre" <>^\markup\character Apollon
r8 fa'8 fa'8. sol'16 mi'4 la'8 sol'16 la'16 |
fa'4\trill re'4 r8 la'8 la'8 la'8 |
sib'4. la'8 sol'4. fa'16 [ mi'16 ] fa'4. sol'8 |
mi'4\trill r8 do'8 re'8 re'8 re'8. mi'16 |
fa'4 fad'8.\trill fad'16 sol'4 la'16 [ sol'16 ] la'8 |
sib'4 sol'4 mi'8. mi'16 fa'8. mi'16 |
re'4 re'8 [ dod'16 ] re'16 dod'2\trill |
re'4 re'8 [ dod'16 ] re'16 dod'4.\trill la'8
fad'8.\trill fad'16 sol'16 fad'16 [ mi'16 re'16 ] mi'4. do'8 |
do'8. sib16 sib16 [ la16 ] sib8 la2\trill |
re'8 re'16 do'16 do'8 [ si16 ] do'16 si2\trill |
sol'8 sol'16 fa'16 mi'8.\trill re'16 dod'4\trill r8 la'16 la'16 |
sib'4 sol'8.\trill fa'16 fa'4( mi'4)\trill |
\sugNotes {
  re'2^"[Ballard]" r4 r8 la' |
}
re'1 |
