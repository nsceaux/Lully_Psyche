\piecePartSpecs
#`((basse-continue #:system-count 2)
   (basse #:system-count 2)
   (quinte)
   (taille)
   (haute-contre)
   (dessus)
   (silence #:on-the-fly-markup , #{ \markup\tacet#17 #}))

