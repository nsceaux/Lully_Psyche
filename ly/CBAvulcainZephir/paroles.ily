%% Zéphir
Pres -- sez- vous ce tra -- vail que l’A -- mour vous de -- man -- de ?
Vous hâ -- tez- vous d’ac -- com -- plir ses dé -- sirs ?
%% Vulcain
Vous le voy -- ez, Zé -- phir ; aus -- si -- tôt qu’il com -- man -- de,
o -- bé -- ir est pour moi le plus grand des plai -- sirs.
%% Zéphir
Psy -- ché mé -- ri -- te bien une ar -- deur si fi -- dè -- le :
En ces lieux, pour l’A -- mour, j’ai con -- duit cet -- te Bel -- le ;
Et main -- te -- nant, sur des ga -- zons voi -- sins,
un doux som -- meil de ses sens est le maî -- tre :
J’ai fait naître au -- tour d’elle, et ro -- ses et jas -- mins,
qu’elle eut pu sans moi fai -- re naî -- tre.
J’ai fait naître au -- tour d’elle, et ro -- ses et jas -- mins,
qu’elle eut pu sans moi fai -- re naî -- tre.
%% Vulcain
C’est donc Psy -- ché pour qui je pré -- pa -- re ces lieux ?
L’a -- gré -- a -- ble nou -- vel -- le !
C’est Psy -- ché que, mal -- gré le ti -- tre d’im -- mor -- tel -- le,
Ve -- nus ne sau -- rait voir que d’un œil en -- vi -- eux ?
Al -- lez, je fe -- rai de mon mieux, 
et suis ra -- vi de m’em -- ploy -- er pour el -- le :

Ve -- nus m’a fait d’é -- tran -- ges tours
sur la foi con -- ju -- ga -- le ;
- ga -- le ;
Mais je veux l’en pu -- nir en prê -- tant mon se -- cours,
au tri -- om -- phe de sa ri -- va -- le.
Mais je veux l’en pu -- nir en prê -- tant mon se -- cours,
en prê -- tant mon se -- cours
au tri -- om -- phe de sa ri -- va -- le.
%% Zéphir
Fai -- tes tout pour l’A -- mour, et rien con -- tre Ve -- nus.
Pen -- ser à la ven -- geance, a -- bus,
Vul -- cain, a -- bus : - bus :
Quel -- ques tours que nous fasse u -- ne moi -- tié co -- quet -- te,
le meil -- leur est de n’y ja -- mais son -- ger,
il est tou -- jours trop tard de se ven -- ger,
l’af -- faire est fai -- te.
Il est tou -- jours trop tard de se ven -- ger,
l’af -- faire est fai -- te.
Je re -- tourne à Psy -- ché, que je vais é -- veil -- ler,
Cy -- clo -- pes, ex -- ci -- tez vos bras à tra -- vail -- ler.
