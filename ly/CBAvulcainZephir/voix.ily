\ffclef "vhaute-contre" <>^\markup\character Zéphir
r8 si16 re'16 si8 si16 si16 sol8 sol16 sol16 sol8 sol16 fad16 |
la8 la8 r16 la16 la16 si16 do'8 do'16 mi'16 do'8 do'16 si16 |
si4\trill 
\ffclef "vhaute-contre" <>^\markup\character Vulcain
r16 sol'16 sol'16 sol'16 mi'8 mi'8 |
dod'4\trill dod'8 dod'8 re'4 re'8 mi'8 |
fad'8 fad'8 re'8. re'16 re'4 do'8. do'16 |
do'4 si8 do'8 la4 la8 si8 |
sol2 
\ffclef "vhaute-contre" <>^\markup\character Zéphir
r8 si8 |
si4. si8 si8 do'8 |
sold2 sold8 sold8 |
la2 la8 sold8 |
la4 la4 do'8 do'8 |
do'2 do'8 re'8 |
si2\trill si8 dod'8 |
re'2 re'8 dod'8 |
re'2 re'4 |
r8 la8 la4. si8 |
do'4. do'8 do'4 |
si4 mi'4. fad'8 |
red'2\trill r4 |
si4 si4. si8 |
si2 si8 si8 |
mi'2 mi'8 red'8 |
mi'2 mi'4 |
r4 si4 do'4 |
re'4 re'4 re'4 |
mi'4. re'8 do'4 |
si4 la4 sol4 |
fad2.\trill |
re'4. re'8 re'8 re'8 |
mi'4 fad'4. sol'8 |
sol'4 ( fad'2 )|
sol'2 re'8 mi'8 |
fa'4 fa'4 ( mi'8 ) fa'8 |
mi'4. re'8 do'4 |
si4 la4 sol4 |
fad2.\trill |
re'4. re'8 re'8 re'8 |
mi'4 fad'4. sol'8 |
si4( la2\trill) |
sol4. 
\ffclef "vhaute-contre" <>^\markup\character Vulcain
sol'8 re'8 re'8 |
si4 si16 si16 si16 si16 do'8 do'16 do'16 |
la2 fa'8 fa'8 |
fa'4 mi'8 fa'8 re'2\trill |
do'4 r8 sol16 sol16 do'8 do'16 re'16 |
mi'8. mi'16 mi'8. mi'16 fad'8. sol'16 |
fad'4\trill fad'8 re'8 re'4 re'8 do'16 si16 |
do'4 do'8 re'8 si4\trill si8 do'8 |
la4 r8 mi'8 do'8 do'16 do'16 do'8 do'16 si16 |
si4.\trill sol'8 mi'8 mi'8 |
dod'8.\trill mi'16 mi'8 fad'8 re'8. mi'16 mi'4\trill |
re'1 |
r4 re'4 si4. sol8 |
re'4 re'4 mi'4. fad'8 |
sol'2 do'4. do'8 |
do'2 si4. do'8 |
la2\trill la4 r4 |
la2\trill la4 la8 si8 |
do'4 do'8 re'8 mi'4 mi'8 mi'8 |
re'4 re'8 do'8 si2\trill |
r4 mi'8 si8 do'8 ([ si8 do'8 re'8 ]|
mi'8 [ fa'8 sol'8 mi'8 ] fa'4 ) fa'8 sol'8 |
fa'8 [ mi'8 ] re'8 [ do'8 ] si2\trill |
la2 r4 la'8 sol'8 |
fad'4 fad'8 mi'8 re'4 re'8 do'8 |
si4 do'8 re'8 sol4 sol8 sol8 |
do'4 do'8 si8 la2\trill |
r4 re'8 do'8 si8 ([ la8 si8 do'8 ]|
re'8 [ mi'8 fad'8 re'8 ] sol'4 ) sol'8 fad'8 |
mi'8 [ re'8 ] do'8 [ si8 ] si4 ( la4\trill) |
sol2. |
\ffclef "vhaute-contre" <>^\markup\character Zéphir
r4 sol'4. re'8 |
mi'4 re'4.\trill do'16 [ si16] |
do'4. re'8 mi'4 |
si4 si4 do'4 |
la2\trill r8 re'8 |
si4.\trill si8 dod'8 re'8 |
dod'4\trill la4 re'4 |
r8 mi'8 dod'4.\trill re'8 |
re'2. |
re'2 fad'8 sol'8 |
mi'4 mi'4. fad'8 |
red'4.\trill red'8 red'8 red'8 |
mi'8 fad'8 fad'2\trill |
mi'4. si8 si8 do'8 |
re'4. re'8 re'4 |
mi'4 la4.\trill la8 |
la4 r8 re'8 la8 si8 |
do'8 re'8 mi'8 mi'8 fad'8 sol'8 |
fad'2\trill r8 la8 |
si8 do'8 la2\trill |
sol4 r8 re'8 la8 si8 |
do'8 re'8 mi'8 mi'8 fad'8 sol'8 |
fad'2\trill r8 la8 |
si8 do'8 la2\trill |
sol1 |
r8 sol16 sol16 do'8 do'16 mi'16 do'8 sol16 la16 sib8 sib16 la16 |
la8\trill do'8 fa'8. fa'16 fa'8 la8 |
si8 si8 do'8 do'8 do'8 si8 |
do'1*3/4 |
