\key sol \major
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*36
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 2/2 \midiTempo#160 s1
\bar ".|:" s1*4 \alternatives s1 s1 s1*12
\digitTime\time 3/4 s2. \bar ".|:" s2.*8 \alternatives s2. s2. s2.*14
\digitTime\time 2/2 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*3 \bar "|."
