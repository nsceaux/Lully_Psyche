Ar -- rê -- tez, cher a -- mant, où fuy -- ez- vous si vi -- te ?
Ar -- rê -- tez, A -- mour, ar -- rê -- tez.
Pou -- vez- vous me lais -- ser tris -- te, seule, in -- ter -- di -- te ?
Je meurs, puis -- que vous me quit -- tez.
J’ai vou -- lu vous voir, c’est mon cri -- me,
ma ten -- dresse a cau -- sé mon trop d’em -- pres -- se -- ment.
Et ne de -- vait- il pas pa -- raî -- tre lé -- gi -- ti -- me,
du moins aux yeux de mon a -- mant ?

Ciel ! le fu -- neste ex -- cès de mon in -- qui -- é -- tu -- de,
oc -- cu -- pait à tel point mon es -- prit af -- fli -- gé,
que je ne voy -- ais point ce beau pa -- lais chan -- gé
en une af -- freu -- se so -- li -- tu -- de.
