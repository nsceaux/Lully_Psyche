\clef "vbas-dessus" <>^\markup\character Femme affligée
r2 sol'2. sol'4 |
sol'8[\melisma lab' sib' lab'] sib'[ do'' lab' sib']\melismaEnd sol'4.\trill sol'8 |
sol'[\melisma lab' sol' lab'] sib'[ sol'] fa'16[ mi' fa' re']\melismaEnd mib'4 r8 fa' |
\appoggiatura fa'8 sol'2 sol' r |
r mib''2. mib''4 |
mib''8[\melisma re''] do''4( sib')\melismaEnd sib'8. sib'16 sib'16[\melisma do'' sib' do'' la']\melismaEnd la' sib' sib'( |
la'2\trill) la' r4 fa'' |
mib''8[ re''] do''[\melisma si']( do''16)[ si' do'' re''] sol'[ si' do'' re''] mib''2~ |
mib''\melismaEnd re''2.\trill re''4 |
re''8[\melisma mib'' do'' la'] do''[ si' si'8. la'16]\melismaEnd sol'4 mib''16[ re'' do''8] |
do''4.(\melisma re''8) do''4. re''8 si'4.(\trill do''8)\melismaEnd |
do''1 sol'4. mib'8 |
sib'2 sib'4 r16 sib'[ do'' re''] mib''4 re'' |
do''2\trill do'' lab'4. lab'8 |
lab'8[ sib' lab'] sol'[ fa'] lab'[ sol' lab' sib' do''16 sib'] do''[ lab'!] lab'8( |
sol'2)\trill sol' mib''4. mib''8 |
mib''2.( re''4) re'' r8 re'' |
re''4\melisma mib'' fa''8[ mib'']\melismaEnd re''[ do''] sib'[ la'] sib'[ sol'] |
sol'[ fad'] fad'2. sol'4. la'8 |
\appoggiatura la'8 sib'4 sib' si'8 do'' si' si' si'4.( do''16[ re''] |
do''4) do''4. do''8 do''[ re'' mib'' re'' do''16 si'] do'' do'' |
si'8[ do'' la' si'] si'2 r |
r sol'2. sol'4 |
sol'8[ lab' sib' lab' sib' do'' lab' sib'] sol'4.\trill sol'8 |
sol'[\melisma do'' sib' lab'] sol'[ fa'] mib'16[ fa' re' mib']\melismaEnd fa'4. fa'8 |
\appoggiatura fa'8 sol'2 sol' r |
r mib''2. mib''4 |
mib''8.[\melisma re''16] do''8.[ sib'16]\melismaEnd sib'8. sib'16 sib'4\trill\melisma do''16[ sib' do'' la']\melismaEnd la'8 sib' |
la'2\trill la' r4 fa'' |
mib''8[ re''] do''[\melisma si'] do''[ si' do'' re'']( sol'16)[ si' do'' re''] mib''8.[ fa''16]( |
mib''4.)( mib''16[ fa''])\melismaEnd re''2.\trill re''4 |
re''8.[\melisma mib''16] re''[ do'' sib' do''] la'[ do'' si'8 sol']\melismaEnd sol' do''16[\melisma si' do'' re''] mib''[ re'' mib'' do'']\melismaEnd |
do''4.\melisma si'8 do''[ si' do'' la'] si'[ do'' si'16 do'' si']\melismaEnd si'( |
do''1.) |
