\piecePartSpecs
#`((basse-continue #:score-template "score-basse-voix")
   (basse #:score-template "score-basse-voix")
   (dessus #:score-template "score-2dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#64 #}))
