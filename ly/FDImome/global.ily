\key re \minor \midiTempo#160
\digitTime\time 3/4 s2.*14 \alternatives s2. { \digitTime\time 2/2 s1 }
\bar ".!:" s1*14 \alternatives s1 s1 \bar "|."
