Je cherche à mé -- di -- re,
sur la terre et dans les cieux ;
Je sou -- mets à ma sa -- ti -- re,
le plus grand, le plus grand des Dieux. Dieux.

Il n’est dans l’u -- ni -- vers que l’A -- mour qui m’é -- ton -- ne,
il est le seul que j’é -- pargne au -- jour -- d’hui ; 
Il n’ap -- par -- tient qu’à lui 
de n’é -- par -- gner per -- son -- ne.
Il n’ap -- par -- tient qu’à lui 
de n’é -- par -- gner per -- son -- ne.
Il - ne.
