\tag #'couplet1 {
  Est - on sa -- ge
  dans le bel â -- ge,
  est - on sa -- ge
  de n'ai -- mer pas ?
  Que sans ces -- se
  l'on se pres -- se
  de goû -- ter les plai -- sirs i -- ci - bas :
  la sa -- ges -- se
  de la jeu -- nes -- se,
  c'est de sa -- voir jou -- ir de ses ap -- pas.
}
\tag #'couplet2 {
  L'a -- mour char -- me 
  ceux qu'il dé -- sar -- me, 
  l'a -- mour char -- me,
  cé -- dons - lui tous.
  No -- tre pei -- ne
  se -- rait vai -- ne
  de vou -- loir ré -- sis -- ter à ses coups :
  quel -- que chaî -- ne
  qu'un a -- mant pren -- ne,
  la li -- ber -- té n'a rien qui soit si doux.
}
