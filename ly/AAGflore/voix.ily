\clef "vbas-dessus" <>^\markup\character Flore
do''4 si'2\trill |
do''2 sol'4 |
la'4 sib'8 [ la'8 ] sol'8 [ fa'8 ]|
mi'2\trill mi'4 |
do''4 si'2\trill |
do''2 sol'4 |
la'4 sib'8 [ la'8 ] sol'8 [ fa'8 ]|
mi'2.\trill |
sol'4 do''2 |
la'2\trill la'4 |
si'4 do''2\trill |
re''2 re''4 |
do''4 do''2 |
do''4. re''8 si'4 ~ |
si'8 la'8 la'4.\trill sol'8 |
sol'2. |
re''4 re''2 |
mi''2 mi''4 |
do''4 la'4. re''8 |
si'2\trill si'4 |
do''4 re''4 mi''4 |
fa''4. re''8 mi''4 ~ |
mi''8 fa''8 re''4.\trill do''8 |
do''2. |
