Ve -- nez voir ce pa -- lais, où pour char -- mer votre â -- me,
les plai -- sirs naî -- tront tour à tour.
Et vous, di -- vi -- ni -- tés qui con -- nais -- sez ma flam -- me,
mar -- quez par nos chan -- sons le pou -- voir, le pou -- voir de l’A -- mour.
Et vous, di -- vi -- ni -- tés qui con -- nais -- sez ma flam -- me,
mar -- quez par nos chan -- sons le pou -- voir, le pou -- voir de l’A -- mour.
