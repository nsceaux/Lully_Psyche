\newBookPart#'()
\act "Acte Premier"
\sceneDescription\markup\justify {
  Le Théâtre représente d’un côté, un agréable paysage au pied
  d’une montagne qui s’élève jusqu’au Ciel : De l’autre côté,
  on voit paroître une campagne à perte de vue.
}
\scene "Scène Première" "Scène 1"
\sceneDescription\markup\wordwrap-center\smallCaps { Aglaure, Cidippe. }
%% 1-1
\pieceToc\markup\wordwrap {
  Aglaure, Cidippe : \italic { Enfin ma sœur le ciel est apaisé }
}
\includeScore "BAAritournelle"
\includeScore "BABaglaureCidippe"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène II" "Scène 2"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Aglaure, Cidippe, Lycas.
}
%% 1-2
\pieceToc\markup\wordwrap {
  Aglaure, Cidippe, Lycas : \italic { Ah, princesses ! }
}
\includeScore "BBAaglaureCidippeLycas"
%% 1-3
\sceneDescription\markup\fontsize#-2 \justify {
  Une troupe de personnes désolées viennent vers la montagne déplorer
  la disgrace de Psyché. Leurs plaintes sont exprimées par une Femme
  & par deux Hommes affigés. Ils sont suivis de six personnes qui jouent
  de la Flûte, & de huit autres qui portent des Flambeaux sembables à
  ceux dont les Anciens se servaient dans les Pompes funèbres.
}
\pieceToc\markup { Plainte italienne }
\includeScore "BBBritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 1-4
\pieceToc\markup\wordwrap {
  Femme et hommes affligés : \italic { Deh piangete al pianto mio }
}
\includeScore "BBCaffliges"
\newBookPart#'(full-rehearsal full-urtext)
%% 1-5
\pieceToc\markup\wordwrap {
  Femme affligée : \italic { Rispondete a miei lamenti }
}
\markupCond#(or (not (symbol? (ly:get-option 'part)))
                (eqv? 'basse-continue (ly:get-option 'part)))
\markup { [Version manuscrit Versailles] } \noPageBreak
\includeScore "BBDaffliges"
\newBookPart#'(full-rehearsal full-urtext)
\markupCond#(or (not (symbol? (ly:get-option 'part)))
                (eqv? 'basse-continue (ly:get-option 'part)))
\markup { [Version édition Ballard] } \noPageBreak
\includeScore "BBDBaffliges"
\newBookPart#'(full-rehearsal full-urtext basse-continue)
%% 1-6
\pieceToc\markup {
  Hommes affligés : \italic { Com'esser può fra voi }
}
\includeScore "BBEaffliges"
\newBookPart#'(taille)
%% 1-7
\sceneDescription\markup\fontsize#-2 \justify {
  Ces plaintes sont entrecoupées ici par une Entrée de Ballet qui
  se fait par les huit personnes qui portent les flambeaux.
}
\pieceToc "Air"
\includeScore "BBFritournelle"
%% 1-8
\pieceToc\markup\wordwrap {
  Femme désolée : \italic { Ahi! ch'indarno si tarda! }
}
\includeScore "BBGdesolee"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène III" "Scène 3"
\sceneDescription \markup \wordwrap-center\smallCaps {
  Aglaure, Cidippe, Psyché.
}
%% 1-9
\pieceToc\markup\wordwrap {
  Aglaure, Cidippe, Psyché :
  \italic { Psyché vient ; à la voir je tremble }
}
\includeScore "BCAaglaureCidippePsyche"


\scene "Scène IV" "Scène 4"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Psyché, le Roi.
}
%% 1-10
\pieceToc\markup\wordwrap {
  Psyché, le Roi : \italic { Seigneur, vous soupirez vous-même ? }
}
\includeScore "BDApsycheRoi"
\sceneDescription\markup\wordwrap-center {
  Quatre Zephirs volent vers Psyché qui est sur la Montagne,
  & l'enlèvent sur le Ceintre.
}
\newBookPart#'(full-rehearsal full-urtext)
%% 1-11
\pieceToc\markup [Entr’acte]
\reIncludeScore "AADritournelle" "BDBentracte"
\actEnd \markup { FIN DU PREMIER ACTE }
