\piecePartSpecs
#`((basse-continue #:tag-global violons)
   (timbales #:tag-global ())
   (trompette #:score "score-trompette")
   (trompette-timbales)
   (basse #:tag-global violons)
   (quinte #:tag-global violons)
   (taille #:tag-global violons)
   (haute-contre #:tag-global violons)
   (dessus #:tag-global violons)
   (silence #:on-the-fly-markup , #{ \markup\tacet#40 #}))
