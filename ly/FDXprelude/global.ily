\key re \major \midiTempo#108
<<
  { \digitTime\time 2/2 \partial 8 s8 \bar ".!:" s1*19
    \alternatives s1 s1 \bar "|." }
  \tag #'violons { 
    s8_\markup\bold\italic f | s1*2 | s8 s8_\markup\bold\italic d s2. |
    s8 s2_\markup\bold\italic f s4._\markup\bold\italic d | s8 s2_\markup\bold\italic f s4._\markup\bold\italic d | s8 s2_\markup\bold\italic f s4._\markup\bold\italic d |
    s8 s8_\markup\bold\italic f s2. | s2 s8 s4._\markup\bold\italic d | s2 s8 s4._\markup\bold\italic f |
    s8 s2_\markup\bold\italic d s4._\markup\bold\italic f | s8 s2_\markup\bold\italic d s4._\markup\bold\italic f | s1 |
    s8 s4._\markup\bold\italic d s2 | s8 s8_\markup\bold\italic f s2. | s8 s2_\markup\bold\italic d s4._\markup\bold\italic f | s2
    s8 s4._\markup\bold\italic d | s8 s4._\markup\bold\italic f s2 | s1 | s8 s4._\markup\bold\italic d
  }
>>
