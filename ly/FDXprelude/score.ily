\score {
  <<
    \new StaffGroup <<
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "dessus"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "taille"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "quinte"
      >>
      \new Staff <<
        \keepWithTag #'violons \global \includeNotes "basse"
      >>
    >>
    \new StaffGroup <<
      \new Staff <<
        \keepWithTag #'() \global \includeNotes "trompette"
        <>^"Trompettes"
      >>
      \new Staff <<
        \keepWithTag #'() \global \includeNotes "timbales"
        <>_"Timbales"
      >>
    >>
    \new Staff << 
      \keepWithTag #'() \global \includeNotes "basse-continue"
    >>
  >>
  \layout { }
  \midi { }
}