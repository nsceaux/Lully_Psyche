Vous m’a -- ban -- don -- nez donc, cru -- el, et cher a -- mant,
ve -- nez, ve -- nez me trai -- ter de cou -- pa -- ble.
Mal -- gré tous les mal -- heurs dont le des -- tin m’ac -- ca -- ble,
votre ab -- sence est mon seul tour -- ment.
Dou -- ces, mais trom -- peu -- ses dé -- li -- ces,
de -- viez "-vous" com -- men -- cer et fi -- nir en un jour ?
À pei -- ne ai-je goû -- té les dou -- ceurs de l’A -- mour,
que j’en res -- sens les plus af -- freux sup -- pli -- ces.
Pour -- quoi cher -- cher le che -- min des en -- fers ?
C’est la mort, c’est la mort qui me le doit ap -- pren -- dre :
Les flots qu’aux mal -- heu -- reux ce fleu -- ve tient ou -- verts,
m’of -- frent ce -- lui que je dois pren -- dre.
