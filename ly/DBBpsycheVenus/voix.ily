\ffclef "vbas-dessus" <>^\markup\character-text Psyché apercevant Venus
r4 r8 fa'16 fa'16 sib'8 sib'16 do''16 |
re''8 re''8 re''8 re''8 re''8 fa''8 |
sib'8 sib'8 r8 sib'16 sib'16 fa'8 fa'16 sol'16 |
la'8 la'8 la'8 la'8 sib'8 do''8 |
la'4\trill 
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 fa'8 do''8. do''16 do''8 re''8 |
mib''4 mib''8 mib''8 do''4 do''8 re''8 |
si'4\trill si'8 re''8 re''8 re''8 re''8 mib''8 |
do''4 do''8 do''8 do''4 do''8 si'8 |
do''8 mib''16 mib''16 do''8. do''16 sol'8 sol'16 la'16 |
sib'4 sib'8 re''16 re''16 re''8 re''16 re''16 |
mib''4 do''8 sib'8 la'4 la'8 sib'8 |
sol'4 
\ffclef "vbas-dessus" <>^\markup\character Psyché
sib'4 sib'8 r16 sib'16 sib'8 re''8 |
sol'4 sib'8 do''8 re''4 re''8 mib''8 |
fa''8 fa''8 re''8 re''16 fa''16 sib'8 sib'8 sib'8 sib'8 |
sol'8 sib'16 sib'16 mib''8 mib''16 mib''16 do''16 do''16 do''16 do''16 sol'8 do''8 |
la'4\trill r16 do''16 do''16 do''16 fa''8 do''8 re''8 mib''8 |
re''8\trill re''8 sib'8 sib'16 re''16 sol'4 sol'8 sol'16 la'16 |
fa'4 r8 do''8 la'4 la'8 la'16 la'16 |
re''4 re''8 re''8 re''4 re''8 la'8 |
sib'8 sib'8 r8 sib'16 sib'16 mib''8 mib''16 mib''16 |
do''8 do''8 re''8 re''8 re''8 mib''8 |
do''4\trill r8 fa'16 sol'16 la'8 la'16 sib'16 |
do''8 do''8 re''8 re''8 re''8 re''8 |
sol'8 r16 mib''16 mib''8 mib''8 do''8 do''16 do''16 sol'8 la'16 sib'16 |
la'4\trill la'8 r16 fa''16 re''8 re''8 mib''8 fa''8 |
sol''8 sol''8 do''8. do''16 do''8 re''8 |
sib'4
\ffclef "vbas-dessus" <>^\markup\character Venus
re''4 r4 re''8 re''8 |
sol'8. sol'16 sol'8 fa'8 fa'8 mi'8 |
mi'8\trill mi'8
\ffclef "vbas-dessus" <>^\markup\character Psyché
do''4 r8 do''8 la'8 la'16 la'16 |
fad'4 fad'8 fad'8 sol'4 sol'8 la'8 |
sib'8 sib'16 sib'16 mib''8 mib''16 re''16 do''8 re''16 mib''16 |
re''8\trill fa''16 fa''16 sib'8 sib'16 do''16 re''8 re''16 re''16 re''8 mi''16 fa''16 |
mi''4\trill do''8 do''8 fa'8 fa'16 fa'16 fa'8 mi'8 |
fa'2 fa'4 r4 |
r4 fa''4 fa''4 re''4 |
mib''4. re''8 do''4.\trill do''8 |
do''1 |
re''2 sib'4. sib'8 |
sol'2 mib''4. mib''8 |
do''2 do''8 [ sib'8.\trill la'16 ] sib'8 |
la'2\trill la'4 la'4 |
sib'4 sib'4 sib'4. do''8 |
re''2 re''4 mib''4 |
fa''2 mib''4. re''8 |
do''2\trill do''4 fa''4 |
re''2 re''4 mi''4 |
fa''2 mi''4.\trill fa''8 |
fa''1 |
re''2 sib'4. sib'8 |
sol'2 mib''4. mib''8 |
do''2 do''8 [ sib'8.\trill la'16 ] sib'8 |
la'2\trill la'4 r4 |
r4 fa''4 fa''4 re''4 |
mib''4 re''4 do''4.\trill sib'8 |
sib'4 
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 re''8 re''8 re''8 re''8 re''8 |
mib''8. mib''16 mib''8 re''8 do''8 sib'8 |
la'8\trill la'8 
\ffclef "vbas-dessus" <>^\markup\character Psyché
re''4 r8 fad'16 fad'16 fad'8 sol'8 |
la'4 la'8 la'8 re''4 re''8 la'8 |
sib'8 sib'8 r8 sib'16 sib'16 do''4 do''8 do''8 |
re''4 r8 re''16 mi''16 fa''4. fa''16 mi''16 |
fa''2 
\ffclef "vbas-dessus" <>^\markup\character Venus
la'8 la'16 la'16 la'8 do''8 |
fa'8 
\ffclef "vbas-dessus" <>^\markup\character Psyché
re''8 re''8 fa''8 sib'4 
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 re''8 |
sol'8 sol'8 
\ffclef "vbas-dessus" <>^\markup\character Psyché
mib''4 sol'8 sol'8 sol'8 la'8 |
sib'4 sib'8 sib'8 si'4\trill si'8 do''8 |
do''4 
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 do''8 sol'4 r16 mib''16 mib''16 mib''16 |
do''4 do''8 do''8 sol'4 sol'8 la'8 |
sib'4 r8 re''8 sol'8 r16 sib'16 sib'8 sib'8 |
sol'8 sol'8 sol'8 fa'8 fa'8 mi'8 |
mi'4\trill mi'8 do''8 sol'8. sol'16 |
lab'4 lab'8. sib'16 sol'4 lab'8 sib'8 |
lab'4\trill r8 fa'16 fa'16 do''8 do''8 do''8 do''16 re''16 |
sib'8 sib'8 r8 sib'16 sib'16 do''8 do''16 do''16 re''8 mib''8 |
re''4\trill 
\ffclef "vbas-dessus" <>^\markup\character Psyché
r8 sib'8 sib'8 sib'8 sib'8 sib'8 |
sol'4 
\ffclef "vbas-dessus" <>^\markup\character Venus
r8 sib'8 mib''8 mib''8 mib''8 mib''8 |
do''4 do''8 do''8 do''4 re''8 mib''8 |
fa''4 mib''8 re''8 do''4\trill do''8 re''8 |
sib'8 sib'8 r4 r4
