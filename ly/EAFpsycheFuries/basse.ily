\clef "basse" sib,1 |
sib2 mib2 |
fa4 fa4 fa4 mib4 |
re4 re4 re4 do4 |
sib,2 sib4. sib8 |
la2. sib4 |
sol1 |
fa2 fa4 fa4 |
do4. do8 sol8 sol,8 |
re2. sib,4 |
do2. do4 |
re2. re,4 |
sol,2 sol4. sol8 |
re4. re8 mib8 do8 |
fa2. re4 |
sol2. mib4 |
fa2. fa,4 |
sib,1 |
%%
sib,1 | \allowPageTurn
la,4 re8 do8 sib,4. la,8 |
sol,4 sol8. la16 sib4 |
re1 |
mib2. |
mi2. |
fa4 fa,4 sib,2 |
do4 re8 sib,8 do2 |
fa,2. fa4 | %%%
sib2. sib,4 |
mib2 mib4 mib4 |
lab2. fa4 |
sol2. sol4 |
do2. do4 |
fa2 fa4 fa4 |
re2. re4 |
sol2. sol4 |
mib2. mib4 |
lab2. lab4 |
fa2. fa4 |
sib2. sib4 |
sol2. mib4 |
sib,2. sib,4 |
mib2. mib4 |
sib2. sib,4 |
fa2. fa4 |
do2. do4 |
sol2. sol4 |
re2. re4 |
mib2. do4 |
fa2. fa,4 |
sib,2 sib,4 r4 | %%%
sib,4 sib4 si2 |
do'4. do8 lab2 |
sol2. |
si,2. |
do4 do'4 mi2 |
fa4 fa,4 re4. do8 |
sib,2 do2 |
fa2. fa4 | %%%
sib2. sib,4 |
mib2 mib4 mib4 |
lab2. fa4 |
sol2. sol4 |
do2. do4 |
fa2 fa4 fa4 |
re2. re4 |
sol2. sol4 |
mib2. mib4 |
lab2. lab4 |
fa2. fa4 |
sib2. sib4 |
sol2. mib4 |
sib,2. sib,4 |
mib2. mib4 |
sib2. sib,4 |
fa2. fa4 |
do2. do4 |
sol2. sol4 |
re2. re4 |
mib2. do4 |
fa2. fa,4 |
sib,2 sib,4 r4 |\allowPageTurn %%%
sib,2 sib2 |
la2. |
sol2 la4 |
sib2. |
re2. |
mib4 re8 do8 sib,16 la,16 sol,8 |
fa,2. fa4 | %%%
sib2. sib,4 |
mib2 mib4 mib4 |
lab2. fa4 |
sol2. sol4 |
do2. do4 |
fa2 fa4 fa4 |
re2. re4 |
sol2. sol4 |
mib2. mib4 |
lab2. lab4 |
fa2. fa4 |
sib2. sib4 |
sol2. mib4 |
sib,2. sib,4 |
mib2. mib4 |
sib2. sib,4 |
fa2. fa4 |
do2. do4 |
sol2. sol4 |
re2. re4 |
mib2. do4 |
fa2. fa,4 |
sib,2 sib,4 r4 | %%%
sib2 fad2 |
sol2 ~ sol4. re8 |
mib4 do4 sol4 sol,4 |
do1 |
sol,2 re4 |
do4 sib,4 fa,2 |
sib,4. |
%%
sib4 sib8 |
fa4 fa8 |
sol4 sol8 |
fa8 mib4 |
re4. |
sib,4 sib,8 |
mib8 do8 re8 |
sol,4. |
sol4 sol8 |
do4 do8 |
fa4 fa8 |
fa8 re8 mib8 |
sib,4. |
sib,4 sol,8 |
re8 sib,8 do8 |
fa,4. |
sib4 sol8 |
lab4 fa8 |
sol4 sol8 |
re8 mib8 do8 |
fa4 fa8 |
re4 mib8 |
do4 sib,8 |
fa8 fa,4 |
sib,4 sol8 |
lab4 fa8 |
sol4 sol8 |
re8 mib8 do8 |
fa4 fa8 |
re4 mib8 |
do4 sib,8 |
fa8 fa,4 |
sib,4.*1/2 \sugNotes { do16^"[Ballard]" sib, la, } |

