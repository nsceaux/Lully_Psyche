<<
  \tag #'(furie1 basse) {
    \tag #'basse <>^\markup\character Les trois Furies
    \clef "vhaute-contre" r4 re'4 re'4 mib'4 |
    fa'4 fa'4 mib'4. re'8 |
    do'2 do'4 do'4 |
    re'2 re'4 mib'4 |
    re'4 re'4 fa'4. fa'8 |
    fa'2 fa'4 fa'4 |
    fa'2 ( mi'2 )|
    fa'2 do'4 re'4 |
    mib'4 mib'8 fa'8 re'8 sol'8 |
    fad'2\trill fad'4 sol'4 |
    mib'2 do'4. sib8 |
    la2\trill la4. sib8 |
    sol2 re'4. mi'8 |
    fa'4 fa'8 fa'8 sol'8 sol'8 |
    la'2 fa'4 fa'4 |
    re'2 re'4 sol'4 |
    do'2 do'4 fa'4 |
    re'1\trill |
  }
  \tag #'furie2 {
    \clef "vtaille" r4 sib4 sib4 do'4 |
    re'4 re'4 do'4. sib8 |
    la2 la4 la4 |
    sib2 sib4 la4 |
    sib4 sib4 re'4. re'8 |
    do'2 do'4 re'4 |
    sib1 |
    la2 la4 si4 |
    do'4 sol8 la8 sib16 [ la16 ] sib8 |
    la2 la4 sib4 |
    sib2 la4.\trill sol8 |
    sol2 fad4.\trill sol8 |
    sol2 sib4. do'8 |
    re'4 re'8 re'8 sib8 mib'8 |
    do'2 la4 la4 |
    sib2 sib4 sib4 |
    sib2 sib4 la4 |
    sib1 |
  }
  \tag #'furie3 {
    \clef "vbasse" R1*2 |
    r4 fa4 fa4 mib4 |
    re4 re4 re4 do4 |
    sib,2 sib4. sib8 |
    la2 la4 sib4 |
    sol1 |
    fa2 fa4. fa8 |
    do4 do8 do8 sol8 sol,8 |
    re2 re4 sib,4 |
    do2 do4. do8 |
    re2 re4 re4 |
    sol,2 sol4. sol8 |
    re4 re8 re8 mib8 do8 |
    fa2 fa4 re4 |
    sol2 sol4 mib4 |
    fa2 fa4 fa,4 |
    sib,1 |
  }
  \tag #'psyche { \clef "vbas-dessus" R1*8 R2. R1*4 R2. R1*4 }
>>
<<
  \tag #'(psyche basse) {
    \clef "vbas-dessus" <>^\markup\character Psyché
    r4 r8 sib'8 sib'8 sib'8 sib'8 do''8 |
    fad'4 fad'8 fad'8 sol'4 sol'8 la'8 |
    sib'8 sib'8 r8 sib'16 do''16 re''8 re''16 fa''16 |
    sib'4 sib'8 sib'8 fa'4 fa'8 sib'8 |
    sol'4\trill r8 mib''8 mib''16 mib''16 mib''16 mib''16 |
    do''8 do''8 do''8 sib'8 sib'8 la'8 |
    la'8\trill la'8 do''8 do''8 re''4 sib'8 sib'8 |
    sib'4 la'8 sib'8 sol'4 sol'8 la'8 |
    fa'2 r2 |
  }
  \tag #'(furie1 furie2) { R1*2 R2. R1 R2.*2 R1*2 R1 }
  \tag #'furie3 { R1*2 R2. R1 R2.*2 R1*2 r2 r4 fa4 | }
>>
<<
  \tag #'psyche { R1*23 }
  \tag #'(furie1 basse) {
    \tag #'basse \clef "vhaute-contre" <>^\markup\character Les trois Furies
    r2 r4 fa'4 |
    sol'2 sol'4 sol'4 |
    mib'4 mib'4 mib'4. fa'8 |
    re'2 re'4 re'4 |
    mi'4 mi'4 mi'4 mi'4 |
    do'2. do'4 |
    fa'4 fa'4 fa'4 fa'4 |
    re'2 re'4 re'4 |
    sol'2 sol'4 sol'4 |
    mib'2 mib'4 mib'4 |
    lab'4 lab'4 lab'4 lab'4 |
    fa'2 fa'4 fa'4 |
    sol'2 sol'4. lab'8 |
    fa'2 sol'4. lab'8 |
    sol'2 sol'4 sol'4 |
    fa'2 re'4 re'4 |
    do'4 do'4 do'4 re'4 |
    mib'2 mib'4 mib'4 |
    re'4 re'4 re'4 mib'4 |
    fa'2 re'4 re'4 |
    sib2 sib4 mib'4 |
    do'2 do'4. fa'8 |
    re'2 re'4 r4 |
  }
  \tag #'furie2 {
    r2 r4 re'4 |
    sib2 sib4 mib'4 |
    do'4 do'4 do'4. re'8 |
    si2 si4 si4 |
    do'4 do'4 do'4 do'4 |
    la2. la4 |
    re'4 re'4 re'4 re'4 |
    sib2 sib4 sib4 |
    mib'2 mib'4 mib'4 |
    do'2 do'4 do'4 |
    fa'4 fa'4 fa'4 fa'4 |
    re'2 re'4 re'4 |
    mib'2 mib'4 mib'4 |
    mib'2 mib'4 re'4 |
    mib'2 mib'4 mib'4 |
    re'2 sib4 sib4 |
    la4 la4 la4 si4 |
    do'2 do'4 la4 |
    sib4 sib4 sib4 do'4 |
    re'2 sib4 sib4 |
    sol2 sol4 do'4 |
    la2 la4 la4 |
    sib2 sib4 r4 |
  }
  \tag #'furie3 {
    sib2 sib,4 sib,4 |
    mib4 mib4 mib4 mib4 |
    lab4 lab4 lab4 fa4 |
    sol4 sol4 sol4 sol4 |
    do2 do4 do4 |
    fa4 fa4 fa4 fa4 |
    re2. re4 |
    sol4 sol4 sol4 sol4 |
    mib2 mib4 mib4 |
    lab2 lab4 lab4 |
    fa2 fa4 fa4 |
    sib4 sib4 sib4 sib4 |
    sol2 sol4 mib4 |
    sib,2 sib,4 sib,4 |
    mib2 mib4 mib4 |
    sib2. sib,4 |
    fa2 fa4 fa4 |
    do4 do4 do4 do4 |
    sol2 sol4 sol4 |
    re4 re4 re4 re4 |
    mib2 mib4 do4 |
    fa2 fa4 fa,4 |
    sib,2 sib,4 r4 |
  }
>>
<<
  \tag #'(psyche basse) {
    \tag #'basse \clef "vbas-dessus" <>^\markup\character Psyché
    r4 fa''4 r4 re''16 re''16 re''16 re''16 |
    mib''4 mib''8 mib''8 do''4 do''8 re''8 |
    si'4.\trill si'8 si'8 re''8 |
    sol'4 sol'8 sol'16 fa'16 fa'8 mi'8 |
    mi'4\trill mi'8 do''8 sol'4 sol'8 sol'8 |
    lab'4 lab'8 lab'8 fa'4 fa'4 |
    r8 reb''8 reb''8 sib'8 sol'8. sol'16 la'8 sib'8 |
    la'2\trill r2 |
  }
  \tag #'(furie1 furie2) { R1 R1 R2.*2 R1*3 R1 }
  \tag #'furie3 { R1 R1 R2.*2 R1*3 r2 r4 fa4 | }
>>
<<
  \tag #'psyche { R1*23 }
  \tag #'(furie1 basse) {
    \tag #'basse \clef "vhaute-contre" <>^\markup\character Les trois Furies
    r2 r4 fa'4 |
    sol'2 sol'4 sol'4 |
    mib'4 mib'4 mib'4. fa'8 |
    re'2 re'4 re'4 |
    mi'4 mi'4 mi'4 mi'4 |
    do'2. do'4 |
    fa'4 fa'4 fa'4 fa'4 |
    re'2 re'4 re'4 |
    sol'2 sol'4 sol'4 |
    mib'2 mib'4 mib'4 |
    lab'4 lab'4 lab'4 lab'4 |
    fa'2 fa'4 fa'4 |
    sol'2 sol'4. lab'8 |
    fa'2 sol'4. lab'8 |
    sol'2 sol'4 sol'4 |
    fa'2 re'4 re'4 |
    do'4 do'4 do'4 re'4 |
    mib'2 mib'4 mib'4 |
    re'4 re'4 re'4 mib'4 |
    fa'2 re'4 re'4 |
    sib2 sib4 mib'4 |
    do'2 do'4. fa'8 |
    re'2\trill re'4 r4 |
  }
  \tag #'furie2 {
    r2 r4 re'4 |
    sib2 sib4 mib'4 |
    do'4 do'4 do'4. re'8 |
    si2 si4 si4 |
    do'4 do'4 do'4 do'4 |
    la2. la4 |
    re'4 re'4 re'4 re'4 |
    sib2 sib4 sib4 |
    mib'2 mib'4 mib'4 |
    do'2 do'4 do'4 |
    fa'4 fa'4 fa'4 fa'4 |
    re'2 re'4 re'4 |
    mib'2 mib'4 mib'4 |
    mib'2 mib'4 re'4 |
    mib'2 mib'4 mib'4 |
    re'2 sib4 sib4 |
    la4 la4 la4 si4 |
    do'2 do'4 la4 |
    sib4 sib4 sib4 do'4 |
    re'2 sib4 sib4 |
    sol2 sol4 do'4 |
    la2 la4 la4 |
    sib2 sib4 r4 |
  }
  \tag #'furie3 {
    sib2 sib,4 sib,4 |
    mib4 mib4 mib4 mib4 |
    lab4 lab4 lab4 fa4 |
    sol4 sol4 sol4 sol4 |
    do2 do4 do4 |
    fa4 fa4 fa4 fa4 |
    re2. re4 |
    sol4 sol4 sol4 sol4 |
    mib2 mib4 mib4 |
    lab2 lab4 lab4 |
    fa2 fa4 fa4 |
    sib4 sib4 sib4 sib4 |
    sol2 sol4 mib4 |
    sib,2 sib,4 sib,4 |
    mib2 mib4 mib4 |
    sib2. sib,4 |
    fa2 fa4 fa4 |
    do4 do4 do4 do4 |
    sol2 sol4 sol4 |
    re4 re4 re4 re4 |
    mib2 mib4 do4 |
    fa2 fa4 fa,4 |
    sib,2 sib,4 r4 |
  }
>>
<<
  \tag #'(psyche basse) {
    \tag #'basse \clef "vbas-dessus" <>^\markup\character Psyché
    r4 r8 sib'8 re''8 re''8 re''8 mib''8 |
    fa''8 fa''8 do''8 do''8 do''8 re''8 |
    sib'8. sib'16 mib''8 mib''16 mib''16 mib''8 mib''16 re''16 |
    re''8\trill re''8 sib'8 sib'16 sib'16 sib'8 re''8 |
    sib'4 sib'8 sib'16 lab'16 lab'8 sol'8 |
    sol'8\trill sol'8 sib'16 sib'16 sib'16 do''16 re''8 re''16 mi''16 |
    fa''2 r2 |
  }
  \tag #'(furie1 furie2) { R1 R2.*5 R1 }
  \tag #'furie3 { R1 R2.*5 r2 r4 fa4 | }
>>
<<
  \tag #'psyche { R1*23 }
  \tag #'(furie1 basse) {
    \tag #'basse \clef "vhaute-contre" <>^\markup\character Les trois Furies
    r2 r4 fa'4 |
    sol'2 sol'4 sol'4 |
    mib'4 mib'4 mib'4. fa'8 |
    re'2 re'4 re'4 |
    mi'4 mi'4 mi'4 mi'4 |
    do'2. do'4 |
    fa'4 fa'4 fa'4 fa'4 |
    re'2 re'4 re'4 |
    sol'2 sol'4 sol'4 |
    mib'2 mib'4 mib'4 |
    lab'4 lab'4 lab'4 lab'4 |
    fa'2 fa'4 fa'4 |
    sol'2 sol'4. lab'8 |
    fa'2 sol'4. lab'8 |
    sol'2 sol'4 sol'4 |
    fa'2 re'4 re'4 |
    do'4 do'4 do'4 re'4 |
    mib'2 mib'4 mib'4 |
    re'4 re'4 re'4 mib'4 |
    fa'2 re'4 re'4 |
    sib2 sib4 mib'4 |
    do'2 do'4. fa'8 |
    re'2 re'4 r4 |
  }
  \tag #'furie2 {
    r2 r4 re'4 |
    sib2 sib4 mib'4 |
    do'4 do'4 do'4. re'8 |
    si2\trill si4 si4 |
    do'4 do'4 do'4 do'4 |
    la2. la4 |
    re'4 re'4 re'4 re'4 |
    sib2 sib4 sib4 |
    mib'2 mib'4 mib'4 |
    do'2 do'4 do'4 |
    fa'4 fa'4 fa'4 fa'4 |
    re'2 re'4 re'4 |
    mib'2 mib'4 mib'4 |
    mib'2 mib'4 re'4 |
    mib'2 mib'4 mib'4 |
    re'2 sib4 sib4 |
    la4 la4 la4 si4 |
    do'2 do'4 la4 |
    sib4 sib4 sib4 do'4 |
    re'2 sib4 sib4 |
    sol2 sol4 do'4 |
    la2 la4 la4 |
    sib2 sib4 r4 |
  }
  \tag #'furie3 {
    sib2 sib,4 sib,4 |
    mib4 mib4 mib4 mib4 |
    lab4 lab4 lab4 fa4 |
    sol4 sol4 sol4 sol4 |
    do2 do4 do4 |
    fa4 fa4 fa4 fa4 |
    re2. re4 |
    sol4 sol4 sol4 sol4 |
    mib2 mib4 mib4 |
    lab2 lab4 lab4 |
    fa2 fa4 fa4 |
    sib4 sib4 sib4 sib4 |
    sol2 sol4 mib4 |
    sib,2 sib,4 sib,4 |
    mib2 mib4 mib4 |
    sib2. sib,4 |
    fa2 fa4 fa4 |
    do4 do4 do4 do4 |
    sol2 sol4 sol4 |
    re4 re4 re4 re4 |
    mib2 mib4 do4 |
    fa2 fa4 fa,4 |
    sib,2 sib,4 r4 |
  }
>>
<<
  \tag #'(psyche basse) {
    \tag #'basse \clef "vbas-dessus" <>^\markup\character Psyché
    r4 r8 re''8 la'4 r16 la'16 la'16 la'16 |
    sib'4 sib'8 sib'8 sib'4 do''8 re''8 |
    sol'4 mib''8 mib''16 mib''16 si'4 si'8 si'16 do''16 |
    do''8 do''8
    \clef "vtaille" <>^\markup\character Une Furie
    sol8 sol16 sol16 mib8. mib16 mib8. re16 |
    re8 re8 r8 re16 mib16 fa8 fa16 sib16 |
    mib4 re4 do4 do8 re8 |
    sib,4.
  }
  \tag #'(furie1 furie2 furie3) { R1*4 R2. R1 R4. }
>>
<<
  \tag #'(furie1 basse) {
    \tag #'basse \clef "vhaute-contre" <>^\markup\character Les trois Furies
    r8 fa'8 re'8 |
    do'8 do'8 fa'8 |
    re'8 re'8 re'8 |
    re'8 sol'8. la'16 |
    fad'4 fad'8 |
    r8 re'8 re'8 |
    sib8 do'8 la8 |
    sib4 sib8 |
    r8 re'8 re'8 |
    mib'8 mib'8 mib'8 |
    do'8 do'8 do'8 |
    fa'8 fa'8 sol'8 |
    fa'4 fa'8 |
    r8 re'8 mi'8 |
    fa'8 sol'8 mi'8 |
    fa'4 fa'8 |
    r8 fa'8 sol'8 |
    mib'8 mib'8 fa'8 |
    re'8 re'8 re'8 |
    fa'8 sib8 mib'8 |
    do'4 do'8 |
    r8 fa'8 sol'8 |
    mib'8 mib'8 re'8 |
    re'8( do'4)\trill |
    sib8 fa'8 sol'8 |
    mib'8 mib'8 fa'8 |
    re'8 re'8 re'8 |
    fa'8 sib8 mib'8 |
    do'4 do'8 |
    r8 fa'8 sol'8 |
    mib'8 mib'8 re'8 |
    re'8 ( do'4)\trill |
    sib4. |
  }
  \tag #'furie2 {
    r8 re'8 sib8 |
    la8 la8 la8 |
    sib8 sib8 sib8 |
    la8 sib8 do'8 |
    re'4 la8 |
    r8 sib8 sib8 |
    sol8 la8 fad8 |
    sol4 sol8 |
    r8 sib8 sib8 |
    sol8 sol8 do'8 |
    la8 la8 la8 |
    la8 la8 sib8 |
    re'4 re'8 |
    r8 sib8 sib8 |
    la8 sib8 sol8 |
    la4 la8 |
    r8 re'8 mi'8 |
    do'8 do'8 re'8 |
    sib8 sib8 sib8 |
    re'8 sol8 do'8 |
    la4 la8 |
    r8 sib8 sib8 |
    la8 la8 sib8 |
    sib8( la4)\trill |
    sib8 re'8 mi'8 |
    do'8 do'8 re'8 |
    sib8 sib8 sib8 |
    re'8 sol8 do'8 |
    la4 la8 |
    r8 sib8 sib8 |
    la8 la8 sib8 |
    sib8 ( la4) |
    sib4. |
  }
  \tag #'furie3 {
    r8 sib8 sib8 |
    fa8 fa8 fa8 |
    sol8 sol8 sol8 |
    fa8 mib8 mib8 |
    re4 re8 |
    r8 sib,8 sib,8 |
    mib8 do8 re8 |
    sol,4 sol,8 |
    r8 sol8 sol8 |
    do8 do8 do8 |
    fa8 fa8 fa8 |
    fa8 re8 mib8 |
    sib,4 sib,8 |
    r8 sib,8 sol,8 |
    re8 sib,8 do8 |
    fa,4 fa,8 |
    r8 sib8 sol8 |
    lab8 lab8 fa8 |
    sol8 sol8 sol8 |
    re8 mib8 do8 |
    fa4 fa8 |
    r8 re8 mib8 |
    do8 do8 sib,8 |
    fa8 ( fa,4) |
    sib,8 sib8 sol8 |
    lab8 lab8 fa8 |
    sol8 sol8 sol8 |
    re8 mib8 do8 |
    fa4 fa8 |
    r8 re8 mib8 |
    do8 do8 sib,8 |
    fa8 ( fa,4) |
    sib,4. |
  }
>>