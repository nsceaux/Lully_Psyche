\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
      \characterName\markup\center-column\smallCaps { Première Furie }
        \global \keepWithTag #'furie1 \includeNotes "voix"
      >> \keepWithTag #'furie1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
      \characterName\markup\center-column\smallCaps { Deuxième Furie }
        \global \keepWithTag #'furie2 \includeNotes "voix"
      >> \keepWithTag #'furie2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
      \characterName\markup\center-column\smallCaps { Troisième Furie }
        \global \keepWithTag #'furie3 \includeNotes "voix"
      >> \keepWithTag #'furie3 \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'psyche \includeNotes "voix"
    >> \keepWithTag #'psyche \includeLyrics "paroles"
    \new Staff <<
      { s1*8 s2. s1*4 s2. s1*4\break
        s1*2 s2. s1 s2.*2 s1*3\noBreak s1*23
        s1 s1 s2.*2 s1*4\noBreak s1*23
        s1 s2.*5 s1\noBreak s1*23 \break
        s1*4 s2. s1 s4.\break }
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
