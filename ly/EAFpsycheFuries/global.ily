\key re \minor
\time 2/2 \midiTempo#160 s1*8
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 \midiTempo#160 s1*4

\midiTempo#80 \time 4/4 s1*2
\digitTime\time 3/4 s2.
\midiTempo#160 \digitTime\time 2/2 s1
\midiTempo#80 \digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\midiTempo#160 \digitTime\time 2/2 s1*24
\midiTempo#80 \time 4/4 s1
\midiTempo#160 \digitTime\time 2/2 s1
\midiTempo#80 \digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\midiTempo#160 \digitTime\time 2/2 s1*24
\midiTempo#80 \time 4/4 s1
\digitTime\time 3/4 s2.*5
\midiTempo#160 \digitTime\time 2/2 s1*24
\midiTempo#80 \time 4/4 s1*4
\digitTime\time 3/4 s2.
\midiTempo#160 \digitTime\time 2/2 s1
\time 3/8 s4.

s4.*33 \bar "|."
