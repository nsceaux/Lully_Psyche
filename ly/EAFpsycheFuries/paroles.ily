\tag #'(furie1 furie2 furie3 basse) {
  Où pen -- ses- tu por -- ter tes pas,
  \tag #'(furie1 furie2 basse) {
    té -- mé -- rai -- re mor -- tel -- le,
  }
  té -- mé -- rai -- re mor -- tel -- le ?
  Quel des -- tin par -- mi nous t’ap -- pel -- le ?
  Viens- tu nous bra -- ver i -- ci bas ?
  Quel des -- tin par -- mi nous t’ap -- pel -- le ?
  Viens- tu nous bra -- ver i -- ci bas ?
}
\tag #'(psyche basse) {
  Si j’ai pas -- sé le Stix, a -- vant l’heu -- re fa -- ta -- le,
  pour ve -- nir aux En -- fers de -- man -- der du se -- cours,
  quand je vous au -- rai dit ma pei -- ne sans é -- ga -- le,
  vous plain -- drez a -- vec moi, le mal -- heur de mes jours.
}
\tag #'(furie1 furie2 furie3 basse) {
  Non, non, n’at -- tends rien de fa -- vo -- ra -- ble,
  \tag #'furie3 { n’at -- tends rien de fa -- vo -- ra -- ble, }
  ja -- mais dans les En -- fers,
  ja -- mais dans les En -- fers on ne fut pi -- toy -- a -- ble.
  Ja -- mais dans les En -- fers
  \tag #'(furie1 furie2 basse) { on ne fut, }
  on ne fut pi -- toy -- a -- ble.
  Non, non,
  \tag #'furie3 { non, non, }
  n’at -- tends rien de fa -- vo -- ra -- ble,
  ja -- mais dans les En -- fers
  \tag #'(furie1 furie2 basse) { on ne fut, }
  on ne fut pi -- toy -- a -- ble.
}
\tag #'(psyche basse) {
  Ah ! lais -- sez- vous tou -- cher à mes tris -- tes dou -- leurs ;
  Je ne viens point dans vos de -- meu -- res som -- bres,
  trou -- bler le si -- len -- ce des Om -- bres,
  j’y viens par -- ler de mes mal -- heurs.
}
\tag #'(furie1 furie2 furie3 basse) {
  Non, non, n’at -- tends rien de fa -- vo -- ra -- ble,
  \tag #'furie3 { n’at -- tends rien de fa -- vo -- ra -- ble, }
  ja -- mais dans les En -- fers,
  ja -- mais dans les En -- fers on ne fut pi -- toy -- a -- ble.
  Ja -- mais dans les En -- fers
  \tag #'(furie1 furie2 basse) { on ne fut, }
  on ne fut pi -- toy -- a -- ble.
  Non, non,
  \tag #'furie3 { non, non, }
  n’at -- tends rien de fa -- vo -- ra -- ble,
  ja -- mais dans les En -- fers
  \tag #'(furie1 furie2 basse) { on ne fut, }
  on ne fut pi -- toy -- a -- ble.
}
\tag #'(psyche basse) {
  Un or -- dre sou -- ve -- rain qu’il faut ex -- é -- cu -- ter
  m’o -- blige à cher -- cher vo -- tre Rei -- ne :
  et me la fai -- sant voir, vous fi -- ni -- rez ma pei -- ne ;
  elle vou -- dra bi -- en m’é -- cou -- ter.
}
\tag #'(furie1 furie2 furie3 basse) {
  Non, non, n’at -- tends rien de fa -- vo -- ra -- ble,
  \tag #'furie3 { n’at -- tends rien de fa -- vo -- ra -- ble, }
  ja -- mais dans les En -- fers,
  ja -- mais dans les En -- fers on ne fut pi -- toy -- a -- ble.
  Ja -- mais dans les En -- fers
  \tag #'(furie1 furie2 basse) { on ne fut, }
  on ne fut pi -- toy -- a -- ble.
  Non, non,
  \tag #'furie3 { non, non, }
  n’at -- tends rien de fa -- vo -- ra -- ble,
  ja -- mais dans les En -- fers
  \tag #'(furie1 furie2 basse) { on ne fut, }
  on ne fut pi -- toy -- a -- ble.
}
\tag #'(psyche basse) {
  Deux mots, et de ces lieux je suis prête à sor -- tir.
  Con -- dui -- sez- moi vers Pro -- ser -- pi -- ne.
  
  Puis -- qu’à la voir el -- le s’ob -- sti -- ne,
  promp -- te -- ment, promp -- te -- ment qu’on l’aille a -- ver -- tir.
}

\tag #'(furie1 furie2 furie3 basse) {
  Ce -- pen -- dant mon -- trons- lui ce que ces lieux ter -- ri -- bles,
  ont d’ob -- jets plus hor -- ri -- bles.
  Ce -- pen -- dant mon -- trons- lui ce que ces lieux ter -- ri -- bles,
  ont d’ob -- jets plus hor -- ri -- bles.
  Ce -- pen -- dant mon -- trons- lui ce que ces lieux ter -- ri -- bles,
  ont d’ob -- jets plus hor -- ri -- bles.
  Ce -- pen -- dant mon -- trons- lui ce que ces lieux ter -- ri -- bles,
  ont d’ob -- jets plus hor -- ri -- bles.
}
