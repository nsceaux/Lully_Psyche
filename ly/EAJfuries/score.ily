\version "2.11.57"
\score {
  \new StaffGroupNoBar <<
    \new Staff \withLyrics << 
      \characterName \markup \center-column \smallCaps { Première Furie }
      \global \keepWithTag #'furie1 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics << 
      \characterName \markup \center-column \smallCaps { Deuxième Furie }
      \global \keepWithTag #'furie2 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics << 
      \characterName \markup \center-column \smallCaps { Troisième Furie }
      \global \keepWithTag #'furie3 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
