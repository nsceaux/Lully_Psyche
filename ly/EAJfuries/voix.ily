<<
  \tag #'(furie1 basse) {
    \clef "vhaute-contre" r8 sol'8 |
    fad'2.\trill r8 fad'8 |
    sol'1 |
    mi'4 mi'8 mi'8 mi'4 mi'4 |
    re'2. r8 re'8 |
    sol'1 |
    mi'4 mi'8 mi'8 mi'4 la'4 |
    fad'4. |
    r8 la8 la8 |
    re'8 re'8 re'8 |
    si8. si16 si8 |
    si8 si8 mi'8 |
    re'4 re'8 |
    r8 re'8 re'8 |
    sol'8 sol'8 sol'8 |
    fad'4 fad'8 |
    sol'8 fad'8 sol'8 |
    mi'8 mi'8 mi'8 |
    fad'8 red'8.\trill mi'16 |
    mi'4. |
    r8 sold'8 sold'8 |
    la'8 la'8 mi'8 |
    fad'8. fad'16 sol'8 |
    sol'8 sol'8 re'8 |
    mi'4 mi'8 |
    r8 mi'8 fad'8 |
    sol'8 sol'8 la'8 |
    fad'4\trill fad'8 |
    sol'8 sol'8 re'8 |
    mi'8 re'8 do'8 |
    si8 la8 re'8 |
    si4.\trill |
  }
  \tag #'furie2 {
    \clef "vtaille" r8 si8 |
    la2.\trill r8 re'8 |
    si1 |
    do'4 do'8 do'8 do'4 do'4 |
    si2. r8 si8 |
    mi'1 |
    dod'4 dod'8 dod'8 dod'4 dod'4 |
    re'4. |
    r8 fad8 fad8 |
    si8 si8 si8 |
    sol8. sol16 sol8 |
    sol8 sol8 do'8 |
    si4 si8 |
    r8 si8 si8 |
    mi'8 mi'8 mi'8 |
    red'4 red'8 |
    si8 si8 si8 |
    do'8 sol8 sol8 |
    la8 fad8 si8 |
    sold4.\trill |
    r8 si8 si8 |
    dod'8 dod'8 dod'8 |
    re'8. la16 si8 |
    si8 si8 si8 |
    do'4 do'8 |
    r8 sol8 la8 |
    si8 si8 do'8 |
    la4 la8 |
    si8 si8 si8 |
    sol8 sol8 fad8 |
    sol8 sol8 la8 |
    sol4. |
  }
  \tag #'furie3 {
    \clef "vbasse" r8 sol8 |
    re2. r8 re8 |
    mi1 |
    do4 do8 do8 do4 do4 |
    sol,2. r8 sol8 |
    re1 |
    la4 la8 la8 la4 la4 |
    re4. |
    r8 re8 re8 |
    si,8 si,8 si,8 |
    mi8. mi16 mi8 |
    mi8 mi8 do8 |
    sol4 sol8 |
    r8 sol8 sol8 mi8 mi8 mi8 |
    si4 si8 |
    sol8 red8 mi8 |
    do8 do8 do8 |
    la,8 si,8 si,8 |
    mi,4. |
    r8 mi8 mi8 |
    la8 la8 la8 |
    re8. re16 sol8 |
    sol8 sol8 sol,8 |
    do4 do8 |
    r8 do8 la,8 |
    mi8 mi8 do8 |
    re4 re8 |
    si,8 si,8 si,8 |
    do8 si,8 la,8 |
    sol,8 re8 re8 |
    sol,4. |
  }
>>
