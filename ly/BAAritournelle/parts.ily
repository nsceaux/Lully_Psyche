\piecePartSpecs
#`((basse-continue #:system-count 3)
   (basse)
   (dessus #:score-template "score-2dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#17 #}))
