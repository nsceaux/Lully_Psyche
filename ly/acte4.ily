\newBookPart#'()
\act "Acte Quatrième"
\sceneDescription\markup\justify {
  Le théâtre représente une salle du palais de Proserpine.
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center\smallCaps { Psyché. }
%% 4-1
\pieceToc\markup\wordwrap {
  Psyché : \italic { Par quels noirs et fâcheux passages }
}
\includeScore "EAAritournelle"
\includeScore "EABpsyche"
\includeScore "EACpsyche"
%% 4-2
\sceneDescription\markup\justify {
  Des Démons passent sur le théâtre, et commencent à épouvanter Psyché :
  Ils sont à l’instant suivis de trois Furies
}
\pieceToc "Prélude"
\includeScore "EADair"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Les trois Furies, Psyché.
}
%% 4-3
\pieceToc\markup\wordwrap {
  Les Furies, Psyché : \italic { Où penses-tu porter tes pas }
}
\includeScore "EAFpsycheFuries"

\sceneDescription\markup\justify {
  Les Démons forment une danse, et montrent à Psyché ce qu’il y a
  de plus effroyable dans les Enfer.
}
%% 4-4
\pieceToc "Air des Démons"
\includeScore "EAHair"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { Les trois Furies, Deux Nymphes } de l’Acheron,
  \smallCaps Psyché.
}
%% 4-5
\pieceToc "Prélude"
\includeScore "EAIair"
\newBookPart#'(full-rehearsal full-urtext)
%% 4-6
\pieceToc\markup\wordwrap {
  Les trois Furies : \italic {
    Venez, venez, Nymphes de l'Acheron }
}
\includeScore "EAJfuries"
\newBookPart#'(full-rehearsal full-urtext)
%% 4-7
\pieceToc\markup\wordwrap {
  Les deux Nymphes, Psyché :
  \italic { En vain ce soin vous embarrasse }
}
\includeScore "EAKnymphesPsyche"
\sceneDescription\markup\justify {
  Quatre Démons traversent le Théâtre en volant, et vont se perdre au
  travers de la voûte de la salle de Proserpine.
}
\newBookPart#'(full-rehearsal full-urtext)
\sceneDescription\markup\justify {
  [Ballard] On reprend pour entr’acte l’air des Démons.
}
%% 4-8
\pieceToc "[Entr’acte]"
\reIncludeScore "EAHair" "EALair"
\actEnd \markup { FIN DU QUATRIÈME ACTE }
