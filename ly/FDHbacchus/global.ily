\key fa \major
\time 3/2 \midiTempo#160 s1.*4
\time 4/8 s2*11
\digitTime\time 3/4 \alternatives s2. s2.
\bar ".!:" s2.*15 \alternatives { s2. \bar ":!." } s2. \bar "|."
