\clef "dessus" r4 si'8. si'16 si'8. do''16 re''8 do''16 si'16 |
do''4. re''8 si'8. si'16 mi''8. fad''16 |
red''4. fad''8 sol''8. fad''16 mi''8. red''16 |
dod''4. dod''8 red''8. mi''16 red''8.\trill mi''16 |
mi''4 sol''8. sol''16 fad''8. fad''16 fad''8. si''16 |
sold''4\trill la''8. la''16 la''8. si''16 sold''8\trill fad''16 sold''16 |
la''4. mi''8 sol''8. sol''16 sol''8. la''16 |
fad''4.\trill fad''8 sol''8 fad''16 sol''16 fad''8.\trill sol''16 |
sol''4 si'8. si'16 si'8. do''16 re''8 do''16 si'16 |
do''4. re''8 si'8. si'16 mi''8. fad''16 |
red''4. fad''8 sol''8. fad''16 mi''8. red''16 |
dod''4. dod''8 red''8. mi''16 red''8.\trill mi''16 |
mi''4 sol''8. sol''16 mi''8.\trill mi''16 la''8. sol''16 |
fad''8. fad''16 sol''8. sol''16 sol''8. fad''16 mi''8.\trill re''16 |
re''8. fad''16 fad''8. fad''16 sol''8. fad''16 mi''8. re''16 |
dod''4\trill re''8. re''16 re''8. dod''16 dod''8.\trill si'16 |
si'4 si'8. si'16 si'8. dod''16 re''8 do''16 si'16 |
do''4. re''8 si'8. si'16 mi''8. fad''16 |
red''4.\trill fad''8 sol''8. fad''16 mi''8. red''16 | 
dod''4.\trill dod''8 red''8. mi''16 red''8.\trill mi''16 |
mi''1 |

