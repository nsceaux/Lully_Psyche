\key sol \minor
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*3
%% 10
\time 4/4 s1
\digitTime\time 3/4 s2.*3
\time 4/4 s1*11
%% 25
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*4
%% 41
\digitTime\time 3/4 s2.*2
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*4
%% 50
\digitTime\time 3/4 \midiTempo#80 s2.*3
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.*2
%% 66
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\bar "|."
