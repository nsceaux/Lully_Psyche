\score {
  \new ChoirStaff <<
    \new Staff \withLyrics << 
      \global \includeNotes "voix" 
    >> \includeLyrics "paroles"
    \new Staff << 
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
  \midi { }
}
