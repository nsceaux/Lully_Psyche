\clef "vhaute-contre" <>^\markup\character-text Vulcain aux Cyclopes
R1 |
r2 r4 sol'8 fa'8 |
mi'4 mi'8 re'8 do'4.\trill si8 |
la2 r4 fa'8. mi'16 |
re'4. mi'8 do'4 si8 do'8 |
si1\trill |
si2.\trill |
r4 sol4 sol4 |
do'2 do'4 |
re'4 mi'4. fa'8 |
mi'2\trill do'4 |
r4 r8 mi'8 mi'8. fa'16 |
re'2 mi'4 |
dod'4\trill ~ dod'4. re'8 |
re'2. |
fa'8 [ mi'8 ] re'8 [ do'8 ] si8 [ la8 ]|
si4 sol4 mi'4 |
la4. si8 do'4 |
do'4. re'8 si4 |
do'2 r4 |
R2.*6 |
r2 <>^\markup\character Vulcain do'4 |
re'4. mi'8 fa'4 |
mi'4 fa'8 [ mi'8 ] re'8 [ do'8 ]|
re'4 si4 r8 mi'8 |
la2 la8 si8 |
do'4 re'4. mi'8 |
mi'4( re'2\trill) |
do'2 mi8 fad?8 |
sol2 sol8. sol16 |
la2 si8. do'16 |
si2\trill r4 |
R2.*2 |
r2 <>^\markup\character Vulcain mi'4 |
la4 fa'8 sol'8 fa'8 mi'8 |
re'2\trill r4 |
R2.*2 |
r4 <>^\markup\character Vulcain do'4 do'4 |
si4. do'8 la4 |
sold4.\trill fad8 ( mi4 )|
mi'4 fa'8 [ mi'8 ] re'8 [ do'8 ]|
si4. do'8 re'4 |
dod'4\trill r2 |
r4 <>^\markup\character Vulcain re'8 la8 si8 do'8 |
si2\trill r4 |
r4 r4 <>^\markup\character Vulcain do'4 |
la2 r4 |
r4 <>^\markup\character Vulcain sol8 la8 si8 sol8 |
do'2 r4 |
r4 r4 <>^\markup\character Vulcain re'4 |
si2 mi'4 |
do'4 do'8 re'8 mi'8 fa'8 |
re'4\trill re'8 mi'8 fa'8 sol'8 |
mi'2\trill r4 |
R2.*4 |
r4 <>^\markup\character Vulcain mi'4 do'4 |
re'4 mi'8 [ re'8 ] do'8 [ si8 ]|
do'2 la4 |
si4 sol4 do'4 |
do'4 re'8 [ do'8 ] re'8 [ si8 ]|
do'4 mi'4 do'4 |
re'4 mi'8 [ re'8 ] do'8 [ si8 ]|
do'2 la4 |
si4 sol4 do'4 |
do'4 re'8 [ do'8 ] re'8 [ si8 ]|
do'2 r4 |
R2.*6 |
R1 |
\sugNotes {
  R1
  r2 r4 sol'8 fa' |
  mi'4. re'8 do'4 do'8 si |
  la2\trill r4 fa'8 mi' |
  re'4 re'8 mi' do'4( si8) do' |
  si2.\trill |
  r4 sol sol |
  \once\set Staff.whichBar = "|"
  \custosNote do'
}


