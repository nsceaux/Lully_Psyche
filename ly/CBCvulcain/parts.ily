\piecePartSpecs
#`((basse-continue #:score-template "score-basse-voix")
   (basse)
   (quinte)
   (taille)
   (haute-contre)
   (dessus)
   (silence #:on-the-fly-markup , #{ \markup\tacet#88 #}))

