\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff <<
        { s1*6 s2.*13 s4 <>^\markup Les Forgerons
          s2\noBreak s2.*16 s2 <>^\markup Les Forgerons
          s4\noBreak s2.*4 s2 <>^\markup Les Forgerons
          s4\noBreak s2.*7 s4 <>^\markup Les Forgerons
          s2 s2.*3 s4 <>^\markup Les Forgerons
          s2 s2. s4 <>^\markup Les Forgerons
          s2 s2.*4 s2 <>^\markup Les Forgerons
          s4\noBreak s2.*14 s4 <>^\markup Les Forgerons
          s2 s2.*6 s1\break
        }
        \global \includeNotes "dessus"
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { }
  \midi { }
}
