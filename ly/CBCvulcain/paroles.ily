Dé -- pe -- chez, pré -- pa -- rez ces lieux 
pour le plus ai -- ma -- ble des Dieux :
Dieux :
Que cha -- cun pour lui s'in -- té -- res -- se,
n'ou -- bli -- ez rien des soins qu'il faut. 
Quand l'A -- mour pres -- se,
l'on a ja -- mais fait as -- sez tôt.

L'A -- mour ne veut point qu'on dif -- fè -- re,
l'A -- mour ne veut point qu'on dif -- fè -- re,
tra -- vail -- lez, hâ -- tez- vous, hâ -- tez- vous.

Frap -- pez, re -- dou -- blez vos coups.

Que l'ar -- deur de lui plai -- re, 
fas -- se vos soins les plus doux.

Re -- dou -- blez vos coups.
Frap -- pez,
re -- dou -- blez vos coups.
Frap -- pez, frap -- pez,
re -- dou -- blez vos coups,
re -- dou -- blez vos coups.

Que l'ar -- deur de lui plai -- re, 
fas -- se vos soins les plus doux.
Que l'ar -- deur de lui plai -- re,
fas -- se vos soins les plus doux.

Ser -- vez bien un Dieu si char -- mant,
il se plait dans l’em -- pres -- se -- ment :
Que cha -- cun
