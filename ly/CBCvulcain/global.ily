\key do \major \midiTempo#132
\time 4/4 s1*5 \alternatives s1 { \digitTime\time 3/4 s2. }
s2. \segnoMark s2.*74
\time 2/2 s1 \bar "|." \fineMark
s1*5
\digitTime\time 3/4 s2.*2 \bar "|."
\endMark\markup\right-column { à la reprise, jusqu’au mot Fin. }
