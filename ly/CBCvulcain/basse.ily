\clef "basse"
<<
  \tag #'basse { R1*6 R2.*13 r4 }
  \tag #'basse-continue {
    do4 do'8 si8 la4 sol8 fa8 |
    mi4 re8 do8 si,2 |
    do4 do8 re8 mi2 |
    fa4 fa8 mi8 re2 |
    sol2. fad4 |
    sol4 sol,8 la,8 si,4 sol,4 |
    sol4 sol,8 la,8 si,8 sol,8 |
    do2 si,4 |
    la,2. |
    sol,2. |
    do2. |
    do'2 do'8 la8 |
    sib2 sol4 |
    la2 sol4 |
    fa8 sol8 fa8 mi8 re4 |
    re2. |
    sol2 mi4 |
    fa4 sol4 la4 |
    fa4 sol4 sol,4 |
    do4
  }
>> do'2 |
si2. |
la2 fa4 |
sol2. |
fa2. |
mi2. |
fa4 sol4 sol,4 |
do2 <<
  \tag #'basse { r4 R2.*9 | r4 r4 }
  \tag #'basse-continue {
    do'4 |
    do'4 si2 |
    do'4 do2 |
    sol4. fa8 mi4 |
    fa2 fa4 |
    mi4 re4 do4 |
    sol4 sol,2 |
    do2. |
    si,2. |
    la,2. |
    sol,2
  }
>> sol4 |
re2 re4 |
la2 la4 |
mi2 <<
  \tag #'basse { r4 R2. r4 r4 }
  \tag #'basse-continue {
    do4 |
    fa4 re2 |
    sol2
  }
>> sol4 |
la2 la4 |
si2 si4 |
do'4 <<
  \tag #'basse { r4 r R2.*4 r4 }
  \tag #'basse-continue {
    do2 |
    re2. |
    mi2. |
    sold,2 la,4 |
    mi,2. |
    la,4
  }
>> la4. la8 |
re4 <<
  \tag #'basse { r4 r | r4 r }
  \tag #'basse-continue {
    re4. re8 |
    sol2
  }
>> sol,4 |
<<
  \tag #'basse { do4 r r | r4 }
  \tag #'basse-continue {
    do4. re8 mi4 |
    fa4
  }
>> re2 |
sol4 <<
  \tag #'basse { r4 r | r4 }
  \tag #'basse-continue {
    sol4 fa4 |
    mi4
  }
>> do2 |
fa2 <<
  \tag #'basse { r4 | R2.*3 | r4 r }
  \tag #'basse-continue {
    re4 |
    sol2 mi4 |
    la2 la4 |
    si2 si4 |
    do'2
  }
>> do'4 |
si4 sib2 |
la2. |
sol2 do4 ~ |
do4 sol,2 |
do4 <<
  \tag #'basse { r4 r | R2.*9 | r4 }
  \tag #'basse-continue {
    do'2 |
    si4 sol2 |
    la2 fa4 |
    sol2 mi4 |
    do4 sol,2 |
    do2. |
    si,2 do4 |
    la,2. |
    sol,2 mi,4 |
    do,4 sol,2 |
    do4
  }
>> do'2 |
si2. |
la2. |
sol2. |
fa2. |
mi2. |
fa4 sol4 sol,4 |
<<
  \tag #'basse { do1 | R1*5 R2.*2 }
  \tag #'basse-continue {
    do1~ |
    \once\set Staff.whichBar = "|"
    <>^\markup { [Ballard] }
    \sugNotes {
      do4 do'8 si la4 sol8 fa |
      mi4 re8 do si,2 |
      do4. re8 mi2 |
      fa4 fa8 mi re2 |
      sol2. fad4 |
      sol sol,8 la, si, sol, |
      do2 si,4 |
      \once\set Staff.whichBar = "|"
      \custosNote la,4
    }
  }
>>
