<<
  \tag #'(recit1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Aglaure
    r2 r4 r8 re''16 re''16 |
    la'4\trill r8 la'16 la'16 fad'8 la'8 |
    re'4 re'4 
    \ffclef "vbas-dessus" <>^\markup\character Cidippe
    sib'8 sib'8 |
    sol'8 sol'8 r8 do''16 re''16 sib'8 sib'16 sib'16 sib'8 la'8 |
    la'4\trill 
    \ffclef "vbas-dessus" <>^\markup\character-text Psyché à ses sœurs
    r8 re''8 la'8 la'16 la'16 fad'8\trill sol'16 la'16 |
    sib'4 sol'8 sol'8 do''4 do''8 do''8 |
    la'4\trill sib'4 sib'8 sib'8 sib'8 la'8 |
    sib'4 sib'4
  }
  \tag #'recit2 {
    \ffclef "vbas-dessus" R1 R2.*2 R1*4 | r2
  }
>>
<<
  \tag #'(recit2 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Aglaure 
    fa''4 r8 re''8 |
    sib'4 r4 mib''4 r8 do''8 |
    la'4
  }
  \tag #'recit1 {
    \ffclef "vbas-dessus" <>^\markup\character Cidippe
    re''4 r8 sib'8 |
    sol'4 r4 do''4 r8 la'8 |
    fad'4\trill
  }
>>
<<
  \tag #'(recit1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    r8 re''8 si'8 si'8 si'8 do''8 |
    do''4 mib''4 r4 do''8 do''8 |
    la'4\trill la'8 la'8 sib'4 sib'8 do''8 |
    re''8 re''8 r8 re''16 mib''16 fa''8 fa''16 fa''16 re''8. re''16 |
    mib''8 mib''16 r16 sol'16 sol'16 sol'16 la'16 sib'4 sib'8 do''8 |
    la'4\trill 
    \ffclef "vbas-dessus" <>^\markup\character Aglaure 
    re''8 re''8 sol'4 sol'8 sol'8 |
    mib'4\trill 
    \ffclef "vbas-dessus" <>^\markup\character Cidippe
    do''4 r8 sol'16 sol'16 do''8. do''16 |
    la'8\trill la'8 
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    r16 fa''16 fa''16 fa''16 sib'8 sib'16 lab'16 lab'8 lab'16 sol'16 |
    sol'8\trill sol'8
  }
  \tag #'recit2 { r4 r2 | R1*7 | r4 }
>>
<<
  \tag #'(recit2 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Aglaure 
    sib'8 sib'16 sib'16 mib''8. mib''16 mib''16 re''16 do''16 sib'16 |
    la'4\trill r8 re''8 mib''4 r8 do''8 |
    do''8 do''8 la'8 la'8 fad'8.\trill fad'16 fad'8 sol'8 |
    sol'2 r2 |
  }
  \tag #'recit1 {
    \ffclef "vbas-dessus" <>^\markup\character Cidippe
    sol'8 sol'16 sol'16 do''8. do''16 do''16 sib'16 la'16 sol'16 |
    fad'4\trill r8 sib'8 sol'4 r8 mib''8 |
    mib''8 mib''8 do''8 do''8 la'8.\trill la'16 la'8 sib'8 |
    sol'2 r2 |
  }
>>
