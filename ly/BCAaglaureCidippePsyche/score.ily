\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics << 
      \global \keepWithTag #'recit2 \includeNotes "voix" 
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff << 
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1 s2.*2 s1*4 \break
        s1*4 \break
        s1*6 \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
