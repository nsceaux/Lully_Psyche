\newBookPart#'()
\act "Acte Troisième"
\sceneDescription\markup\justify {
  Le Théâtre représente la chambre la plus magnifique du palais de
  l’Amour : elle est ornée de cabinets, de miroirs, et d’autres meubles
  très riches ; on voit dans le fond une alcove fermée d’un rideau.
}
\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center\smallCaps { Venus. }
%% 3-1
\pieceToc "Ritournelle"
\includeScore "DAAritournelle"
%% 3-2
\pieceToc\markup\wordwrap {
  Venus : \italic { Pompe que ce palais de tous côtés étale }
}
\includeScore "DABvenus"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center\smallCaps { Venus, Psyché. }
%% 3-3
\pieceToc\markup\wordwrap {
  Psyché : \italic { Que fais-tu ? montre-toi, cher objet de ma flamme }
}
\includeScore "DBApsyche"
%% 3-4
\pieceToc\markup\wordwrap {
  Psyché, Venus :
  \italic { Par quel art dans ces lieux vous rendez-vous visible }
}
\includeScore "DBBpsycheVenus"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { Psyché, l'Amour } endormi.
}
%% 3-5
\pieceToc\markup\wordwrap {
  Psyché, l’Amour : \italic { À la fin je vais voir mon destin éclairci }
}
\includeScore "DCApsycheAmour"
\sceneDescription\markup\justify {
  Lorsque la lampe étincelle, l’Amour s’éveille et se dérobe,
  en s’envolant aux yeux de Psyché. La Décoration change dans le même
  instant, et ne laisse plus voir qu’un affreux désert.
}
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène IV" "Scène IV"
\sceneDescription\markup\wordwrap-center\smallCaps { Psyché. }
%% 3-6
\pieceToc\markup\wordwrap {
  Psyché : \italic { Arrêtez, cher amant, où fuyez-vous si vite }
}
\includeScore "DDApsyche"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène V" "Scène V"
\sceneDescription\markup\wordwrap-center\smallCaps { Venus, Psyché. }
%% 3-7
\pieceToc\markup\wordwrap {
  Psyché, Vénus : \italic { Ah ! nymphe, venez-vous soulager mes ennuis ? }
}
\includeScore "DEApsycheVenus"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène VI" "Scène VI"
\sceneDescription\markup\wordwrap-center\smallCaps { Psyché. }
%% 3-8
\pieceToc "Ritournelle"
\includeScore "DEBritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 3-9
\pieceToc\markup\wordwrap {
  Psyché : \italic { Vous m’abandonnez donc, cruel, et cher amant }
}
\includeScore "DECpsyche"
\sceneDescription\markup\justify {
  Psyché étant prête à se précipiter dans les flots, le Fleuve paraît
  assis sur son urne, environné de roseaux.
}
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène VII" "Scène VII"
\sceneDescription\markup\wordwrap-center\smallCaps { Le Fleuve, Psyché. }
%% 3-10
\pieceToc\markup\wordwrap {
  Le Fleuve, Psyché :
  \italic { Arrête, c’est trop tôt renoncer à l’espoir }
}
\includeScore "DFAfleuvePsyche"
\newBookPart#'(full-rehearsal full-urtext)
\sceneDescription\markup\justify {
  [Ballard] On reprend pour entr’acte, le deuxième air des Forgerons.
}
%% 3-11
\pieceToc "[Entr’acte]"
\reIncludeScore "CBBair" "DFBentracte"
\actEnd\markup { FIN DU TROISIÈME ACTE }
