\tag #'(couplet1 basse) {
  Fo -- lâ -- trons, fo -- lâ -- trons, di -- ver -- tis -- sons- nous,
  rail -- lons, rail -- ons, nous ne sau -- rions mieux fai -- re,
  la rail -- le -- rie est né -- ces -- sai -- re,
  dans les jeux, dans les jeux les plus doux :
  
  Sans la dou -- ceur que l’on goûte à mé -- di -- re,
  on trou -- ve peu de plai -- sirs sans en -- nui,
  on trou -- ve peu de plai -- sirs sans en -- nui.
  Rien n’est si plai -- sant que de ri -- re,
  quand on rit, quand on rit __ aux dé -- pens d’au -- trui.
  Rien n’est si plai -- sant que de ri -- re,
  quand on rit, quand on rit __ aux dé -- pens d’au -- trui.
}
\tag #'couplet2 {
  Plai -- san -- tons, plai -- san -- tons, ne par -- don -- nons rien,
  ri -- ons, ri -- ons, rien n’est plus à la mo -- de :
  On court pé -- ril d’être in -- com -- mo -- de,
  en di -- sant, en di -- sant trop de bien :
}
