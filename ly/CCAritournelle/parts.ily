\piecePartSpecs
#`((basse-continue)
   (basse)
   (dessus #:score-template "score-2dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#17 #}))
