\score {
  <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \includeNotes "basse" >>
    >>
    \new StaffGroup <<
      \new Staff <<
        <>^\markup\whiteout [Trompette]
        \global \includeNotes "trompette"
      >>
      \new Staff <<
        <>^\markup\whiteout [Tambour]
        \global \includeNotes "tambour"
      >>
      \new Staff <<
        <>^\markup\whiteout [Timbales]
        \global \includeNotes "timbales"
      >>
    >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}
