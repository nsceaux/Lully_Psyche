\score {
  \new Staff \with {
    instrumentName = \markup\center-column { Trompette en ré }
  } \notemode <<
    \transpose re do \global
    \transpose re do { \includeNotes "trompette" }
  >>
  \layout { indent = \largeindent }
}
