\tag #'(voix0 voix1 basse) {
  En vain ce soin vous em -- bar -- ras -- se :
  Nous a -- vons l'or -- dre,
  al -- lez, al -- lez et nous quit -- tez la pla -- ce.
}
\tag #'(voix1 basse) {
  Que m’est- il per -- mis d’es -- pé -- rer ?
  Me fe -- ra- t’on en -- fin con -- duire à vo -- tre Rei -- ne ?
  
  Psy -- ché, ces -- sez de sou -- pi -- rer :
  Si Ve -- nus vous pour -- suit,
  on flé -- chi -- ra sa hai -- ne.

  Quoi ! l’on sait dans ce noir sé -- jour
  à quels maux Ve -- nus me des -- ti -- ne ?

  Mer -- cure en -- voy -- é par l’A -- mour, 
  vient d’en ins -- trui -- re Pro -- ser -- pi -- ne.
  El -- le sait quel pré -- sent Ve -- nus at -- tend de vous ;
  et pour vous l’ap -- por -- ter, el -- le se sert de nous.

  Ah ! que mes pei -- nes sont char -- man -- tes,
  puis -- que l’A -- mour cherche à les sou -- la -- ger !
  Ah ! que mes pei -- nes sont char -- man -- tes,
  puis -- que l’A -- mour cherche à les sou -- la -- ger !
  Dès qu’il veut rendre un mal lé -- ger,
  il n’a plus de chaî -- nes pe -- san -- tes.
  Dès qu’il veut rendre un mal lé -- ger,
  il n’a plus de chaî -- nes pe -- san -- tes.
  Ah ! que mes pei -- nes sont char -- man -- tes,
  puis -- que l’A -- mour cherche à les sou -- la -- ger !
  Ah ! que mes pei -- nes sont char -- man -- tes,
  puis -- que l’A -- mour cherche à les sou -- la -- ger !
}
\tag #'(voix0 voix1 basse) {
  Il doit ê -- tre bien doux d’ai -- mer com -- me vous fai -- tes.
}
\tag #'(voix1 basse) {
  Et n’ai -- me- t’on pas où vous ê -- tes ?
}
\tag #'(voix0 voix1 basse) {
  L’A -- mour a -- ni -- me l’u -- ni -- vers,
  tout cède aux ar -- deurs qu’il ins -- pi -- re : - re :
  Et jus -- que dans les En -- fers, 
  \tag #'voix1 { on re -- con -- nait, }
  on re -- con -- nait son em -- pi -- re.
  Et jus -- que dans les En -- fers,
  on re -- con -- nait son em -- pi -- re.
  Et jus -- que dans les En -- fers,
  \tag #'(voix0 basse) { on re -- con -- nait, }
  on re -- con -- nait son em -- pi -- re.
  Et jus -- que dans les En -- fers,
  on re -- con -- nait son em -- pi -- re.
}
\tag #'(voix1 basse) {
  Eh, qui s’en vou -- drait ga -- ran -- tir !
  Mais de ces lieux par où sor -- tir ?
  Tout ce que je vois m’in -- ti -- mi -- de.
}
\tag #'(voix0 voix1 basse) {
  Per -- dez l’ef -- froi dont vos sens sont gla -- cés,
  nous al -- lons vous ser -- vir de gui -- de.
  Vous, noirs es -- prits, dis -- pa -- rais -- sez.
}
