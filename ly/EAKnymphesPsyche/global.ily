\key sol \major
\digitTime\time 3/4 \midiTempo#80 s2.
\digitTime\time 2/2 \midiTempo#160 s1*6
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.*41
\time 4/4 s1*2
\digitTime\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 2/2 \midiTempo#160 s1 \bar ".|:" s1*6 \alternatives s1 s1 s1*27
\digitTime\time 3/4 \midiTempo#80 s2.*2 \bar "|."
