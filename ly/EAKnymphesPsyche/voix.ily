<<
  \tag #'(voix0 basse) {
    \clef "vdessus" <>^\markup\character Première Nymphe
    r4 r8 sol'8 re''8. si'16 |
    do''4. do''8 do''4. re''8 |
    si'4\trill si'4 r8 re''8 re''8 re''8 |
    mi''4 mi''4 r4 r8 do''8 |
    la'2 r4 r8 re''8 |
    si'8 si'8 si'8 mi''8 la'4. re''8 |
    si'2\trill si'4 r4 |
  }
  \tag #'voix1 {
    \clef "vbas-dessus" <>^\markup\character Deuxième Nymphe
    r4 r8 re''8 si'8. sol'16 |
    la'4. la'8 la'4 la'4 |
    re'4 re'4 r8 si'8 si'8 si'8 |
    do''4 do''4 r4 r8 la'8 |
    fad'2 r4 r8 fad'8 |
    sol'8 sol'8 sol'8 sol'8 sol'4. fad'8 |
    sol'2 sol'4 r4 |
  }
>>
<<
  \tag #'voix0 {
    R1*2 R2. R1 R1*2 R2.*2 R1*4 R2.*2 R1 R2.*2
    R2.*39 r4
  }
  \tag #'(voix1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    re''8 re''8 re''8 mi''8 do''4 do''8 si'8 |
    si'4.\trill si'8 si'8 si'8 si'8 si'8 |
    mi''8 mi''8 fad''8 fad''8 fad''8 fad''8 |
    red''4 red''8 r8
    \ffclef "vbas-dessus" <>^\markup\character Deuxième Nymphe
    r4 si'4 |
    fad'4 r8 si'8 si'8 la'8 la'8 sold'8 |
    sold'4 si'8 si'8 mi''4 mi''8 mi''8 |
    dod''4 re''8 mi''16 fad''16 dod''8.\trill re''16 |
    re''8 re''8
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    la'4 r8 la'8 |
    re''4 re''8 re''8 sol'4 sol'4 |
    mi'4 do''8 mi''8 do''8 do''8 la'8 la'16 sol'16 |
    fad'4\trill fad'8
    \ffclef "vdessus" <>^\markup\character Première Nymphe
    re''8 la'8 la'16 la'16 la'8 la'16 si'16 |
    do''4 do''8 do''16 do''16 mi'8 mi'8 mi'8 fad'8 |
    sol'8 sol'8 r8 si'16 si'16 si'8 do''16 re''16 |
    mi''8 mi''8 fa''8 mi''8 re''8.\trill do''16 |
    do''2 <>^\markup\italic { en présentant la boëte }
    si'8 si'16 si'16 si'8 do''8 |
    re''4 la'8 la'16 la'16 la'8. si'16 |
    sol'2. |
    \ffclef "vbas-dessus" <>^\markup\character-text Psyché ayant la boête
    re''2 si'8 si'8 | \noBreak
    do''4. do''8 do''8 re''8 |
    mi''2 mi''4 |
    si'4 si'4. do''8 |
    la'2. |
    si'4 si'8 si'8 si'8 dod''8 |
    re''2. |
    sol''2 re''8 mi''8 |
    do''4. si'8 la'8 sol'8 |
    fad'4.\trill mi'8( re'4) |
    re''4 re''4 si'4 |
    do''4 la'4 la'4 |
    si'4 la'4.\trill sol'8 |
    sol'2. |
    si'4. si'8 mi''4 |
    dod''4. dod''8 red''8 mi''8 |
    red''2 si'8 si'8 |
    si'4( la'4\trill) sol'4 |
    fad'2 sold'8 la'8 |
    sold'4.\trill fad'8( mi'4) |
    sold'4. sold'8 la'4 |
    si'4. si'8 dod''8 re''8 |
    dod''2\trill la'8 si'8 |
    sol'4.\trill fad'8( mi'4) |
    la'4. sol'8 [ fad'8 ] sol'8 |
    fad'2\trill fad'4 |
    re''2 si'8 si'8 |
    do''4. do''8 do''8 re''8 |
    mi''2 mi''4 |
    si'4 si'4. do''8 |
    la'2. |
    si'4 si'8 si'8 si'8 dod''8 |
    re''2. |
    sol''2 re''8 mi''8 |
    do''4. si'8 la'8 sol'8 |
    fad'4.\trill mi'8( re'4) |
    re''4 re''4 si'4 |
    do''4 la'4 la'4 |
    si'4 la'4.\trill sol'8 |
    sol'4
  }
>>
<<
  \tag #'(voix0 basse) {
    \ffclef "vdessus" <>^\markup\character Première Nymphe
    re''8 re''8 mi''4 mi''8 mi''8 |
    do''4. do''8 re''4 re''8 re''16 re''16 |
    si'2\trill si'4 r4 |
  }
  \tag #'voix1 {
    \ffclef "vbas-dessus" <>^\markup\character Deuxième Nymphe
    si'8 si'8 do''4 do''8 do''8 |
    la'4. la'8 si'4 si'8 si'16 si'16 |
    sold'2\trill sold'4 r4 |
  }
>>
<<
  \tag #'voix0 { R1*2 }
  \tag #'(voix1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    mi''8 mi''8 mi''8 si'8 do''4 do''8 do''8 |
    la'4 la'4 r2 |
  }
>>
<<
  \tag #'(voix0 basse) {
    \ffclef "vdessus" <>^\markup\character Première Nymphe
    r4 re''4 re''4 re''4 |
    mi''4 mi''4 mi''4. fad''8 |
    sol''2. sol''4 |
    mi''2 mi''4 mi''4 |
    fa''2 fa''8[ mi''8] fa''4 |
    fa''2 ( mi''2\trill) |
    re''1 |
    re''2 r2 |
    r2 r4 sol''4 |
    mi''4 mi''8 mi''8 mi''4 sol''4 |
    do''4. do''8 re''4 mi''4 |
    fa''2 fa''8[ mi''8] fa''4 |
    mi''2 mi''4 mi''4 |
    fa''4 fa''8 fa''8 fa''4 re''4 |
    si'4. si'8 si'4 do''4 |
    re''2 do''8[ si'8] do''4 |
    si'2 si'4 mi''4 |
    dod''4 dod''8 dod''8 dod''4 mi''4 |
    la'4. la'8 la'4 la'4 |
    si'4 re''4 re''4. re''8 |
    mi''2 fad''4 sol''4 |
    fad''2\trill fad''4 re''4 |
    si'4 si'8 si'8 si'4 re''4 |
    sol'4 do''4 do''4. do''8 |
    do''2 si'4 do''4 |
    si'2 ( la'2\trill) |
    sol'2
  }
  \tag #'voix1 {
    \ffclef "vbas-dessus" <>^\markup\character Deuxième Nymphe
    r4 si'4 si'4. si'8 |
    do''4 do''4 do''4. re''8 |
    si'2. mi''4 |
    dod''2\trill dod''4 dod''4 |
    re''2 re''8[ dod''8] re''4 |
    re''2 ( dod''2) |
    re''1 |
    re''2 r4 re''4 |
    si'4 si'8 si'8 si'4 sol'4 |
    do''4. do''8 do''4 mi''4 |
    la'4. la'8 si'4 do''4 |
    do''2 si'4 ( la'8 ) si'8 |
    do''2 do''4 do''4 |
    la'4 la'8 la'8 re''4 si'4 |
    sold'4.\trill sold'8 sold'4 la'4 |
    si'2 la'8 [ sold'8 ] la'4 |
    la'2 ( sold'2) |
    la'2. la'4 |
    fad'4 fad'8 fad'8 fad'4 la'4 |
    re'4 si'4 si'4. si'8 |
    do''2 do''4 si'4 |
    la'2\trill la'4 si'4 |
    sol'4 sol'8 sol'8 sol'4 si'4 |
    mi'4 mi'4 mi'4 la'4 |
    fad'2\trill sol'4( fad'8) sol'8 |
    sol'2( fad'2) |
    sol'2
  }
>>
<<
  \tag #'voix0 { r2 R1*4 r2 }
  \tag #'(voix1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Psyché
    r4 si'4 |
    r8 si'8 si'8 do''8 la'4 la'8 sold'8 |
    sold'8\trill r8 mi''4 r4 mi''8 mi''8 |
    si'8 si'8 si'8 mi''8 dod''2\trill |
    la'8 la'8 la'8 re''8 sol'4 sol'8 fad'8 |
    fad'4\trill fad'4
    ^\markup\right-align\italic\override #'(line-width . 35) \wordwrap {
      Elle montre les démons qui sont dans les ailes du Théâtre.
    }
  }
>>
<<
  \tag #'(voix0 basse) {
    \ffclef "vdessus" <>^\markup\character Première Nymphe
    r8 la'8 la'8 si'8 |
    do''4 do''8 mi''8 do''4 do''8 si'8 |
    si'4\trill si'8 si'8 mi''8 mi''16 re''16 do''8 si'8 |
    la'8\trill la'8 r4 re''4 r8 re''16 sol''16 |
    mi''4 r8 la'8 la'8 re''8 |
    si'4 r4 r4 |
  }
  \tag #'voix1 {
    \ffclef "vbas-dessus" <>^\markup\character Deuxième Nymphe
    r8 fad'8 fad'8 sol'8 |
    la'4 la'8 do''8 mi'4 mi'8 fad'8 |
    sol'4 sol'8 sol'8 do''8 do''16 si'16 la'8 sol'8 |
    fad'8 fad'8 r4 si'4 r8 si'16 si'16 |
    sol'4 r8 sol'8 sol'8 fad'8 |
    sol'4 r4 r4 |
  }
>>
