\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'voix0 \includeNotes "voix"
    >> \keepWithTag #'voix0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}
