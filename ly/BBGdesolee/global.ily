\key sol \minor \midiTempo#92
\time 4/4 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3 \bar "|."
