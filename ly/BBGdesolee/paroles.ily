\tag #'(voix1 basse) {
  Ahi, ch'in -- dar -- no si tar -- da,
  non re -- sis -- te a li Dei mor -- ta -- le af -- fet -- to; 
  Al -- to im -- pe -- ro ne sfor -- za,
  o -- ve com -- man -- da il Ciel, l'uom ce -- de a for -- za.
  Al -- to im -- pe -- ro ne sfor -- za,
  o -- ve com -- man -- da il Ciel, l'uom ce -- de a for -- za.
}
\tag #'voix2 {
  \set fontSize = #-2 Ahi, do -- lo -- re!
}
