<<
  \tag #'(voix1 basse) {
    \clef "vbas-dessus" r4 do''4 r8 do''8 lab'8 lab'16 sol'16 |
    sol'4\trill sol'4 r8 sol'16 sol'16 do''8 do''16 re''16 |
    mib''8 mib''16 mib''16 do''8 do''16 si'16 si'4 si'4 |
    r4 re''8 re''8 mib''8 mib''16 mib''16 |
    do''4\trill do''4 la'8 la'16 la'16 sib'8. do''16 |
    re''4 r8 re''8 fad'8 sol'8 sol'8. fad'16 |
    sol'2. |
    r4 mib''8 mib''8 sib'8 sib'16 do''16 |
    re''4 re''4 re''8 re''16 re''16 re''8 mib''8 |
    fa''4 r8 re''8 si'8 do''8 do''8\trill si'8 |
    do''1 |
  }
  \tag #'voix2 {
    \clef "vpetite-haute-contre" R1*3 R2. R1*2 R2.*2 R1
    r2 <>^\markup\small {
      [Édition Ballard : \italic { Ahi, dolore! } page \page-refII #'BBCaffliges ]
    }
    sol'4. re'8 |
    mib'2*2/3 mib' r |
  }
>>
