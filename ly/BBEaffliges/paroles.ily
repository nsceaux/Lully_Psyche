\tag #'(voix2 basse) {
  Com' es -- ser può fra voi, o Nu -- mi e -- ter -- ni! 
  chi vo -- glia es -- tin -- ta u -- na bel -- tà in -- no -- cen -- te!
  Ahi che tan -- to ri -- gor, Cie -- lo in -- cle -- men -- te, 
  Vin -- ce di cru -- del -- tà gli stes -- si in -- fer -- ni!
  Ahi! che tan -- to ri -- gor, Cie -- lo in -- cle -- men -- te! 
  Vin -- ce di cru -- del -- tà gli stes -- si in -- fer -- ni!
}
\tag #'(voix1 basse) {
  Nu -- me fie -- ro!
}
\tag #'(voix2 basse) {
  Dio se -- ve -- ro!
}
\tag #'(voix1 basse) {
  Nu -- me fie -- ro!
}
\tag #'(voix2 basse) {
  Dio se -- ve -- ro!
}
\tag #'(voix1 basse) {
  Per -- chè tan -- to ri -- gor 
  con -- tro in -- no -- cen -- te cor?
  Per -- chè tan -- to ri -- gor 
  per -- chè tan -- to ri -- gor 
  con -- tro in -- no -- cen -- te cor?
  
  Ahi! sen -- ten -- za i -- nu -- di -- ta! 
  Dar mor -- te à la bel -- tà, ch'al -- trui dà vi -- ta,
  ch'al -- trui dà vi -- ta!
  Dar mor -- te à la bel -- tà, ch'al -- trui dà vi -- ta!
  Ahi! sen -- ten -- za i -- nu -- di -- ta, 
  dar mor -- te à la bel -- tà, ch'al -- trui dà vi -- ta!
}
\tag #'voix2 {
  Per -- chè tan -- to ri -- gor 
  con -- tro in -- no -- cen -- te cor?
  Per -- chè tan -- to ri -- gor 
  per -- chè tan -- to ri -- gor 
  con -- tro in -- no -- cen -- te cor?
  Ahi, sen -- ten -- za i -- nu -- di -- ta! 
  Ahi sen -- ten -- za i -- nu -- di -- ta!
  Dar mor -- te à la bel -- tà, ch'al -- trui dà vi -- ta!
  Dar mor -- te à la bel -- tà, ch'al -- trui dà vi -- ta!
  Ahi! sen -- ten -- za i -- nu -- di -- ta, 
  dar mor -- te à la bel -- tà, ch'al -- trui dà vi -- ta!
}
