<<
  \tag #'(voix2 basse) {
    \clef "vbasse" r4 r8 do8 do8 re8 mib8 do8 |
    sol8. sol16 sol8 sol8 do'4 do'4 |
    r8 sol8 sol8 sol8 lab8 lab16 lab16 lab16 sol16 fa16 mib16 |
    re4 re4 r4 sol4 |
    r8 mib8 sol8 sol16 lab16 sib4 do'8 do'16 re'16 |
    re'4 re'4 do'8 do'16 sib16 la8 sol8 |
    fad8 re'8 sib8 sol8 re4 |
    sol,2 r4 sol4 |
    r8 sol8 do'8 do'16 sib16 la4 re'8 re'16 do'16 |
    si4 si4 do'8 do'16 sib16 lab8 sol8 |
    fa8 mib8 re8 do8 sol,4 |
    do2 <<
      \tag #'voix2 { r2 | r2 }
      \tag #'basse { s2 s2 \clef "vbasse" }
    >> r4 do'8 sib8 |
    lab4 fa4 <<
      \tag #'voix2 { r2 | r2 }
      \tag #'basse { s2 s2 \clef "vbasse" }
    >> r4 sib8 lab8 |
    sol4 mib4
    \tag #'voix2 {
      r2 |
      r4 mib8 fa8 sol4 sol8 lab8 |
      sib8 sib,8 sib,8 do8 re4. mib8 |
      fa4 fa8 sol8 lab4 lab8 sib8 |
      do'4 do8 re8 mib4 mib8 fa8 |
      sol4 sol4 sol8 sol8 |
      lab4. ( sol8 ) lab4 |
      sol2 r4 |
      r4 do'4. do'8 |
      sib2 sib8 do'8 |
      lab2. |
      sol4 sol4. sol8 |
      fa2 fa8 fa8 |
      mib4 mib4 r8 mib8 |
      sib4. sib8 si8 si8 |
      do'2 do4 |
      re2 mib4 |
      re2. |
      sol,2 sol4 |
      lab4. lab8 la8 la8 |
      sib2 sol4 |
      do'2 lab4 |
      sib4 ( sib,2) |
      mib4 sib4. sib8 |
      si4 si4 si4 |
      do'4 do'4 do'4 |
      mi4. mi8 mi16 [ re16 ] mi8 |
      fa2 fa4 |
      sol2 lab4 |
      sol4 ( sol,2) |
      do2. |
    }
  }
  \tag #'(voix1 basse) {
    <<
      \tag #'voix1 {
        \clef "vpetite-haute-contre" R1*6 R2. R1*3 R2. r2
      }
      \tag #'basse { s1*6 s2. s1*3 s2. s2 }
    >> \clef "vpetite-haute-contre" r4 sol'8 fa'8 |
    mib'4 do'4 <<
      \tag #'voix1 { r2 | r2 }
      \tag #'basse { s2 s2 \clef "vpetite-haute-contre" }
    >> r4 fa'8 mib'8 |
    re'4 sib4 <<
      \tag #'voix1 { r2 | r2 }
      \tag #'basse { s2 s2 \clef "vpetite-haute-contre" }
    >> r4 mib'8 fa'8 |
    sol'4 sol'8 la'8 sib'8 sib8 sib8 do'8 |
    re'4. mib'8 fa'4 fa'8 sol'8 |
    lab'4 lab'8 sib'8 do''4 do'8 re'8 |
    mib'4 mib'8 fa'8 sol'2 |
    r4 si4 si8 si8 |
    do'4. ( si8 ) do'4 |
    si2 r4 |
    R2. |
    r4 sol'4. sol'8 |
    sol'8 [ fa'8 ] fa'4 fa'8 fa'8 |
    fa'8 [ sol'8 ] mib'4. mib'8 |
    lab'4 lab'8 lab'8 lab'8 sib'8 |
    sol'2 sol'8. [ lab'16 ] |
    fa'2 sol'8 [ re'8 ] |
    mib'4 mib'4 mi'4 |
    fad'2 r8 sol'8 |
    sol'4 ( fad'4. mi'16 [ fad'16 ])|
    sol'2 sol'4 |
    sol'8 [ fa'8 ] fa'8. fa'16 fa'16 [ re'16 ] mib'8 |
    re'2 r8 sib'8 |
    mib'2 fa'8 [ sol'8 ]|
    sol'4( fa'2\trill) |
    mib'4 sol'4. sol'8 |
    fa'4 fa'4 fa'8 re'8 |
    mib'4 mib'4 sol'4 |
    sol'4 sol'8 sol'8 sol'16 [ fa'16 ] sol'8 |
    lab'2 re'4 |
    si2 r8 do'8 |
    do'8([ re'16 mib'16] re'2)\trill |
    do'2. |
  }
>>
