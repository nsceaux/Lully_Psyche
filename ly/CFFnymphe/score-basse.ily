\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyricsB << 
      \global \includeNotes "voix"
    >>
    { \set fontSize = #-1 \keepWithTag #'couplet1 \includeLyrics "paroles" }
    { \set fontSize = #-1 \keepWithTag #'couplet2 \includeLyrics "paroles" }
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}
