\tag #'(couplet1 basse) {
  Ai -- ma -- ble jeu -- nes -- se, 
  sui -- vez la ten -- dres -- se,
  joi -- gnez aux beaux jours 
  la dou -- ceur des A -- mours.
  C’est pour vous sur -- pren -- dre, 
  qu’on vous fait en -- ten -- dre, 
  qu’il faut é -- vi -- ter leurs sou -- pirs,
  et crain -- dre leurs dé -- sirs.
  Lais -- sez- vous ap -- pren -- dre 
  quels sont leurs plai -- sirs.
}
\tag #'couplet2 {
  L’A -- mour a des char -- mes,
  ren -- dons- lui les ar -- mes ;
  Ses soins et ses pleurs
  ne sont pas sans dou -- ceurs :
  Un cœur pour le sui -- vre,
  à cent maux se li -- vre ;
  Il faut pour goû -- ter ses ap -- pas,
  lan -- guir jus -- qu’au tré -- pas :
  Mais ce c’est pas vi -- vre,
  que de n’ai -- mer pas.
}
