\tag #'(voix1 basse) {
  Deh, pian -- ge -- te al pian -- to mi -- o, 
  Deh, pian -- ge -- te al pian -- to mi -- o, 
  pian -- ge -- te al pian -- to mi -- o, 
  sas -- si du -- ri, an -- ti -- che sel -- ve, 
  la -- gri -- ma -- te, fon -- ti, bel -- ve, 
  la -- gri -- ma -- te, la -- gri -- ma -- te, fon -- "ti e" bel -- ve, 
  d'un bel vo -- to il fa -- to ri -- o
  il fa -- to ri -- o.
  Deh, pian -- ge -- te al pian -- to mi -- o, 
  Deh, pian -- ge -- te al pian -- to mi -- o, 
  pian -- ge -- te al pian -- to mi -- o.
}
\tag #'(voix2 basse) {
  Ahi do -- lo -- re!
}
\tag #'(voix3 basse) {
  Ahi mar -- ti -- re!
}
\tag #'(voix2 basse) {
  cru -- da mor -- te,
}
\tag #'(voix3 voix1 basse) {
  em -- pia sor -- te,
}
\tag #'(voix2 basse) {
  che con -- dan -- "ni a" mo -- rir 
  che con -- dan -- "ni a" mo -- rir, a mo -- rir tan -- ta bel -- tà! 
}
\tag #'voix3 {
  che con -- dan -- "ni a" mo -- rir 
  che con -- dan -- "ni a" mo -- rir tan -- ta bel -- tà! 
}
\tag #'(voix1 basse) {
  Cie -- li, stel -- le, ahi cru -- del -- tà
  ahi cru -- del -- tà!
}
\tag #'(voix2 basse) {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi cru -- del -- tà!
}
\tag #'(voix3 basse) {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi cru -- del -- tà
}
\tag #'(voix1 basse) {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi __ cru -- del -- tà!
}
\tag #'voix2 {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi __ cru -- del -- tà!
}
\tag #'(voix3 basse) {
  Cie -- li, stel -- le, ahi cru -- del -- tà!
}
\tag #'(voix1 basse) {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi __ cru -- del -- tà!
}
\tag #'voix2 {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi __ cru -- del -- tà!
}
\tag #'voix3 {
  Cie -- li, stel -- le, ahi cru -- del -- tà,
  ahi cru -- del -- tà!
}
