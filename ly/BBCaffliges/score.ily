\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics << 
      \global \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'voix1 \includeLyrics "paroles"
    \new Staff \withLyrics << 
      \global \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'voix2 \includeLyrics "paroles"
    \new Staff \withLyrics << 
      \global \keepWithTag #'voix3 \includeNotes "voix"
    >> \keepWithTag #'voix3 \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}
