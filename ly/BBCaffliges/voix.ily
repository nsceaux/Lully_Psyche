<<
  \tag #'(voix1 basse) {
    \clef "vbas-dessus" <>^\markup\character Une femme alligée
    r2 sol'2. sol'4 |
    lab'2. ( sol'4 ) sol'4 sol'4 |
    sol'2 ( mib'2 ) fa'4 fa'4 (|
    sol'2 ) sol'2 r2 |
    r2 mib''2. mib''4 |
    do''2 ( sib'4 ) sib'4 sib'4 la'8 sib'8 |
    la'2\trill la'2 r4 fa''4 |
    si'2 ( la'4 sol'4 mib''2 ~|
    mib''4 re''4 ) re''2 r4 re''4 |
    re''2 ( si'2 ) do''2 |
    do''1 ( si'2 )|
    do''1 sol'4. mib'8 |
    sib'2 sib'4 sib'4 mib''4. re''8 |
    do''2 do''2 lab'4. lab'8 |
    lab'2 fa'2 sib'4. lab'8 |
    sol'2 sol'2 mib''4. mib''8 |
    mib''2 ( re''4 ) re''4 re''4 re''4 |
    re''4 ( mib''4 ) do''2 la'4. re''8 |
    fad'2\trill fad'2 sol'4 la'4 |
    sib'2 sib'4 sib'4 si'4. si'8 |
    do''2 do''4 do''8 do''8 ( re''4. ) mib''8 |
    si'2 si'2 r2 |
    r2 sol'2. sol'4 |
    lab'1 sol'2 |
    do''1 fa'4 fa'4 |
    sol'2 sol'2 r2 |
    r2 mib''2. mib''4 |
    do''2 sib'4 sib'4 sib'4 sib'4 |
    la'2\trill la'2 r4 fa''4 |
    si'4. ( la'8 sol'2 mib''2 ~ |
    mib''2. re''4 ) re''4. re''8 |
    re''2 ( si'2 ) do''2 |
    do''1 ( si'2 )|
    do''2 r2 r2 |
    R1.*6 |
    <<
      \tag #'voix1 {
        R1.*3 |
        r2 r \sug sib'4 \sug sib' |
        \sug sol'2 \sug sol' r |
        R1.*5 r2
      }
      \tag #'basse { s1.*10 s2 \clef "vbas-dessus" }
    >> re''2 re''2 |
    r2 mib''2 mib''2 |
    do''1 sib'4 do''4 |
    la'1 re''2 ~ |
    re''2 r2 do''4 re''4 |
    si'2
    <<
      \tag #'voix1 { r2 r2 | R1.*12 | r2 }
      \tag #'basse { s1 s1.*12 s2 \clef "vbas-dessus" }
    >> mib''2 mib''2 |
    r2 do''2 do''2 |
    re''1 re''4 do''4 |
    si'1 do''2 ~ |
    do''2( si'2 ) si'4 do''4 |
    <<
      \tag #'voix1 { do''1 r2 | R1.*3 | }
      \tag #'basse { do''2 s1 s1.*3 \clef "vbas-dessus" }
    >>
    r2 mib''2 mib''2 |
    r2 re''2 re''2 |
    fa''1 fa''4 sol''4 |
    mib''1 do''2 ~ |
    do''1 si'4 do''4 |
    do''1 r2 |
    R1.*6 |
  }
  \tag #'(voix2 basse) {
    <<
      \tag #'voix2 {
        \clef "vpetite-haute-contre" R1.*40 |
         <>^\markup\character Un homme affligé
      }
      \tag #'basse { s1.*40 <>^\markup\character Hommes affligés }
    >>
    \clef "vpetite-haute-contre"
    r2 sol'2. re'4 |
    mib'2 mib'2 <<
      \tag #'voix2 { r2 | r2 r2 }
      \tag #'basse { s2 s1 \clef "vpetite-haute-contre" }
    >> fa'4 fa'4 |
    re'2 re'2 <<
      \tag #'voix2 { r2 | r2 r2 }
      \tag #'basse { s2 s1 \clef "vpetite-haute-contre" }
    >> sol'4. sol'8 |
    fa'1 fa'4 re'4 |
    mib'1 mi'4 mi'4 |
    fa'1 fa'4 fa'4 |
    sol'1 sol'4. sol'8 |
    do'2 re'2 re'4 do'4 |
    si2 <<
      \tag #'voix2 { r2 r2 | R1.*4 | r2 }
      \tag #'basse { s1 s1.*4 s2 \clef "vpetite-haute-contre" }
    >> sol'2 sol'2 |
    r2 lab'2 lab'2 |
    fa'1 mib'4 fa'4 |
    re'1 sol'2 ~ |
    sol'2 r2 fa'4 fa'4 |
    sol'2
    \tag #'voix2 {
      r2 r2 |
      R1.*7 |
      r2 sol'2 sol'2 |
      r2 lab'2 lab'2 |
      fa'1 fa'4 mib'4 |
      re'1 mib'2 ~ |
      mib'2 ( re'2 ) re'4 mib'4 |
      do'1 r2 |
      R1.*3 |
      r2 sol'2 sol'2 |
      r2 fa'2 fa'2 |
      lab'1 lab'4 sib'4 |
      sol'1 mib'2 ~ |
      mib'2( re'2) re'4 mib'4 |
      do'1 r2 |
      R1.*6 |
    }
  }
  \tag #'(voix3 basse) {
    <<
      \tag #'voix3 {
        \clef "vbasse" R1.*41 |
        \clef "vbasse" <>^\markup\character Un homme affligé
        r2 r2
      }
      \tag #'basse { s1.*41 s1 \clef "vbasse" }
    >> do'4 do'4 |
    fa2 fa2 <<
      \tag #'voix3 { r2 | r2 r2 }
      \tag #'basse { s2 s1 \clef "vbasse" }
    >> sib4. sib8 |
    mib2 mib2 <<
      \tag #'voix3 {
        mib4. mib8 |
        re1 re4. re8 |
        do1 do'4 do'4 |
        lab1 lab4. lab8 |
        mi1 r2 |
        r2 fa2 fa8[ mib8] fa4 |
        sol1 r2 |
        R1.*9 |
        r2
      }
      \tag #'basse { s2 s1.*15 s2 \clef "vbasse" }
    >> sol2 sol2 |
    r2 lab2 lab2 |
    la2 la2 la2 |
    sib2 si2 si2 |
    r2 do'2 do'2 |
    lab1 lab4 sib4 |
    sol1. |
    fa1 fa4 sib4 |
    <<
      \tag #'voix3 { mib1 r2 | R1.*4 | r2 }
      \tag #'basse { mib2 s1 s1.*4 s2 \clef "vbasse" }
    >> do'2 do'2 |
    r2 re'2 re'2 |
    sol2 re2 re2 |
    sol,1 r2 |
    \tag #'voix3 {
      r2 mib2 mib2 |
      r2 sib2 sib2 |
      fa1 fa4 fa4 |
      do1 r2 |
      sol2 sol,2 sol,2 |
      do1 r2 |
      R1.*6 |
    }
  }
>>
