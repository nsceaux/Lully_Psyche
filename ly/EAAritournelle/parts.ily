\piecePartSpecs
#`((basse-continue #:system-count 2)
   (basse #:system-count 2)
   (dessus #:score-template "score-2dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#10 #}))
