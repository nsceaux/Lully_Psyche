R2.*7 |
\ffclef "vhaute-contre" <>^\markup\character Mercure
r8 re'8 re'8 fa'8 do'8 do'16 do'16 do'8 do'16 mi'16 |
la8 la8 r16 la16 la16 re'16 si4 si8 dod'16 re'16 |
dod'4\trill 
\ffclef "vbas-dessus" <>^\markup\character Venus
r4 mi''4 r8 la'8 |
fad'8\trill fad'16 fad'16 sol'8 sol'8 la'8 la'8 la'8 la'8 |
sib'4 r8 sib'16 do''16 re''8 re''16 mi''16 |
fa''4 la'8 la'16 la'16 re''8 sol'8 |
la'8. la'16 
\ffclef "vhaute-contre" <>^\markup\character Mercure
r8 la8 dod'8 dod'16 dod'16 dod'8 dod'16 re'16 |
mi'4. mi'16 mi'16 fa'8 fa'16 fa'16 re'8 re'8 |
si8\trill si8 r16 sol16 sol16 sol16 do'8 do'8 do'8 do'8 |
la8 la8 r8 fa'16 fa'16 la8 sib16 do'16 |
re'4 re'8 mi'8 fa'4 fa'8 mi'8 |
fa'4 r8 fa8 la8 la8 la8 si8 |
do'8. do'16 dod'8\trill dod'8 dod'8 mi'8 |
la8 la16 r16 fa'8 fa'16 mi'16 re'8 do'8 |
si8 si8 do'8 do'8 do'8 re'8 |
mi'2 do'4 do'4 |
sol4 sol16 sol16 la16 sib16 la8\trill la8 fa'8 fa'8 |
re'8 re'16 do'16 sib8 la8 sol8 sol8 sol8 la8 |
fa4 r8 do'8 do'8. do'16 re'8 la8 |
sib8 sib8 sib8 sib8 sib8 sib8 |
sol8\trill sol8 r8 sol16 sol16 do'8 do'16 re'16 |
mi'4 do'8 do'8 fa'4 fa'8 fa'8 |
re'4 re'8 re'8 sol'4 sol'8 sol'8 |
mi'8\trill mi'8 r8 mi'16 fa'16 sol'4 r8 la'16 mi'16 |
fa'4 r4 re'4 re'4 |
si4 r8 si8 mi'4 mi'8 mi'8 |
dod'4\trill dod'8 la16 la16 re'8 re'16 re'16 re'8 mi'8 |
fa'8 r16 fa'16 re'8 re'8 si8 si8 si8 re'8 |
sol8 sol8 r8 do'16 do'16 la8 la16 la16 re'8 re'8 |
si8\trill si8 r8 mi'8 dod'8 dod'8 dod'8 re'8 |
mi'4 mi'8 mi'8 fa'4 fa'8 sol'8 |
mi'2.\trill 
\ffclef "vbas-dessus" <>^\markup\character Venus
dod''2 dod''8 mi''8 |
la'4. la'8 la'8 mi'8 |
fa'4 re'4 sib'8 sib'8 |
sol'4 sol'8 sol'8 do''8 sib'8 |
la'2.\trill |
dod''2 dod''8 mi''8 |
la'4. la'8 la'8 mi'8 |
fa'4 re'4 sib'8 sib'8 |
sol'4 sol'8 sol'8 do''8 sib'8 |
la'2\trill r8 do''8 |
dod''4. dod''8 dod''8 re''8 |
mi''2 mi''4 |
fa''4. mi''8 re''8 do''8 |
si'4. la'8 ( sold'4 )|
si'4 si'4 do''4 |
re''2 do''4 |
do''4( si'4.\trill) la'8 |
la'2 r8 la'8 |
re''4. re''8 re''8 mi''8 |
fa''2 re''4 |
si'4. si'8 dod''8 re''8 |
dod''4.\trill si'8( la'4) |
fa''4. fa''8 re''8 fa''8 |
dod''2 dod''4 |
re''8 [ mi''16 fa''16 ] mi''4.\trill re''8 |
re''2. |
\ffclef "vhaute-contre" <>^\markup\character Mercure
r8 re'8 la8 la8 la8 sib8 |
do'2 do'8 do'8 |
re'2 mi'8 fa'8 |
mi'4\trill mi'4 do'4 |
la4. la8 re'4 |
si4.\trill si8 mi'4 |
dod'2\trill r4 |
r8 re'8 la8 la8 la8 sib8 |
do'2 do'8 do'8 |
re'2 mi'8 fa'8 |
mi'4\trill mi'4 do'4 |
la4. la8 re'4 |
si4.\trill si8 mi'4 |
dod'2.\trill |
r8 mi'8 mi'4. mi'8 |
fa'2. |
re'4 re'4. sol'8 |
mi'4.\trill re'8( dod'4) |
la'4. la'8 sol'8 fa'8 |
mi'2\trill mi'8 fa'8 |
re'2. |
r8 mi'8 mi'4. mi'8 |
fa'2. |
re'4 re'4. sol'8 |
mi'4.\trill re'8( dod'4) |
la'4. la'8 sol'8 fa'8 |
mi'2\trill mi'8 fa'8 |
re'4 r4 r2 |
\ffclef "vbas-dessus" <>^\markup\character Venus
r4 r8 re''16 re''16 mi''4 mi''8 fa''8 |
re''8 re''8 re''8 mi''8 dod''8 re''8 |
mi''8 mi''8 
\ffclef "vhaute-contre" <>^\markup\character Mercure
r8 la16 la16 re'8 re'16 re'16 |
si8. si16 dod'8 re'8 re'8 dod'8 |
re'2 re'4 r4 |
