Vous croy -- ez trop la ja -- lou -- se co -- lè -- re
qui vous a -- ni -- me contre un fils.

Quoi ! Mer -- cure, on n’au -- ra pour moi que du mé -- pris ?
Je pour -- rai me ven -- ger, et n’o -- se -- rai le fai -- re ?

L’A -- mour est ve -- nu dans les cieux,
Ju -- pi -- ter a re -- çu sa plain -- te,
et n’en -- vi -- sa -- ge qu’a -- vec crain -- te,
le dé -- sordre é -- ter -- nel qui me -- na -- ce les Dieux.
Par l’or -- dre du des -- tin Psy -- ché vous est sou -- mi -- se ;
Quand vous le pour -- sui -- vez, son sort dé -- pend de vous :
Mais voy -- ez dans cette en -- tre -- pri -- se
quels mal -- heurs ont dé -- jà sui -- vi vo -- tre cou -- roux ;
L’A -- mour dont les en -- nuis n’ont pu tou -- cher votre â -- me,
em -- poi -- son -- ne les traits dont il per -- ce les cœurs,
il les ouvre à la hai -- ne, aux dé -- dains, aux ri -- gueurs ;
Tout lan -- guit, et rien ne s’en -- flam -- me,
la dis -- corde est par -- mi les Dieux,
la paix s’é -- loi -- gne de la ter -- re,
on se hait, on se fait la guer -- re.
Ces maux que vous cau -- sez vous sont- ils glo -- ri -- eux ?

Ah ! qu’on me lais -- se ma co -- lè -- re,
el -- le venge un trop juste en -- nui :
Ah ! qu’on me lais -- se ma co -- lè -- re,
el -- le venge un trop juste en -- nui :
L’A -- mour à l’u -- ni -- vers
est- il si né -- ces -- sai -- re,
qu’on ne puisse être heu -- reux sans lui ?
L’a -- mour à l’u -- ni -- vers
est- il si né -- ces -- sai -- re,
qu’on ne puisse _ être heu -- _ reux sans lui ?

S’il est quel -- que bon -- heur,
c’est l’A -- mour qui l’as -- su -- re.
Tout flate en ai -- mant, tout nous rit.
S’il est quel -- que bon -- heur,
c’est l’A -- mour qui l’as -- su -- re.
Tout flate en ai -- mant, tout nous rit :
Ô -- tez l’A -- mour de la Na -- tu -- re,
tou -- te la Na -- tu -- re pé -- rit.
Ô -- tez l’A -- mour de la Na -- tu -- re,
tou -- te la Na -- tu -- re pé -- rit.

On veut donc m’o -- bli -- ger à con -- sen -- tir qu’il ai -- me ?

Ju -- pi -- ter qui pa -- raît, vous le di -- ra lui- mê -- me.
