\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"
\newBookPart#'()
\scene "Scène Première" "Scène I"
\sceneDescription\markup\column {
  \justify {
    Le Theâtre représente une Cour magnifique au bord de la Mer.
  }
  \justify {
    Flore paraît au milieu du Théâtre, suivi de ses Nymphes,
    & accompagnée de Vertumne, Dieu des Arbres & des Fruits ;
    & de Palemon, Dieu des eaux : Chacun de ces Dieux conduit
    une Troupe de Divinités ; L'un mène à sa suite des Dryades
    et des Sylvains ; Et l'autre des Dieux des Fleuves et des 
    Naïades. Flore chante le récit suivant, pour inviter Vénus
    à descendre sur terre.
  }
}
%% 0-2
\pieceToc\markup\wordwrap {
  Flore : \italic { Ce n'est plus le temps de la guerre }
}
\includeScore "AABflore"
\newBookPart#'(full-rehearsal full-urtext basse-continue)
%% 0-3
\pieceToc\markup\wordwrap {
  Chœur : \italic { Nous goûtons une paix profonde }
}
\includeScore "AACchoeur"
\newBookPart#'(full-rehearsal full-urtext)
%% 0-4
\pieceTitleToc\markup\wordwrap {
  Air pour les Driades, les Silvains, les Dieux des Fleuves, et des Nayades.
} \markup Air
\includeScore "AADritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 0-5
\pieceToc\markup\wordwrap {
  Vertumne, Palemon : \italic { Rendez-vous, beautés cruelles }
}
\includeScore "AAEvertumnePalaemon"
\newBookPart#'(full-rehearsal full-urtext basse-continue)
%% 0-6
\pieceToc "Menuet"
\includeScore "AAFmenuet"
%% 0-7
\pieceToc\markup\wordwrap {
  Flore : \italic { Est-on sage dans le bel âge }
}
\includeScore "AAGflore"
\markup\column {
  \line { [Edition Ballard] On reprend le menuet ci-devant. }
}
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène II" "Scène 2"
\sceneDescription\markup\justify {
  Venus descend dans une grande machine de nuages, au travers de
  laquelle on découvre son Palais. Les Divinités de la Terre &
  des Eaux recommencent de joindre leurs voix & continuent leurs danses.
}
%% 0-8
\pieceToc\markup\wordwrap {
  Chœur : \italic { Nous goûtons une paix profonde }
}
\reIncludeScore "AACchoeur" "AAGchoeur"
\newBookPart#'(full-rehearsal full-urtext)
%% 0-9
\pieceToc\markup\wordwrap {
  Vénus : \italic { Pourquoi du ciel m'obliger à descendre }
}
\includeScore "AAHvenus"
\newBookPart#'(full-rehearsal full-urtext basse-continue)

\scene "Scène III" "Scène 3"
\sceneDescription\markup\wordwrap-center {
  Flore, & les autres Dieux se retirent, L’Amour descend dans un Nuage.
}
%% 0-10
\pieceToc "Ritournelle"
\includeScore "AAIritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 0-11
\pieceToc\markup\wordwrap {
  Vénus : \italic { Mon fils, si tu plains mes malheurs }
}
\includeScore "AAJvenus"
\newBookPart#'(full-rehearsal full-urtext)
%% 0-12
\pieceToc "[Entr’acte]"
\reIncludeScore "AAAouverture" "AAAKouverture"
\actEnd\markup { FIN DU PROLOGUE }
\partBlankPageBreak#'(basse-continue)