\newBookPart#'()
\act "Acte Deuxième"
\sceneDescription\markup\justify {
  Le Théâtre change, et représente un palais que Vulcain fait achever
  par ses Cyclopes : sa forge se voit dans le fond ; et toute la
  décoration est embarrassée d’enclumes, et de quantité d’autres
  ustenciles propres aux forgerons.
}
%% 2-1
\pieceToc "Ritournelle"
\includeScore "CAAsymphonie"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène Première" "Scène I"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Vulcain, huit cyclopes.
}
%% 2-2
\pieceToc\markup\wordwrap {
  Vulcain : \italic { Cyclopes, achevez ce superbe palais }
}
\includeScore "CABvulcain"
\sceneDescription\markup\wordwrap-center {
  [Ballard] Les Cyclopes se préparent à travailler : ils y sont excités
  par la symphonie suivante.
}
\markupCond#(not (symbol? (ly:get-option 'part))) \markup\vspace#20
\newBookPart#'(full-rehearsal full-urtext)
%% 2-3
\pieceToc "[Ballard] Entrée des Cyclopes"
\reIncludeScore "CBDritournelle" "CACentree"
\newBookPart#'(full-rehearsal full-urtext)
%% 2-4
\pieceToc "[Ballard] Les Forgerons"
\reIncludeScore "CBBair" "CADforgerons"
\newBookPart#'(full-rehearsal full-urtext basse-continue)

\scene "Scène II" "Scène II"
\sceneDescription\markup\wordwrap-center\smallCaps { Zéphir, Vulcain. }
%% 2-5
\pieceToc\markup\wordwrap {
  Zéphir, Vulcain :
  \italic { Pressez-vous ce travail que l’Amour nous demande }
}
\includeScore "CBAvulcainZephir"
\newBookPart#'(full-rehearsal full-urtext)
%% 2-6
\pieceToc "Air"
\includeScore "CBBair"
\newBookPart#'(full-rehearsal full-urtext)
%% 2-7
\pieceToc\markup\wordwrap {
  Vulcain : \italic { Dépechez, préparez ces lieux }
}
\includeScore "CBCvulcain"
\newBookPart#'(full-rehearsal full-urtext basse-continue)
%% 2-8
\pieceToc "[Entrée]"
\includeScore "CBDritournelle"
\newBookPart#'(full-rehearsal full-urtext)

\sceneDescription\markup\wordwrap-center {
  \smallCaps Venus descend de son char.
}
%% 2-9
\pieceToc "Ritournelle"
\includeScore "CCAritournelle"

\scene "Scène III" "Scène III"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Venus, Vulcain.
}
%% 2-10
\pieceToc\markup\wordwrap {
  Venus, Vulcain :
  \italic { Quoi, vous vous employez pour la fière Psyché }
}
\includeScore "CCBvenusVulcain"
\newBookPart#'(full-rehearsal full-urtext)
%% 2-11
\sceneDescription\markup\justify {
  Vulcain et les forgerons disparaissent avec la forge, et l’on voit
  le palais dans son entière perfection, il est orné de vases d’or,
  avec des Amours sur des piédestaux. Il y a dans le fond un magnifique
  portail, au travers duquel on découvre une cour ovale percée en
  plusieurs endroits, sur un jardin délicieux.
}
\pieceToc "Prélude"
\includeScore "CCCritournelle"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène IV" "Scène IV"
\sceneDescription\markup\wordwrap-center\smallCaps { Psyché. }
%% 2-12
\pieceToc\markup\wordwrap {
  Psyché : \italic { Où suis-je ? Quel spectacle est offert à mes yeux ? }
}
\includeScore "CDApsyche"
\newBookPart#'(full-rehearsal full-urtext)

\scene "Scène V" "Scène V"
\sceneDescription\markup\wordwrap-center {
  \smallCaps L'Amour, Nymphes, et Zephirs cachés.
}
%% 2-13
\pieceToc "Prélude"
\includeScore "CEAritournelle"
\newBookPart#'(full-rehearsal full-urtext)
%% 2-14
\pieceToc\markup\wordwrap {
  Psyché, Nymphe, Zéphirs, l'Amour :
  \italic { Quels agréables sons ont frappé mes oreilles }
}
\includeScore "CEBpsycheNympheZephirAmour"

\scene "Scène VI" "Scène VI"
\sceneDescription\markup\wordwrap-center {
  \smallCaps L'Amour sous la figure d’un jeune homme, \italic Psyché.
}
%% 2-15
\pieceToc\markup\wordwrap {
  Psyché, l'Amour : \italic { Eh bien, Psyché, des cruautés du sort }
}
\includeScore "CFAritournelle"
\includeScore "CFBamourPsyche"
\newBookPart#'(full-rehearsal full-urtext)
%% 2-16
\pieceToc\markup\wordwrap {
  L'Amour : \italic { Venez voir ce palais }
}
\includeScore "CFCritournelle"
\includeScore "CFDamour"
\sceneDescription\markup\justify {
  Trois des Nymphes qui étaient cachées commencent à paraître,
  et chantent les couplets qui suivent, pendant que six petits
  Amours et quatre Zephirs expriment par leurs danses, la joie
  qu’ils ont des avantages de l’amour.
}
\newBookPart#'(full-rehearsal full-urtext)
%% 2-17
\pieceToc\markup { Air pour les petits Amours et les Zephirs }
\includeScore "CFEair"
\sceneDescription\markup\justify {
  Les Nymphes chantent de suite les premiers couplets, & reprennent
  ensuinte les seconds, qui sont entremêlés par les danses des petits
  Amours et des Zephirs.
}
\newBookPart#'(full-rehearsal full-urtext)
%% 2-18
\pieceToc\markup\wordwrap {
  Première Nymphe : \italic { Aimable jeunesse }
}
\includeScore "CFFnymphe"
%% 2-19
\pieceToc\markup\wordwrap {
  Deuxième et troisième Nymphes : \italic { Chacun est obligé d'aimer }
}
\includeScore "CFGnymphes"
\newBookPart#'(full-rehearsal full-urtext)
\sceneDescription\markup\justify {
  [Ballard] On reprend pour entr’acte l’air des petits Amours
  et des Zephirs.
}
%% 2-20
\pieceToc "[Entr’acte]"
\reIncludeScore "CFEair" "CFHentracte"
\actEnd "FIN DU SECOND ACTE"
