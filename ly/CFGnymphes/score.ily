\score {
  \new ChoirStaff <<
    \new Staff \withLyricsB << 
      \characterName \markup \center-column \smallCaps { Deuxième Nymphe }
      \global \keepWithTag #'nymphe1 \includeNotes "voix"
    >>
    \keepWithTag #'nymphe1-couplet1 \includeLyrics "paroles"
    \keepWithTag #'nymphe1-couplet2 \includeLyrics "paroles"
    \new Staff \withLyricsB << 
      \characterName \markup \center-column \smallCaps { Troisième Nymphe }
      \global \keepWithTag #'nymphe2 \includeNotes "voix"
    >>
    \keepWithTag #'nymphe2-couplet1 \includeLyrics "paroles"
    \keepWithTag #'nymphe2-couplet2 \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
