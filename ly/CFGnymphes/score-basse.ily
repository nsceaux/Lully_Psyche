\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff } \withLyricsB << 
      \global \keepWithTag #'basse \includeNotes "voix"
    >>
    { \set fontSize = -1
      \keepWithTag #'basse-couplet1 \includeLyrics "paroles" }
    { \set fontSize = -1
      \keepWithTag #'basse-couplet2 \includeLyrics "paroles" }
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
  \midi { }
}
