\clef "vbas-dessus"
<<
  \tag #'(nymphe1 basse) {
    la'4 |
    do''4. re''8 mi''4 |
    fa''4 re''4.\trill re''8 |
    re''4. mi''8 do''4 |
    si'2\trill r8 si'8 |
    do''4. re''8 mi''4 |
    fa''4 re''4.\trill re''8 |
    re''4. mi''8 do''4 ~|
    do''8 si'8 si'4.\trill la'8 |
    la'2 mi''4 |
    si'4 si'4. do''8 |
    la'4 la'4 r8 re''8 |
    re''2 re''8 dod''8 |
    re''4 re''4 r8 fa''8 |
    fa''4 mi''4. fa''8 |
    re''4\trill si'4 do''4 ~|
    do''8 re''8 re''4.\trill do''8 |
    do''2 la'4 |
    do''4. re''8 mi''4 |
    fa''4 re''4.\trill re''8 |
    re''4. mi''8 do''4 |
    si'2\trill r8 si'8 |
    do''4. re''8 mi''4 |
    fa''4 re''4.\trill re''8 |
    re''4. mi''8 do''4 ~|
    do''8 si'8 si'4.\trill la'8 |
    la'2
  }
  \tag #'nymphe2 {
    do''4 |
    la'4. si'8 do''4 |
    re''4 si'4.\trill si'8 |
    si'4. do''8 la'4 |
    sold'2\trill r8 sold'8 |
    la'4. si'8 do''4 |
    re''4 si'4.\trill si'8 |
    si'4. do''8 la'4 ~|
    la'8 si'8 sold'4.\trill la'8 |
    la'2 r4 |
    R2.*7 |
    r4 r4 do''4 |
    la'4. si'8 do''4 |
    re''4 si'4.\trill si'8 |
    si'4. do''8 la'4 |
    sold'2\trill r8 sold'8 |
    la'4. si'8 do''4 |
    re''4 si'4.\trill si'8 |
    si'4. do''8 la'4 ~|
    la'8 si'8 sold'4.\trill la'8 |
    la'2
  }
>>
<<
  \tag #'nymphe1 { r4 | R2.*7 | r4 r4 }
  \tag #'(nymphe2 basse) {
    mi''4 |
    si'4 si'4. do''8 |
    re''4 re''4 r8 la'8 |
    la'4. si'8 do''4 |
    si'4\trill sol'4 r8 si'8 |
    si'4. do''8 re''4 |
    do''4. re''8 mi''4 |
    mi''4. fa''8 re''4 |
    mi''2
  }
>>

<<
  \tag #'(nymphe1 basse) {
    la'4 |
    do''4. re''8 mi''4 |
    fa''4 re''4.\trill re''8 |
    re''4. mi''8 do''4 |
    si'2\trill r8 si'8 |
    do''4. re''8 mi''4 |
    fa''4 re''4.\trill re''8 |
    re''4. mi''8 do''4 ~|
    do''8 si'8 si'4.\trill la'8 |
    la'2
  }
  \tag #'nymphe2 {
    do''4 |
    la'4. si'8 do''4 |
    re''4 si'4.\trill si'8 |
    si'4. do''8 la'4 |
    sold'2\trill r8 sold'8 |
    la'4. si'8 do''4 |
    re''4 si'4.\trill si'8 |
    si'4. do''8 la'4 ~|
    la'8 si'8 sold'4.\trill la'8 |
    la'2
  }
>>
