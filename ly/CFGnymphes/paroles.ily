\tag #'(nymphe1-couplet1 nymphe2-couplet1 basse-couplet1) {
  Cha -- cun est o -- bli -- gé 
  d’ai -- mer à son tour ;
  Et plus on a de quoi char -- mer, 
  plus on doit à l’a -- mour.
}
\tag #'(nymphe1-couplet1 basse-couplet1) {
  Un cœur jeune et ten -- dre 
  est fait pour se ren -- dre,
  il n’a point à pren -- dre 
  de fâ -- cheux dé -- tour.
}
\tag #'(nymphe1-couplet1 nymphe2-couplet1 basse-couplet1) {
  Cha -- cun est o -- bli -- gé 
  d’ai -- mer à son tour ;
  Et plus on a de quoi char -- mer,
  plus on doit à l’a -- mour.
}
\tag #'(nymphe2-couplet1 basse-couplet1) {
  Pour -- quoi s’en dé -- fen -- dre ?
  Que sert- il d’at -- tre -- ndre ?
  Quand on perd un jour
  on le perd sans re -- tour.
}
\tag #'(nymphe1-couplet1 nymphe2-couplet1 basse-couplet1) {
  Cha -- cun est o -- bli -- gé 
  d’ai -- mer à son tour ;
  Et plus on a de quoi char -- mer,
  plus on doit à l’a -- mour.
}

\tag #'(nymphe1-couplet2 nymphe2-couplet2 basse-couplet2) {
  S’il faut des soins et des tra -- vaux
  en ai -- mant,
  on est pay -- é de mil -- le maux
  par un heu -- reux mo -- ment.
}
\tag #'(nymphe1-couplet2 basse-couplet2) {
  On craint, on es -- pè -- re,
  il faut du mys -- tè -- re ;
  Mais on n’ob -- tient guè -- re
  des biens sans tour -- ment.
}
\tag #'(nymphe1-couplet2 nymphe2-couplet2 basse-couplet2) {
  S’il faut des soins et des tra -- vaux
  en ai -- mant,
  on est pay -- é de mil -- le maux
  par un heu -- reux mo -- ment.
}
\tag #'(nymphe2-couplet2 basse-couplet2) {
  Que peut- on mieux fai -- re,
  qu’ai -- mer et que plai -- re ?
  C’est un soin char -- mant
  que l’em -- ploi d’un a -- mant.
}
\tag #'(nymphe1-couplet2 nymphe2-couplet2 basse-couplet2) {
  S’il faut des soins et des tra -- vaux
  en ai -- mant,
  on est pay -- é de mil -- le maux
  par un heu -- reux mo -- ment.
}
